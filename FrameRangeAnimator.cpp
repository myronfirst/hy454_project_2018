#include "FrameRangeAnimator.h"

using namespace std;

FrameRangeAnimator::FrameRangeAnimator()
	: sprite(nullptr), anim(nullptr), currFrame(0) {}
FrameRangeAnimator::~FrameRangeAnimator() {}
void FrameRangeAnimator::Start(Sprite* s, FrameRangeAnimation* a, timestamp_t t) {
	sprite = s;
	anim = a;
	lastTime = t;
	state = ANIMATOR_RUNNING;
	sprite->SetFrame(currFrame = anim->GetStart());
}
void FrameRangeAnimator::Progress(timestamp_t currTime) {
	while (currTime > lastTime && currTime - lastTime >= anim->GetDelay()) {
		if (currFrame == anim->GetEnd())
			currFrame = anim->GetStart();
		else
			++currFrame;

		sprite->Move(anim->GetDx(), anim->GetDy());
		sprite->SetFrame(currFrame);
		lastTime += anim->GetDelay();

		if (currFrame == anim->GetEnd() && !anim->GetContinuous()) {
			state = ANIMATOR_FINISHED;
			NotifyStopped();
			return;
		}
	}
}
