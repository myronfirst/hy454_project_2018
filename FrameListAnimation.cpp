#include "FrameListAnimation.h"

#include <cassert>

using namespace std;
FrameListAnimation::FrameListAnimation(animd_t _id, offset_t _dx, offset_t _dy, delay_t _delay, bool _continuous, const std::vector<frame_t>& _frames)
	: MovingAnimation(_id, _dx, _dy, _delay, _continuous), frames(_frames) {
	assert(!frames.empty());
}
FrameListAnimation::~FrameListAnimation() {}
Animation* FrameListAnimation::Clone(animd_t newId) const {
	return (new FrameListAnimation(newId, GetDx(), GetDy(), GetDelay(), GetContinuous(), frames));
}
const std::vector<frame_t>& FrameListAnimation::GetFrames(void) const {
	return frames;
}
void FrameListAnimation::SetFrames(const std::vector<frame_t>& _frames) {
	frames.clear();
	frames = _frames;
	assert(!frames.empty());
}
