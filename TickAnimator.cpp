#include "TickAnimator.h"

TickAnimator::TickAnimator()
	: completedReps(0), anim(nullptr) {}
TickAnimator::~TickAnimator() {}
void TickAnimator::Start(TickAnimation* a, timestamp_t t) {
	anim = a;
	lastTime = t;
	state = ANIMATOR_RUNNING;
	completedReps = 0;
}
void TickAnimator::Progress(timestamp_t currTime) {
	while (currTime > lastTime && currTime - lastTime >= anim->GetDelay()) {
		if (anim->GetAction()) {
			anim->GetAction()();
		}
		lastTime += anim->GetDelay();
		if (anim->GetRepetitions() != 0) {
			completedReps++;
			if (completedReps == anim->GetRepetitions()) {
				state = ANIMATOR_FINISHED;
				NotifyStopped();
				return;
			}
		}
	}
}
