#include "ConfigParser.h"

#include <cassert>
#include "Wrappers.h"

using namespace std;

ConfigParser::ConfigParser(const std::string& path)
	: config(nullptr), sectionIterator(nullptr), entryIterator(nullptr) {
	config = wrap_create_config();
	LoadConfigFile(path);
}
ConfigParser::~ConfigParser() {
	wrap_destroy_config(config);
	config = nullptr;
	sectionIterator = nullptr;
	entryIterator = nullptr;
}
void ConfigParser::LoadConfigFile(const std::string& path) {
	assert(config);
	assert(!path.empty());
	config = wrap_load_config_file(path.c_str());
}
ALLEGRO_CONFIG* ConfigParser::GetConfigFile(void) {
	return config;
}
//Methods return empty string on default section or nullptr
const std::string ConfigParser::GetConfigValue(const std::string& section, const std::string& key) {
	assert(!section.empty());
	assert(!key.empty());
	const char* ret = wrap_get_config_value(config, section.c_str(), key.c_str());
	return (ret != nullptr ? string(ret) : string(""));
}
const std::string ConfigParser::GetConfigFirstSection() {
	assert(sectionIterator == nullptr);
	const char* ret = wrap_get_first_config_section(config, &sectionIterator);
	return (ret != nullptr ? string(ret) : string(""));
}
const std::string ConfigParser::GetConfigNextSection() {
	assert(sectionIterator);
	const char* ret = wrap_get_next_config_section(&sectionIterator);
	return (ret != nullptr ? string(ret) : string(""));
}
const std::string ConfigParser::GetConfigFirstKey(const std::string& section) {
	assert(!section.empty());
	const char* ret = wrap_get_first_config_entry(config, section.c_str(), &entryIterator);
	return (ret != nullptr ? string(ret) : string(""));
}
const std::string ConfigParser::GetConfigNextKey() {
	assert(entryIterator);
	const char* ret = wrap_get_next_config_entry(&entryIterator);
	return (ret != nullptr ? string(ret) : string(""));
}
