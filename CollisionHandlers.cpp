#include "CollisionHandlers.h"
#include <iostream>
#include "AnimationDataHolder.h"
#include "AnimationFilmHolder.h"
#include "SoundMixer.h"
using namespace std;

CollisionData::CollisionData(std::string film_name, std::string newState, int dmg)
	: filmID(film_name), newForcedState(newState), damage(dmg) {}

CollisionData::~CollisionData() {}

void CollisionData::SetFilmName(std::string name) {
	filmID = name;
}
std::string CollisionData::GetFilmName() {
	return filmID;
}
void CollisionData::SetNewForcedState(std::string state) {
	newForcedState = state;
}
std::string CollisionData::GetNewForcedState() {
	return newForcedState;
}
void CollisionData::SetDamage(int dmg) {
	damage = dmg;
}
int CollisionData::GetDamage() {
	return damage;
}

std::map<std::string, CollisionData> CollisionFilmMap = {
	{"high_kick", CollisionData(".successful.kick.by.enemy", "hit_by_high_kick", 15)},
	{"right_high_punch", CollisionData(".enemy.hit.left.punch", "hit_by_high_punch", 7)},
	{"left_high_punch", CollisionData(".enemy.hit.left.punch", "hit_by_high_punch", 7)},
	{"down_high_kick", CollisionData(".successful.kick.by.enemy", "hit_by_high_kick", 10)},
	{"jump_upper_high_kick", CollisionData(".successful.kick.by.enemy", "hit_by_high_kick", 16)},
	{"jump_forward_high_kick", CollisionData(".successful.kick.by.enemy", "hit_by_high_kick", 16)},
	{"uppercut", CollisionData(".successful.hit.left.by.enemy", "hit_by_high_punch", 15)},
	{"upper_high_kick", CollisionData(".successful.kick.by.enemy", "hit_by_high_kick", 10)},
	{"upper_right_high_punch", CollisionData(".enemy.hit.upper.punch", "hit_by_upper_punch", 9)},
	{"upper_left_high_punch", CollisionData(".enemy.hit.upper.punch", "hit_by_upper_punch", 9)},
	{"flip_low_kick", CollisionData(".successful.kick.by.enemy", "hit_by_high_kick", 12)},
	{"flip_upper_kick", CollisionData(".successful.kick.by.enemy", "hit_by_high_kick", 12)},
	{"down_high_kick", CollisionData(".successful.kick.by.enemy", "hit_by_high_kick", 12)},
	{"down_forward_high_kick", CollisionData(".successful.kick.by.enemy", "hit_by_high_kick", 12)},
	{"FREEZEBALL", CollisionData(".freeze.form", "frozen", 40)},
	{"LIGHTINGBOLT", CollisionData(".enemy.hit.upper.punch", "hit_by_upper_punch", 40)},
};

void SeparateSprites(Sprite* player, Sprite* enemy) {
	unsigned x1 = player->GetX();
	unsigned w1 = player->GetFrameBox().GetW() + player->GethFrameAllign();
	unsigned x2 = enemy->GetX();
	unsigned w2 = enemy->GetFrameBox().GetW() + enemy->GethFrameAllign();
	unsigned intersection = 0;
	if (x1 < x2) {
		intersection = x1 + w1 - x2;
		assert(intersection < SCREEN_WIDTH);
		if (x1 - intersection > 0)
			player->Move(-static_cast<int>(intersection), 0);
		else
			enemy->Move(static_cast<int>(intersection), 0);
	} else {
		intersection = x2 + w2 - x1;
		assert(intersection < SCREEN_WIDTH);
		if (x1 + intersection < SCREEN_WIDTH)
			player->Move(static_cast<int>(intersection), 0);
		else
			enemy->Move(-static_cast<int>(intersection), 0);
	}
}

void FighterToFighterCollisionHandler(Fighter* player, Fighter* enemy) {
	SeparateSprites(player->GetSprite(), enemy->GetSprite());
	FighterState playerState = player->GetState();
	FighterState enemyState = enemy->GetState();

	std::string enemyFilm = "";
	std::string newEnemyState = "";
	int enemyDamage = 0;

	std::string playerFilm = "";
	std::string newPlayerState = "";
	int playerDamage = 0;

	//Player 1 (Raiden) performs fatality
	if (enemy->GetFinisher() && playerState == ATTACK) {
		player->GetStateTransitions()->SetState("Fatality");
		enemy->StopAnimators();
		enemy->GetStateTransitions()->SetState("Being_Executed");

		player->SetState(ATTACKED);
		player->ForceFrameRangeAnimation("raiden.electric.decapitation");
		enemy->ForceFrameRangeAnimation("subzero.fatality.successful");

		return;
	} else if (player->GetFinisher() && enemyState == ATTACK) {
		enemy->GetStateTransitions()->SetState("Fatality");
		player->StopAnimators();
		player->GetStateTransitions()->SetState("Being_Executed");

		enemy->SetState(ATTACKED);
		enemy->ForceFrameListAnimation("subzero.cast.freezeball");
		player->ForceFrameRangeAnimation("raiden.fatality.successful");

		return;
	}

	auto i = CollisionFilmMap.find(player->GetStateTransitions()->GetState());
	if (i != CollisionFilmMap.end()) {
		enemyFilm = i->second.GetFilmName();
		newEnemyState = i->second.GetNewForcedState();
		enemyDamage = i->second.GetDamage();
	}

	auto j = CollisionFilmMap.find(enemy->GetStateTransitions()->GetState());
	if (j != CollisionFilmMap.end()) {
		playerFilm = j->second.GetFilmName();
		newPlayerState = j->second.GetNewForcedState();
		playerDamage = j->second.GetDamage();
	}

	switch (playerState) {
		case NORMAL:
			switch (enemyState) {
				case NORMAL: break;
				case ATTACK:
					cout << "player2 hit player1" << endl;
					//stun animation
					player->GetStateTransitions()->SetState(newPlayerState);
					player->ForceFrameRangeAnimation(player->GetName() + playerFilm);
					//sound effect
					//SoundMixer::AttachAudioInstanceToMixer(player->GetName() + ".kicked");
					//health reduction
					player->ReduceHealth(playerDamage);
					//prevent collision repeat
					enemy->SetState(ATTACKED);
					break;
				case ATTACKED: break;
				case BLOCK: break;
				default: assert(0); break;
			}
			break;
		case ATTACK:
			switch (enemyState) {
				case NORMAL:
					cout << "player1 hit player2" << endl;
					//stun animation
					assert(i != CollisionFilmMap.end());
					enemy->GetStateTransitions()->SetState(newEnemyState);
					enemy->ForceFrameRangeAnimation(enemy->GetName() + enemyFilm);
					//sound effect
					SoundMixer::PlayAudioInstance(enemy->GetName() + ".kicked");

					//health reduction
					enemy->ReduceHealth(enemyDamage);
					//prevent collision repeat
					player->SetState(ATTACKED);
					break;
				case ATTACK:
					cout << "player2 hit player1" << endl;
					//stun animation
					assert(i != CollisionFilmMap.end());
					assert(j != CollisionFilmMap.end());
					player->GetStateTransitions()->SetState(newPlayerState);
					player->ForceFrameRangeAnimation(player->GetName() + playerFilm);
					enemy->GetStateTransitions()->SetState(newEnemyState);
					enemy->ForceFrameRangeAnimation(enemy->GetName() + enemyFilm);
					//sound effect
					SoundMixer::PlayAudioInstance(enemy->GetName() + ".kicked");
					//health reduction
					player->ReduceHealth(5);
					enemy->ReduceHealth(5);
					//prevent collision repeat
					player->SetState(ATTACKED);
					enemy->SetState(ATTACKED);
					//health reduction
					break;
				case ATTACKED:
					cout << "player1 hit player2(attacked)" << endl;
					//stun animation
					assert(i != CollisionFilmMap.end());
					enemy->GetStateTransitions()->SetState(newEnemyState);
					enemy->ForceFrameRangeAnimation(enemy->GetName() + enemyFilm);
					//health reduction
					enemy->ReduceHealth(5);
					//sound effect
					SoundMixer::PlayAudioInstance(enemy->GetName() + ".kicked");
					//prevent collision repeat
					player->SetState(ATTACKED);
					break;
				case BLOCK:
					cout << "player2 blocked player1" << endl;
					//prevent collision repeat
					player->SetState(ATTACKED);
					break;
				default: assert(0); break;
			}
			break;
		case ATTACKED:
			switch (enemyState) {
				case NORMAL: break;
				case ATTACK:
					cout << "player2 hit player1(attacked)" << endl;
					//stun animation
					assert(j != CollisionFilmMap.end());
					player->GetStateTransitions()->SetState(newPlayerState);
					player->ForceFrameRangeAnimation(player->GetName() + playerFilm);
					//sound effect
					SoundMixer::PlayAudioInstance(enemy->GetName() + ".kicked");
					//health reduction
					player->ReduceHealth(5);
					//prevent collision repeat
					enemy->SetState(ATTACKED);
					break;
				case ATTACKED: break;
				case BLOCK: break;
				default: assert(0); break;
			}
			break;
		case BLOCK:
			switch (enemyState) {
				case NORMAL: break;
				case ATTACK:
					cout << "player1 blocked player2" << endl;
					//sound effect
					SoundMixer::PlayAudioInstance(enemy->GetName() + ".punched");
					//prevent collision repeat
					enemy->SetState(ATTACKED);
					break;
				case ATTACKED: break;
				case BLOCK: break;
				default: assert(0); break;
			}
			break;
		default: assert(0); break;
	}
}

/* CollisionHandler::CollisionHandler() {}
CollisionHandler::~CollisionHandler() {}

FightersCollisionHandler::FightersCollisionHandler() {}
FightersCollisionHandler::~FightersCollisionHandler() {}
CollisionHandler* FightersCollisionHandler::Clone(void) const {
	return (new FightersCollisionHandler());
}
void FightersCollisionHandler::operator()(Sprite* player, Sprite* enemy) const {
	if (player->GetType() != FIGHTER || enemy->GetType() != FIGHTER) {
		cout << "FightersCollisionHandler, Sprites are not fighters" << endl;
		return;
	}
	SeparateSprites(player, enemy);
	SpriteState playerState = player->GetState();
	SpriteState enemyState = enemy->GetState();
	switch (playerState) {
		case NORMAL:
			switch (enemyState) {
				case NORMAL: break;
				case ATTACK:
					cout << "player2 hit player1" << endl;
					player->SetState(STUN);	//signal animation
					enemy->SetState(NORMAL);
					//health reduction
					break;
				case BLOCK: break;
				case STUN: break;
				default: assert(0); break;
			}
			break;
		case ATTACK:
			switch (enemyState) {
				case NORMAL:
					cout << "player1 hit player2" << endl;
					enemy->SetState(STUN);	//signal animation
					player->SetState(NORMAL);
					//health reduction
					break;
				case ATTACK:
					cout << "player2 hit player1" << endl;
					player->SetState(STUN);	//signal animation
					enemy->SetState(STUN);	 //signal animation
					//health reduction
					break;
				case BLOCK:
					cout << "player2 blocked player1" << endl;
					player->SetState(NORMAL);
					//signal block animation
					break;
				case STUN:
					cout << "player1 hit player2(stunned)" << endl;
					player->SetState(STUN);	//signal animation
					enemy->SetState(NORMAL);
					//health reduction
					break;
				default: assert(0); break;
			}
			break;
		case BLOCK:
			switch (enemyState) {
				case NORMAL: break;
				case ATTACK:
					cout << "player1 blocked player2" << endl;
					// enemy->SetState(NORMAL);
					//signal block animation
					break;
				case BLOCK: break;
				case STUN: break;
				default: assert(0); break;
			}
			break;
		case STUN:
			switch (enemyState) {
				case NORMAL: break;
				case ATTACK:
					cout << "player2 hit player1(stunned)" << endl;
					player->SetState(STUN);	//signal animation
					enemy->SetState(NORMAL);
					//signal stun animation
					//health reduction
					break;
				case BLOCK: break;
				case STUN: break;
				default: assert(0); break;
			}
			break;
		default: assert(0); break;
	}
} */
void ProjectileToFighterCollisionHandler(Fighter* player, Fighter* enemy) {
	auto i = CollisionFilmMap.find(player->GetProjectile()->GetName());
	assert(i != CollisionFilmMap.end());
	std::string enemyFilm = i->second.GetFilmName();
	std::string newEnemyState = i->second.GetNewForcedState();
	int enemyDamage = i->second.GetDamage();

	enemy->GetStateTransitions()->SetState(newEnemyState);
	enemy->ForceFrameRangeAnimation(enemy->GetName() + enemyFilm);
	//health reduction
	enemy->ReduceHealth(enemyDamage);

	// Mark to delete projectile
	player->GetProjectile()->toBeDeleted = true;
	player->GetProjectile()->GetAnimator()->Stop();

	// Change player state
	// player->GetStateTransitions()->SetState("rewind." + player->GetStateTransitions()->GetState());	//rewind state: rewind.<player_projectile_state>
}

void CheckFatality(Fighter* player, Fighter* enemy) {
	if (player->GetHealth() <= 0 && player->GetStateTransitions()->GetState() != ("fatality_start") && !player->GetFinisher()) {
		string playerFilm = player->GetName() + ".fatality.start";
		player->StopAnimators();
		player->GetStateTransitions()->SetState("fatality_start");
		player->GetSprite()->SetFilm(AnimationFilmHolder::GetFilm(playerFilm));
		player->SetFinisher(true);
		player->GetFrameRangeAnimator()->Start(player->GetSprite(), AnimationDataHolder::GetFrameRangeAnimation(playerFilm), GetGameTime());
	} else if (enemy->GetHealth() <= 0 && enemy->GetStateTransitions()->GetState() != ("fatality_start") && !enemy->GetFinisher()) {
		std::string enemyFilm = enemy->GetName() + ".fatality.start";
		enemy->StopAnimators();
		enemy->GetStateTransitions()->SetState("fatality_start");
		enemy->GetSprite()->SetFilm(AnimationFilmHolder::GetFilm(enemyFilm));
		enemy->SetFinisher(true);
		// enemy->GetFrameRangeAnimator()->Start(enemy->GetSprite(), AnimationDataHolder::GetFrameRangeAnimation(enemyFilm), GetGameTime());
		enemy->ForceFrameRangeAnimation(enemyFilm);
		enemy->SetState(ATTACKED);
	}
}
