IDIR=includes
ODIR=objects
_DEPS = Wrappers.h Utilities.h ConfigParser.h BitmapLoader.h AnimationFilmHolder.h\
Bitmap.h AnimationFilm.h Animation.h MovingAnimation.h FrameRangeAnimation.h FrameListAnimation.h\
MovingPathAnimation.h TickAnimation.h Animator.h MovingAnimator.h FrameRangeAnimator.h\
FrameListAnimator.h MovingPathAnimator.h TickAnimator.h InputController.h StateTransitions.h\
Sprite.h Fighter.h AnimationDataHolder.h CollisionHandlers.h SoundMixer.h EventQueue.h\
PauseGame.h Projectile.h
_OBJ = main.o Wrappers.o Utilities.o ConfigParser.o BitmapLoader.o AnimationFilmHolder.o\
Bitmap.o AnimationFilm.o Animation.o MovingAnimation.o FrameRangeAnimation.o FrameListAnimation.o\
MovingPathAnimation.o TickAnimation.o Animator.o MovingAnimator.o FrameRangeAnimator.o\
FrameListAnimator.o MovingPathAnimator.o TickAnimator.o InputController.o StateTransitions.o\
Sprite.o Fighter.o AnimationDataHolder.o CollisionHandlers.o SoundMixer.o EventQueue.o\
PauseGame.o Projectile.o

DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))
LIBNAMES = -lallegro -lallegro_dialog -lallegro_primitives\
-lallegro_font -lallegro_ttf -lallegro_color -lallegro_image\
-lallegro_acodec -lallegro_audio

CXX=g++
WFLAGS=-Wall -Wextra -Wcast-align -Wcast-qual -Wctor-dtor-privacy -Wformat=2 -Winit-self\
-Wlogical-op -Wmissing-include-dirs -Wold-style-cast -Woverloaded-virtual -Wredundant-decls\
-Wshadow -Wsign-conversion -Wsign-promo -Wswitch-default -Wswitch-enum  -Wundef -Wformat-nonliteral\
-Winvalid-pch -Wstrict-overflow=5 -Wmissing-declarations -Wstrict-null-sentinel -Wdisabled-optimization
DFLAGS=
# DFLAGS=-DNDEBUG
CXXFLAGS=-g3 -O0 -std=c++11 $(DFLAGS) $(WFLAGS) $(LIBNAMES) -I$(IDIR)

.PHONY: clean
default: main

$(ODIR)/%.o: %.cpp $(DEPS)
	@$(CXX) -c -o $@ $< $(CXXFLAGS)

main: $(OBJ)
	@$(CXX) -o $@ $^ $(CXXFLAGS)

clean:
	rm -f $(ODIR)/*.o
	rm -f main
