#include "includes/Bitmap.h"
// #include <iostream>
#include <sstream>
#include "includes/Utilities.h"
#include "includes/Wrappers.h"

static bool IsValidRect(const Rect& from, const unsigned BitmapWitdth, const unsigned BitmapHeight) {
	// X axis out of bound check
	if (from.GetW() > BitmapWitdth - from.GetStart().GetX()) return false;
	// Y axis out of bound check
	if (from.GetH() > BitmapHeight - from.GetStart().GetY()) return false;
	return true;
}

Bitmap BitmapAPI::Create(unsigned width, unsigned height) {
	return wrap_create_bitmap(static_cast<int>(width), static_cast<int>(height));
}

bool BitmapAPI::Destroy(Bitmap bmp) {
	if (!bmp) return false;
	wrap_destroy_bitmap(bmp);
	return true;
}

Bitmap BitmapAPI::GetScreen() {
	return wrap_get_backbuffer(wrap_get_current_display());
}

unsigned BitmapAPI::GetWidth(Bitmap bmp) {
	return static_cast<unsigned>(wrap_get_bitmap_width(bmp));
}

unsigned BitmapAPI::GetHeight(Bitmap bmp) {
	return static_cast<unsigned>(wrap_get_bitmap_height(bmp));
}

void BitmapAPI::Blit(Bitmap src, const Rect& from, Point paintCoords) {
	Blit(src, from, paintCoords, 0);
}

void BitmapAPI::Blit(Bitmap src, const Rect& from, Point paintCoords, int flags) {
	if (!IsValidRect(from, BitmapAPI::GetWidth(src), BitmapAPI::GetHeight(src))) {
		std::ostringstream str;
		str << "Warning: Rect" << from.RectStringify() << " exceeds bitmap grid size";
		PrintWarning(str.str());
	}

	Bitmap subBitmap = wrap_create_sub_bitmap(src, static_cast<int>(from.GetStart().GetX()), static_cast<int>(from.GetStart().GetY()), static_cast<int>(from.GetW()), static_cast<int>(from.GetH()));
	wrap_draw_bitmap(subBitmap, static_cast<float>(paintCoords.GetX()), static_cast<float>(paintCoords.GetY()), flags);
}

void BitmapAPI::Blit(Bitmap src, const Rect& from, Bitmap dest, Point paintCoords) {
	Blit(src, from, dest, paintCoords, 0);
}

void BitmapAPI::Blit(Bitmap src, const Rect& from, Bitmap dest, Point paintCoords, int flags) {
	if (src == dest) {	//src and dest bitmaps cannot be the same
		PrintError("In BitmapAPI in function Blit: Target Bitmap cannot be the same as source Bitmap");
	}

	if (!IsValidRect(from, BitmapAPI::GetWidth(src), BitmapAPI::GetHeight(src))) {
		std::ostringstream str;
		str << "Warning: Rect" << from.RectStringify() << "exceeds bitmap grid size";
		PrintWarning(str.str());
	}

	Bitmap temp = wrap_get_target_bitmap();
	wrap_set_target_bitmap(dest);
	Bitmap subBitmap = wrap_create_sub_bitmap(src, static_cast<int>(from.GetStart().GetX()), static_cast<int>(from.GetStart().GetY()), static_cast<int>(from.GetW()), static_cast<int>(from.GetH()));
	// Draw_scaled_rotated_bitmap() was used in order to scale the characters sub-bitmaps, for better visuals.
	wrap_draw_scaled_rotated_bitmap(subBitmap, 0, 0,
									static_cast<float>(paintCoords.GetX()), static_cast<float>(paintCoords.GetY()),
									2.5, 2.5, 0, flags);
	//wrap_draw_bitmap(subBitmap, static_cast<float>(paintCoords.GetX()), static_cast<float>(paintCoords.GetY()), flags);
	al_set_target_bitmap(temp);	//reset the target bitmap to its previous bitmap
}

void BitmapAPI::SetTargetBitmap(Bitmap bmp) {
	wrap_set_target_bitmap(bmp);
}

Bitmap BitmapAPI::GetTargetBitmap() {
	return wrap_get_target_bitmap();
}
