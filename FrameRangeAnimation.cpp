#include "FrameRangeAnimation.h"

using namespace std;
FrameRangeAnimation::FrameRangeAnimation(animd_t _id, offset_t _dx, offset_t _dy, delay_t _delay, bool _continuous, frame_t _start, frame_t _end)
	: MovingAnimation(_id, _dx, _dy, _delay, _continuous), start(_start), end(_end) {}
FrameRangeAnimation::~FrameRangeAnimation() {}
Animation* FrameRangeAnimation::Clone(animd_t newId) const {
	return (new FrameRangeAnimation(newId, GetDx(), GetDy(), GetDelay(), GetContinuous(), start, end));
}
frame_t FrameRangeAnimation::GetStart(void) const {
	return start;
}
void FrameRangeAnimation::SetStart(frame_t _start) {
	start = _start;
}
frame_t FrameRangeAnimation::GetEnd(void) const {
	return end;
}
void FrameRangeAnimation::SetEnd(frame_t _end) {
	end = _end;
}
