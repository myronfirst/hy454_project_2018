#include "SoundMixer.h"
#include <cassert>
#include <iostream>
#include "ConfigParser.h"

using namespace std;

SoundMixer::AudioSampleHolder SoundMixer::audioSamples;
SoundMixer::AudioInstanceHolder SoundMixer::audioInstances;

void SoundMixer::Initialize(void) {
	//wrap_install(uninstall)_audio on main
}
void SoundMixer::CleanUp(void) {
	//clear all audioSamples
	for (AudioSampleHolder::iterator i = audioSamples.begin(); i != audioSamples.end(); i++) {
		wrap_destroy_sample(i->second);
	}
	audioSamples.clear();

	//clear all audioInstances
	for (AudioInstanceHolder::iterator i = audioInstances.begin(); i != audioInstances.end(); i++) {
		wrap_destroy_sample_instance(i->second);
	}
	audioInstances.clear();
}

void SoundMixer::LoadAudioSamples(const std::string& path) {
	std::string audioID("");
	std::string audioPath("");

	ConfigParser parser(path);
	wrap_reserve_samples(1);

	assert(parser.GetConfigFirstSection().empty());	//must be default section
	for (std::string section = parser.GetConfigNextSection(); !section.empty(); section = parser.GetConfigNextSection()) {
		std::string key = parser.GetConfigFirstKey(section);	//audioID

		audioID = parser.GetConfigValue(section, key);

		key = parser.GetConfigNextKey();	// audioPath
		audioPath = parser.GetConfigValue(section, key);

		audioSamples.insert(std::pair<std::string, AudioSample>(audioID, wrap_load_sample(audioPath.c_str())));
		// cout << "Audio Sample Created with id: " + audioID + "\n";
		SoundMixer::CreateAudioSampleInstance(audioID);
		SoundMixer::AttachAudioInstanceToMixer(audioID);
	}
}

AudioSample SoundMixer::GetAudioSample(const std::string id) {
	AudioSampleHolder::const_iterator i = audioSamples.find(id);
	return (i != audioSamples.end() ? i->second : nullptr);
}

AudioInstance SoundMixer::GetAudioInstance(const std::string id) {
	AudioInstanceHolder::const_iterator i = audioInstances.find(id);
	return (i != audioInstances.end() ? i->second : nullptr);
}

void SoundMixer::CreateAudioSampleInstance(const std::string id) {
	AudioSample sample = SoundMixer::GetAudioSample(id);
	assert(sample != nullptr);
	AudioInstance instance;
	instance = wrap_create_sample_instance(sample);
	audioInstances.insert(std::pair<std::string, AudioInstance>(id, instance));
	//cout << "Audio Sample Instance Created with id: " + id + "\n";
}

void SoundMixer::AttachAudioInstanceToMixer(const std::string id) {
	AudioInstance instance = SoundMixer::GetAudioInstance(id);
	//cout << "Instance:" + id;
	assert(instance != nullptr);
	if (id == "theme.song") wrap_set_sample_instance_playmode(instance, ALLEGRO_PLAYMODE_LOOP);
	wrap_attach_sample_instance_to_mixer(SoundMixer::GetAudioInstance(id), al_get_default_mixer());
}

void SoundMixer::PlayAudioSample(const std::string id) {
	AudioSample sample = SoundMixer::GetAudioSample(id);
	assert(sample != nullptr);
	cout << "Audio Sample with ID: " + id + " playing!\n";

	wrap_play_sample(sample, 1.0, 0.0, 1.0, ALLEGRO_PLAYMODE_ONCE, 0);
}

void SoundMixer::PlayAudioInstance(const std::string id) {
	AudioInstance instance = SoundMixer::GetAudioInstance(id);
	assert(instance != nullptr);
	cout << "Audio Sample with ID: " + id + " playing!\n";
	//SoundMixer::AttachAudioInstanceToMixer(id);
	wrap_play_sample_instance(instance);
}
