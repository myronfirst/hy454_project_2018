#include "Animator.h"

using namespace std;
// Animator::Animator(void)
// : lastTime(0), state(ANIMATOR_FINISHED), onFinish(nullptr), finishClosure(nullptr) {}
Animator::Animator(void)
	: lastTime(0), state(ANIMATOR_FINISHED), onFinish(nullptr) {}

Animator::~Animator(){};

// void Animator::SetOnFinish(FinishCallBack f, void* c = nullptr) {
// 	onFinish = f;
// 	finishClosure = c;
// }
void Animator::SetOnFinish(FinishCallBack f) {
	onFinish = f;
}

bool Animator::IsSuspended(void) const {
	return state != ANIMATOR_RUNNING;
}

void Animator::Stop(void) {
	if (!IsSuspended()) {
		state = ANIMATOR_STOPPED;
		NotifyStopped();
	}
}

// void Animator::NotifyStopped(void) {
// 	//assert(ofFinish); ?????
// 	if (onFinish) {
// 		(onFinish)(this, finishClosure);
// 	}
// }
void Animator::NotifyStopped(void) {
	//assert(ofFinish); ?????
	if (onFinish) {
		onFinish(this);
	}
}

void Animator::TimeShift(timestamp_t offset) {
	lastTime += offset;
}
