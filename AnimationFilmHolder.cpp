#include "AnimationFilmHolder.h"

#include "ConfigParser.h"
#include "Utilities.h"

#include <sstream>

using namespace std;
AnimationFilmHolder::Films AnimationFilmHolder::films;
BitmapLoader AnimationFilmHolder::bitmaps;
void AnimationFilmHolder::Initialize(void) {}
void AnimationFilmHolder::CleanUp(void) {
	for (Films::iterator i = films.begin(); i != films.end(); ++i) {
		delete (i->second);
	}
	films.clear();
}
void AnimationFilmHolder::Load(const std::string& catalogue) {
	string id("");
	string path("");
	vector<Rect> rects;
	ConfigParser parser(catalogue);

	assert(parser.GetConfigFirstSection().empty());	//must be default section
	for (string section = parser.GetConfigNextSection(); !section.empty(); section = parser.GetConfigNextSection()) {
		string key = parser.GetConfigFirstKey(section);	//id
		// str << parser.GetConfigValue(section, key);
		// str >> id;
		id = parser.GetConfigValue(section, key);

		key = parser.GetConfigNextKey();	//path
		// str << parser.GetConfigValue(section, key);
		// str >> path;
		path = parser.GetConfigValue(section, key);

		//frames
		for (key = parser.GetConfigNextKey(); !key.empty(); key = parser.GetConfigNextKey()) {
			stringstream str;
			str << parser.GetConfigValue(section, key);
			array<unsigned, 4> num = {0};
			for (auto it = num.begin(); it != num.end(); ++it) {
				str >> *it;
			}
			rects.push_back(Rect(Point(num.at(0), num.at(1)), num.at(2), num.at(3)));
		}
		//Load bitmap and construct film
		Bitmap bmp = bitmaps.Load(path);
		assert(!GetFilm(id));	//id should not exist
		films[id] = new AnimationFilm(id, bmp, rects);
		id.clear();
		path.clear();
		rects.clear();
	}
}
const AnimationFilm* AnimationFilmHolder::GetFilm(const std::string& id) {
	Films::const_iterator i = films.find(id);
	return (i != films.end() ? i->second : nullptr);
}
