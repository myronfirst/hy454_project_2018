
#include "AnimationFilm.h"
#include <cassert>

AnimationFilm::AnimationFilm(const std::string& _id, Bitmap _bitmap, const std::vector<Rect> _rects)
	: id(_id), bitmap(_bitmap), rects(_rects) {}
AnimationFilm::~AnimationFilm() {}
unsigned AnimationFilm::GetTotalFrames(void) const {
	return rects.size();
}
Bitmap AnimationFilm::GetBitmap(void) const {
	return bitmap;
}
const std::string AnimationFilm::AnimationFilm::GetId(void) const {
	return id;
}
const Rect AnimationFilm::GetFrameBox(unsigned frameNo) const {
	assert(rects.size() > frameNo);
	return rects[frameNo];
}
void AnimationFilm::DisplayFrame(Bitmap dest, const Point& at, unsigned frameNo) const {	// add const later
	// MaskedBlit(bitmap, GetFrameBox(frameNo), dest, at);
	BitmapAPI::Blit(bitmap, GetFrameBox(frameNo), dest, at);
}
