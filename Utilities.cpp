#include "Utilities.h"

#include <iostream>
#include <sstream>

using namespace std;

const unsigned SCREEN_WIDTH = 800;
const unsigned SCREEN_HEIGHT = 600;
const int GRID_SIZE = 256;

unsigned long GetGameTime() {
	return static_cast<unsigned long>(wrap_get_time() * 1e6);	//milli seconds
}

void PrintWarning(std::string msg) {
	cout << msg << endl;
}
void PrintError(std::string msg) {
	PrintWarning(msg);
	exit(-1);
}

//Point
Point::Point()
	: x(0), y(0) {}
Point::Point(unsigned _x, unsigned _y)
	: x(_x), y(_y) {}
Point::Point(const Point& p)
	: x(p.x), y(p.y) {}
Point::~Point() {}
void Point::SetX(unsigned _x) {
	x = _x;
}
unsigned Point::GetX() const {
	return x;
}
void Point::SetY(unsigned _y) {
	y = _y;
}
unsigned Point::GetY() const {
	return y;
}
std::string Point::PointStringify(void) const {
	std::ostringstream str;
	str << "X: " << x << " Y: " << y;
	return str.str();
}

//Rect object Implementation
Rect::Rect()
	: start(0, 0), width(0), height(0) {}
Rect::Rect(Point x_y, unsigned _w, unsigned _h)
	: start(x_y), width(_w), height(_h) {}
Rect::~Rect() {}

void Rect::PrintRect(void) const {
	cout << start.PointStringify() << " W: " << width << " H: " << height << endl;
}
std::string Rect::RectStringify(void) const {
	std::ostringstream str;
	str << start.PointStringify() << " W: " << width << " H: " << height;
	return str.str();
}
void Rect::SetStart(Point p) {
	start = p;
}
Point Rect::GetStart(void) const {
	return start;
}
void Rect::SetW(unsigned w) {
	width = w;
}
unsigned Rect::GetW(void) const {
	return width;
}
void Rect::SetH(unsigned h) {
	height = h;
}
unsigned Rect::GetH(void) const {
	return height;
}
