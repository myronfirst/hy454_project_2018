#include <iostream>
#include <string>
#include "AnimationDataHolder.h"

using namespace std;
int main() {
	string path = "./assets/Raiden/Template_Raiden_AnimationData.ini";

	AnimationDataHolder::Initialize();
	AnimationDataHolder::Load(path);

	std::cout << "ID: " << (AnimationDataHolder::GetMovingAnimation("raiden.start.position"))->GetId() << std::endl;
	std::cout << "ID: " << (AnimationDataHolder::GetFrameRangeAnimation("raiden.walking.forward"))->GetId() << std::endl;
	std::cout << "ID: " << (AnimationDataHolder::GetFrameListAnimation("raiden.a.framelistanimation"))->GetId() << std::endl;
	std::cout << "ID: " << (AnimationDataHolder::GetMovingPathAnimation("raiden.lowkick"))->GetId() << std::endl;
	AnimationDataHolder::CleanUp();

	return 0;
}
