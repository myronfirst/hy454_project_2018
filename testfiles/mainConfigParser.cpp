#include <iostream>
#include <string>
#include "ConfigParser.h"

using namespace std;
int main() {
	string path = "./assets/SubZero/SubZero.ini";
	ConfigParser parser(path);
	parser.LoadConfigFile(path);
	ALLEGRO_CONFIG* file = parser.GetConfigFile();
	cout << parser.GetConfigValue("subzero.idle", "id") << endl;

	string section = parser.GetConfigFirstSection();
	cout << "First Section, must be empty:" << section << endl;
	section = parser.GetConfigNextSection();
	while (!section.empty()) {
		cout << "section:" << section << endl;
		string key = parser.GetConfigFirstKey(section);
		while (!key.empty()) {
			string value = parser.GetConfigValue(section, key);
			cout << key << "-->" << value << endl;
			key = parser.GetConfigNextKey();
		}
		section = parser.GetConfigNextSection();
	}
	parser.LoadConfigFile("");
	return 0;
}
