#include "AnimationFilmHolder.h"
#include "BitmapLoader.h"
#include "Wrappers.h"

#include <allegro5/allegro.h>
#include <iostream>
#include <string>

using namespace std;
int main() {
	wrap_init();
	wrap_init_image_addon();

	ALLEGRO_DISPLAY* display = wrap_create_display(640, 480);
	wrap_clear_to_color(wrap_map_rgb(0, 0, 0));
	cout << "Hello There!" << endl;
	Bitmap screen = BitmapAPI::GetScreen();

	BitmapLoader::Initialize();
	AnimationFilmHolder::Initialize();
	AnimationFilmHolder::Load(string("./assets/SubZero/SubZero.ini"));
	AnimationFilmHolder::GetFilm(string("subzero.idle"))->DisplayFrame(screen, Point(10, 10), 0);
	for (unsigned i = 0; i < 12; ++i) {
		wrap_clear_to_color(wrap_map_rgb(0, 0, 0));
		AnimationFilmHolder::GetFilm(string("subzero.forward"))->DisplayFrame(screen, Point(50, 50), i);
		wrap_flip_display();
		wrap_rest(0.25);
	}

	wrap_flip_display();
	wrap_clear_to_color(wrap_map_rgb(0, 0, 0));
	wrap_rest(20.0);

	AnimationFilmHolder::CleanUp();
	BitmapLoader::CleanUp();
	wrap_destroy_display(display);
	return 0;
}
