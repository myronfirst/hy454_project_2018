#include "AnimationFilmHolder.h"
#include "BitmapLoader.h"
#include "Sprite.h"
#include "Wrappers.h"

#include <allegro5/allegro.h>
#include <allegro5/allegro_primitives.h>
#include <iostream>
#include <sstream>
#include <string>

using namespace std;
int main() {
	wrap_init();
	wrap_init_image_addon();
	wrap_init_primitives_addon();
	ALLEGRO_DISPLAY* display = wrap_create_display(1280, 720);
	wrap_set_window_title(display, "Sprites Collision Check");
	// color = white;
	wrap_clear_to_color(wrap_map_rgb(0, 0, 0));
	Bitmap screen = BitmapAPI::GetScreen();

	BitmapLoader::Initialize();
	AnimationFilmHolder::Initialize();
	Sprite *player_1, *player_2;

	AnimationFilmHolder::Load(string("./assets/Raiden/Raiden.ini"));
	AnimationFilmHolder::Load(string("./assets/SubZero/SubZero.ini"));

	player_1 = new Sprite(300, 350, AnimationFilmHolder::GetFilm(string("subzero.grabbed.by.enemy")));
	player_2 = new Sprite(400, 350, AnimationFilmHolder::GetFilm(string("raiden.grab.enemy")));

	while (1) {
		for (unsigned i = 0; i < 7; ++i) {
			wrap_clear_to_color(wrap_map_rgb(0, 0, 0));

			player_1->SetFrame(i);
			player_2->SetFrame(i);
			player_1->Display(BitmapAPI::GetTargetBitmap(), 0);
			player_2->Display(BitmapAPI::GetTargetBitmap(), 1);
			//AnimationFilmHolder::GetFilm(string("raiden.walking.forward"))->DisplayFrame(screen, Point(300, 350), i);
			wrap_flip_display();
			wrap_rest(0.25);

			ostringstream str;

			if (player_1->CollisionCheck(player_2)) {
				str << "Player 1 collide on player 2 at frame: " << static_cast<int>(player_1->GetFrameNo()) << "\n";
				cout << str.str();
			} else {
				str << "No collision: " << static_cast<int>(player_1->GetFrameNo()) << "\n";
				cout << str.str();
			}
			//player_1->Move(8, 0);
		}
	}

	wrap_flip_display();
	wrap_clear_to_color(wrap_map_rgb(0, 0, 0));
	wrap_rest(2.0);

	AnimationFilmHolder::CleanUp();
	BitmapLoader::CleanUp();
	wrap_destroy_display(display);
	return 0;
}
