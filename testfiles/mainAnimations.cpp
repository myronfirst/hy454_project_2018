#include "FrameListAnimation.h"
#include "FrameRangeAnimation.h"
#include "MovingPathAnimation.h"

#include <iostream>
#include <sstream>
using namespace std;
int main() {
	ostringstream oss;
	MovingAnimation moveAnim(0, 6, 6, 1, false);
	moveAnim.SetDx(1);
	moveAnim.SetDy(1);
	moveAnim.SetDelay(1);
	moveAnim.SetContinuous(true);
	cout << "moveAnim("
		 << moveAnim.GetId() << ","
		 << moveAnim.GetDx() << ","
		 << moveAnim.GetDy() << ","
		 << moveAnim.GetDelay() << ","
		 << moveAnim.GetContinuous()
		 << ")" << endl;
	MovingAnimation* moveClone = static_cast<MovingAnimation*>(moveAnim.Clone(9));
	cout << "moveClone("
		 << moveClone->GetId() << ","
		 << moveClone->GetDx() << ","
		 << moveClone->GetDy() << ","
		 << moveClone->GetDelay() << ","
		 << moveClone->GetContinuous()
		 << ")" << endl;

	FrameRangeAnimation frameRangeAnim(0, 6, 6, 1, false, 0, 4);
	frameRangeAnim.SetDx(1);
	frameRangeAnim.SetDy(1);
	frameRangeAnim.SetDelay(1);
	frameRangeAnim.SetContinuous(true);
	frameRangeAnim.SetStart(10);
	frameRangeAnim.SetEnd(14);
	cout << "frameRangeAnim("
		 << frameRangeAnim.GetId() << ","
		 << frameRangeAnim.GetDx() << ","
		 << frameRangeAnim.GetDy() << ","
		 << frameRangeAnim.GetDelay() << ","
		 << frameRangeAnim.GetContinuous() << ","
		 << frameRangeAnim.GetStart() << ","
		 << frameRangeAnim.GetEnd() << ","
		 << ")" << endl;
	FrameRangeAnimation* frameRangeClone = static_cast<FrameRangeAnimation*>(frameRangeAnim.Clone(99));
	cout << "frameRangeClone("
		 << frameRangeClone->GetId() << ","
		 << frameRangeClone->GetDx() << ","
		 << frameRangeClone->GetDy() << ","
		 << frameRangeClone->GetDelay() << ","
		 << frameRangeClone->GetContinuous() << ","
		 << frameRangeClone->GetStart() << ","
		 << frameRangeClone->GetEnd() << ","
		 << ")" << endl;

	FrameListAnimation frameListAnim(0, 6, 6, 1, false, {0, 2, 4});
	frameListAnim.SetDx(1);
	frameListAnim.SetDy(1);
	frameListAnim.SetDelay(1);
	frameListAnim.SetContinuous(true);
	frameListAnim.SetFrames({1, 3, 5});
	oss.str("");
	oss.clear();
	oss << "{";
	for (auto it = frameListAnim.GetFrames().begin(); it != frameListAnim.GetFrames().end(); ++it) {
		oss << *it;
	}
	oss << "}";
	cout << "frameListAnim("
		 << frameListAnim.GetId() << ","
		 << frameListAnim.GetDx() << ","
		 << frameListAnim.GetDy() << ","
		 << frameListAnim.GetDelay() << ","
		 << frameListAnim.GetContinuous() << ","
		 << oss.str() << ","
		 << ")" << endl;
	FrameListAnimation* frameListClone = static_cast<FrameListAnimation*>(frameListAnim.Clone(999));
	oss.str("");
	oss.clear();
	oss << "{";
	for (auto it = frameListClone->GetFrames().begin(); it != frameListClone->GetFrames().end(); ++it) {
		oss << *it;
	}
	oss << "}";
	cout << "frameListClone("
		 << frameListClone->GetId() << ","
		 << frameListClone->GetDx() << ","
		 << frameListClone->GetDy() << ","
		 << frameListClone->GetDelay() << ","
		 << frameListClone->GetContinuous() << ","
		 << oss.str() << ","
		 << ")" << endl;

	MovingPathAnimation movingPathAnim(0, false, {PathEntry(20, 20, 4, 4000000u), PathEntry(10, 10, 2, 1000000u), PathEntry(-10, -10, 1, 1000000u)});
	movingPathAnim.SetContinuous(true);
	movingPathAnim.SetPath({PathEntry(20, 20, 4, 4000000u), PathEntry(10, 10, 2, 1000000u), PathEntry(-10, -10, 1, 1000000u)});
	oss.str("");
	oss.clear();
	oss << "{";
	for (auto it = movingPathAnim.GetPath().begin(); it != movingPathAnim.GetPath().end(); ++it) {
		oss << "(" << it->dx << "," << it->dy << "," << it->frame << "," << it->delay << ")";
	}
	oss << "}";
	cout << "movingPathAnim("
		 << movingPathAnim.GetId() << ","
		 << movingPathAnim.GetContinuous() << ","
		 << oss.str() << ","
		 << ")" << endl;
	MovingPathAnimation* movingPathClone = static_cast<MovingPathAnimation*>(movingPathAnim.Clone(9999));
	oss.str("");
	oss.clear();
	oss << "{";
	for (auto it = movingPathClone->GetPath().begin(); it != movingPathClone->GetPath().end(); ++it) {
		oss << "(" << it->dx << "," << it->dy << "," << it->frame << "," << it->delay << ")";
	}
	oss << "}";
	cout << "movingPathClone("
		 << movingPathClone->GetId() << ","
		 << movingPathClone->GetContinuous() << ","
		 << oss.str() << ","
		 << ")" << endl;

	delete moveClone;
	delete frameRangeClone;
	delete frameListClone;
	delete movingPathClone;
	return 0;
}
