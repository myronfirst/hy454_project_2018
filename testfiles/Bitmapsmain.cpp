#include <allegro5/allegro_image.h>
#include <iostream>
#include "Bitmap.h"
#include "BitmapLoader.h"
#include "Utilities.h"
#include "Wrappers.h"

const float FPS = 60;
const int SCREEN_W = 640;
const int SCREEN_H = 480;
const int BOUNCER_SIZE = 32;

int main() {
	ALLEGRO_DISPLAY* display = NULL;
	BitmapLoader bmpldr;

	if (!al_init()) {
		fprintf(stderr, "failed to initialize allegro!\n");
		return -1;
	}

	al_init_image_addon();
	display = al_create_display(SCREEN_W, SCREEN_H);
	if (!display) {
		fprintf(stderr, "failed to create display!\n");
		return -1;
	}

	// Rect r(0, 0, 42, 100);
	bmpldr.Load("assets/SubZero/SubZero_Crouch.png");
	Rect r1(Point(0, 0), 50, 89);
	Rect r2(Point(58, 0), 50, 89);
	Rect r3(Point(113, 0), 50, 89);	//Note that this rectangle goes out bound in the blit function to test the warning
	// std::cout << "Bitmap Height: " << BitmapAPI::GetHeight((bmpldr.GetBitmap("assets/SubZero/SubZero_Crouch.png"))) << std::endl;
	// std::cout << "Bitmap Weight: " << BitmapAPI::GetWidth((bmpldr.GetBitmap("assets/SubZero/SubZero_Crouch.png"))) << std::endl;

	for (size_t i = 0; i < 10; i++) {
		al_clear_to_color(al_map_rgb(0, 0, 0));
		BitmapAPI::Blit(bmpldr.GetBitmap("assets/SubZero/SubZero_Crouch.png"), r1, Point(150, 150), 0);
		al_flip_display();
		al_rest(0.3);
		al_clear_to_color(al_map_rgb(0, 0, 0));
		BitmapAPI::Blit(bmpldr.GetBitmap("assets/SubZero/SubZero_Crouch.png"), r2, Point(150, 150), 0);
		al_flip_display();
		al_rest(0.3);
		al_clear_to_color(al_map_rgb(0, 0, 0));
		BitmapAPI::Blit(bmpldr.GetBitmap("assets/SubZero/SubZero_Crouch.png"), r3, Point(150, 150), 0);
		al_flip_display();
		al_rest(0.3);
		al_clear_to_color(al_map_rgb(0, 0, 0));
		BitmapAPI::Blit(bmpldr.GetBitmap("assets/SubZero/SubZero_Crouch.png"), r2, Point(150, 150), 0);
		al_flip_display();
		al_rest(0.3);
	}

	al_destroy_display(display);

	return 0;
}
