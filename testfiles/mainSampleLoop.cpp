#include "AnimationFilmHolder.h"
#include "BitmapLoader.h"
#include "FrameListAnimator.h"
#include "FrameRangeAnimator.h"
#include "MovingAnimator.h"
#include "MovingPathAnimator.h"
#include "Sprite.h"
#include "TickAnimator.h"
#include "Wrappers.h"

#include <iostream>

using namespace std;
int main() {
	//Allegro lib inits
	wrap_init();
	wrap_init_image_addon();
	wrap_install_keyboard();

	//Allegro flags, display and keyboard inits
	wrap_set_new_display_flags(ALLEGRO_WINDOWED);									  //wrong binary operator??
	wrap_set_new_display_option(ALLEGRO_VSYNC, 1, ALLEGRO_SUGGEST);					  //depends on graphic card settings
	wrap_set_new_display_option(ALLEGRO_AUTO_CONVERT_BITMAPS, 1, ALLEGRO_SUGGEST);	//depends on bitmap flag ALLEGRO_CONVERT_BITMAP which is 1 by default
	wrap_set_new_bitmap_flags(ALLEGRO_CONVERT_BITMAP);								  //auto Converts bitmap to video bitmap when display is created
	ALLEGRO_DISPLAY* screen = wrap_create_display(static_cast<int>(SCREEN_WIDTH), static_cast<int>(SCREEN_HEIGHT));
	ALLEGRO_KEYBOARD_STATE keyboard_state;
	Bitmap backbuffer = BitmapAPI::GetScreen();

	BitmapLoader::Initialize();
	AnimationFilmHolder::Initialize();
	AnimationFilmHolder::Load("./assets/SubZero/SubZero.ini");	//Bitmap Loading (video bitmaps)
	const AnimationFilm* film = AnimationFilmHolder::GetFilm("subzero.forward");
	assert(film);
	Sprite* spr = new Sprite(20, 60, film);

	//Animators Construction
	MovingAnimation* moveAnim = new MovingAnimation(0, 100, 100, 1000000u, true);
	MovingAnimator* moveAnimator = new MovingAnimator();
	moveAnimator->SetOnFinish([](Animator* animator) { cout << "moveAnimator onFinish" << endl; });
	FrameRangeAnimation* frameRangeAnim = new FrameRangeAnimation(1, 0, 10, 1400000u, true, 0, 11);
	FrameRangeAnimator* frameRangeAnimator = new FrameRangeAnimator();
	frameRangeAnimator->SetOnFinish([](Animator* animator) { cout << "frameRangeAnimator onFinish" << endl; });
	FrameListAnimation* frameListAnim = new FrameListAnimation(2, 10, 0, 2000000u, true, {2, 4, 6});
	FrameListAnimator* frameListAnimator = new FrameListAnimator();
	frameListAnimator->SetOnFinish([](Animator* animator) { cout << "frameListAnimator onFinish" << endl; });
	MovingPathAnimation* movPathAnim = new MovingPathAnimation(3, true, {PathEntry(300, 0, 8, 1000000u), PathEntry(0, 300, 6, 800000u), PathEntry(-300, 0, 4, 600000), PathEntry(0, -300, 8, 400000)});
	MovingPathAnimator* movPathAnimator = new MovingPathAnimator();
	movPathAnimator->SetOnFinish([](Animator* animator) { cout << "movPathAnimator onFinish" << endl; });

	TickAnimation* tickAnim = new TickAnimation(4, 3000000u, 3, []() {
		cout << "TickAnimation" << endl;
	});
	TickAnimator* tickAnimator = new TickAnimator();
	tickAnimator->SetOnFinish([](Animator* animator) { cout << "tickAnimator onFinish" << endl; });

	wrap_clear_to_color(wrap_map_rgb(0, 0, 0));
	wrap_flip_display();
	bool doExit = false;
	bool reDraw = true;
	while (!doExit) {
		//Input
		wrap_get_keyboard_state(&keyboard_state);
		if (wrap_key_down(&keyboard_state, ALLEGRO_KEY_ESCAPE)) {
			doExit = true;
		}
		//Animations
		unsigned long currTime = GetGameTime();
		/* if (moveAnimator->IsSuspended()) {
			moveAnimator->Start(spr, moveAnim, currTime);
		} else {
			moveAnimator->Progress(currTime);
		} */
		/* if (frameRangeAnimator->IsSuspended()) {
			frameRangeAnimator->Start(spr, frameRangeAnim, currTime);
		} else {
			frameRangeAnimator->Progress(currTime);
		} */
		/* if (frameListAnimator->IsSuspended()) {
			frameListAnimator->Start(spr, frameListAnim, currTime);
		} else {
			frameListAnimator->Progress(currTime);
		} */
		/* if (movPathAnimator->IsSuspended()) {
			movPathAnimator->Start(spr, movPathAnim, currTime);
		} else {
			movPathAnimator->Progress(currTime);
		} */
		/* if (tickAnimator->IsSuspended()) {
			tickAnimator->Start(tickAnim, currTime);
		} else {
			tickAnimator->Progress(currTime);
		} */

		//Collision
		//Rendering
		if (reDraw) {
			reDraw = true;	//false
			wrap_clear_to_color(wrap_map_rgb(0, 0, 0));
			spr->Display(backbuffer);
			wrap_flip_display();
		}
	}
	delete spr;
	delete moveAnim;
	delete moveAnimator;
	delete frameRangeAnim;
	delete frameRangeAnimator;
	delete frameListAnim;
	delete frameListAnimator;
	delete movPathAnim;
	delete movPathAnimator;
	delete tickAnim;
	delete tickAnimator;
	AnimationFilmHolder::CleanUp();
	BitmapLoader::CleanUp();
	wrap_destroy_display(screen);
	return 0;
}
