#include <allegro5/allegro_primitives.h>
#include "AnimationDataHolder.h"
#include "AnimationFilmHolder.h"
#include "BitmapLoader.h"
#include "Fighter.h"
#include "FrameListAnimator.h"
#include "FrameRangeAnimator.h"
#include "InputController.h"
#include "MovingAnimator.h"
#include "MovingPathAnimator.h"
#include "Sprite.h"
#include "StateTransitions.h"
#include "TickAnimator.h"
#include "Utilities.h"
#include "Wrappers.h"

#include <iostream>
#include <sstream>

using namespace std;

InputController::Actions Player1Keybindings = {
	// Common moves
	{{"ALLEGRO_KEY_ALT"}, "block"},
	{{"ALLEGRO_KEY_V"}, "high_kick"},
	{{"ALLEGRO_KEY_B"}, "high_punch"},
	{{"ALLEGRO_KEY_V", "ALLEGRO_KEY_W"}, "upper.high_kick"},
	{{"ALLEGRO_KEY_SPACE", "ALLEGRO_KEY_V", "ALLEGRO_KEY_W"}, "jump.upper.high_kick"},
	{{"ALLEGRO_KEY_SPACE"}, "jump"},
	{{"ALLEGRO_KEY_S"}, "duck"},
	{{"ALLEGRO_KEY_D"}, "right"},
	{{"ALLEGRO_KEY_A"}, "left"},
	{{"ALLEGRO_KEY_B", "ALLEGRO_KEY_S"}, "down.high_punch"},
	{{"ALLEGRO_KEY_D", "ALLEGRO_KEY_S"}, "duck.right"},
	{{"ALLEGRO_KEY_S", "ALLEGRO_KEY_V"}, "duck.high_kick"},
	{{"ALLEGRO_KEY_D", "ALLEGRO_KEY_S", "ALLEGRO_KEY_V", "ALLEGRO_KEY_W"}, "down.high_kick.right.upper"},
	{{"ALLEGRO_KEY_W"}, "upper"},
	{{"ALLEGRO_KEY_V", "ALLEGRO_KEY_W"}, "upper.high_kick"},
	{{"ALLEGRO_KEY_B", "ALLEGRO_KEY_W"}, "upper.high_punch"},
	{{"ALLEGRO_KEY_ALT", "ALLEGRO_KEY_S"}, "block.duck"},
	{{"ALLEGRO_KEY_A", "ALLEGRO_KEY_S"}, "duck.left"},
	{{"ALLEGRO_KEY_S", "ALLEGRO_KEY_V"}, "down.high_kick"},
	{{"ALLEGRO_KEY_D", "ALLEGRO_KEY_S", "ALLEGRO_KEY_V"}, "down.high_kick.right"},
	{{"ALLEGRO_KEY_D", "ALLEGRO_KEY_SPACE", "ALLEGRO_KEY_V"}, "jump.forward.high_kick"},

	// {{"ALLEGRO_KEY_ALT", "ALLEGRO_KEY_S"}, "block.duck"},
	// {{"ALLEGRO_KEY_D", "ALLEGRO_KEY_LCTRL"}, "left.low_kick"},
	// {{"ALLEGRO_KEY_D", "ALLEGRO_KEY_LCTRL"}, "left.high_kick"},

	// Rayden special Moves
	// {{"ALLEGRO_KEY_D", "ALLEGRO_KEY_LCTRL", "ALLEGRO_KEY_W"}, "duck.low_punch.right."},
	// {{"ALLEGRO_KEY_A", "ALLEGRO_KEY_LCTRL", "ALLEGRO_KEY_W"}, "duck.jump"}
	//TODO: add torpedo move (Back,Back,Forward)
};

InputController::Actions Player2Keybindings = {
	{{"ALLEGRO_KEY_PAD_1"}, "block"},
	{{"ALLEGRO_KEY_PAD_2"}, "high_kick"},
	{{"ALLEGRO_KEY_PAD_3"}, "high_punch"},
	{{"ALLEGRO_KEY_PAD_2", "ALLEGRO_KEY_UP"}, "upper.high_kick"},
	{{"ALLEGRO_KEY_PAD_0", "ALLEGRO_KEY_PAD_2", "ALLEGRO_KEY_UP"}, "jump.upper.high_kick"},
	{{"ALLEGRO_KEY_PAD_0"}, "jump"},
	{{"ALLEGRO_KEY_DOWN"}, "duck"},
	{{"ALLEGRO_KEY_RIGHT"}, "right"},
	{{"ALLEGRO_KEY_LEFT"}, "left"},
	// {{"ALLEGRO_KEY_DOWN", "ALLEGRO_KEY_LEFT"}, "duck.right"},
	{{"ALLEGRO_KEY_DOWN", "ALLEGRO_KEY_LEFT", "ALLEGRO_KEY_PAD_2"}, "down.high_kick.right"},
	{{"ALLEGRO_KEY_DOWN", "ALLEGRO_KEY_LEFT", "ALLEGRO_KEY_PAD_2", "ALLEGRO_KEY_UP"}, "down.high_kick.right.upper"},
	{{"ALLEGRO_KEY_UP"}, "upper"},
	{{"ALLEGRO_KEY_PAD_2", "ALLEGRO_KEY_UP"}, "high_kick.upper"},
	{{"ALLEGRO_KEY_PAD_3", "ALLEGRO_KEY_UP"}, "upper.high_punch"},
	{{"ALLEGRO_KEY_DOWN", "ALLEGRO_KEY_PAD_1"}, "block.duck"},
	{{"ALLEGRO_KEY_DOWN", "ALLEGRO_KEY_RIGHT"}, "duck.left"},
	{{"ALLEGRO_KEY_PAD_0", "ALLEGRO_KEY_PAD_2", "ALLEGRO_KEY_LEFT"}, "jump.forward.high_kick"},
};

int main() {
	//Allegro lib inits
	wrap_init();
	wrap_init_image_addon();
	wrap_install_keyboard();
	wrap_init_font_addon();
	wrap_init_ttf_addon();
	wrap_init_primitives_addon();

	//Allegro flags, display and keyboard inits
	wrap_set_new_display_flags(ALLEGRO_WINDOWED);									  //wrong binary operator??
	wrap_set_new_display_option(ALLEGRO_VSYNC, 1, ALLEGRO_SUGGEST);					  //depends on graphic card settings
	wrap_set_new_display_option(ALLEGRO_AUTO_CONVERT_BITMAPS, 1, ALLEGRO_SUGGEST);	//depends on bitmap flag ALLEGRO_CONVERT_BITMAP which is 1 by default
	wrap_set_new_bitmap_flags(ALLEGRO_CONVERT_BITMAP);								  //auto Converts bitmap to video bitmap when display is created
	ALLEGRO_DISPLAY* screen = wrap_create_display(static_cast<int>(SCREEN_WIDTH), static_cast<int>(SCREEN_HEIGHT));
	ALLEGRO_KEYBOARD_STATE keyboard_state;
	ALLEGRO_COLOR blackColor = wrap_map_rgb(0, 0, 0);
	ALLEGRO_FONT* font = wrap_load_ttf_font("./assets/Fonts/Courier New.ttf", 16, 0);

	const Color COLOR_RED = wrap_map_rgb(225, 0, 0);
	const Color COLOR_GREEN = wrap_map_rgb(0, 255, 0);
	const Color COLOR_BLACK = wrap_map_rgb(0, 0, 0);

	BitmapLoader::Initialize();
	AnimationFilmHolder::Initialize();
	AnimationFilmHolder::Load("./assets/Raiden/Raiden.ini");	//Bitmap Loading (video bitmaps)
	AnimationFilmHolder::Load("./assets/SubZero/SubZero.ini");
	// Background assets
	Bitmap Background = BitmapLoader::Load("./assets/Backgrounds/csd.jpg");
	Point backgroundDisplayLocation(0, 0);
	Rect BackgroundRect(backgroundDisplayLocation, static_cast<unsigned>(SCREEN_WIDTH), static_cast<unsigned>(SCREEN_HEIGHT));

	// Set up animation data
	AnimationDataHolder::Load("./assets/Raiden/Raiden_AnimationData.ini");
	AnimationDataHolder::Load("./assets/SubZero/SubZero_AnimationData.ini");
	//Animators Construction

	/*****Initialize Player 1 *****/
	Fighter Player1 = Fighter(new Sprite(0, SCREEN_HEIGHT - 150, AnimationFilmHolder::GetFilm("raiden.idle")), "ready", "raiden", FACE_RIGHT, &Player1Keybindings);
	//Player1.SetTransitions("ready", {"duck.forward.high_punch"}, "raiden.cast.lighting.bolt", FRAME_LIST_ANIMATOR, "cast_lighing_bolt");
	//Player1.SetTransitions("cast_lighing_bolt", {}, "raiden.start.position", MOVING_ANIMATOR, "ready");

	/*****Initialize Player 2 *****/
	Fighter Player2 = Fighter(new Sprite(600, SCREEN_HEIGHT - 150, AnimationFilmHolder::GetFilm("subzero.idle")), "ready", "subzero", FACE_LEFT, &Player2Keybindings);

	wrap_clear_to_color(blackColor);
	wrap_flip_display();
	bool doExit = false;
	bool reDraw = true;
	while (!doExit) {
		//Input
		wrap_get_keyboard_state(&keyboard_state);
		if (wrap_key_down(&keyboard_state, ALLEGRO_KEY_ESCAPE)) {
			doExit = true;
		}
		Player1.GetInputController()->Handle();
		Player2.GetInputController()->Handle();

		//Animations
		unsigned long currTime = GetGameTime();
		Player1.ProgressAnimators(currTime);
		Player2.ProgressAnimators(currTime);

		//Collision

		//Rendering
		if (reDraw) {
			reDraw = true;	//false
			wrap_clear_to_color(blackColor);
			BitmapAPI::Blit(Background, BackgroundRect, backgroundDisplayLocation);

			// Health Bar 1
			wrap_draw_filled_rectangle(50.0f, 50.0f, 300.0f, 80.0f, COLOR_RED);
			wrap_draw_filled_rectangle(50.0f, 50.0f, 200.0f, 80.0f, COLOR_GREEN);
			wrap_draw_rectangle(50.0f, 50.0f, 300.0f, 80.0f, COLOR_BLACK, 3.5f);

			// Health Bar 2
			wrap_draw_filled_rectangle(500.0f, 50.0f, 750.0f, 80.0f, COLOR_RED);
			wrap_draw_filled_rectangle(670.0f, 50.0f, 750.0f, 80.0f, COLOR_GREEN);
			wrap_draw_rectangle(500.0f, 50.0f, 750.0f, 80.0f, COLOR_BLACK, 3.5f);

			Player1.GetSprite()->Display(BitmapAPI::GetTargetBitmap(), FACE_RIGHT);
			Player2.GetSprite()->Display(BitmapAPI::GetTargetBitmap(), FACE_LEFT);
			wrap_flip_display();
		}
	}
	//Free up resources
	AnimationDataHolder::CleanUp();
	AnimationFilmHolder::CleanUp();
	BitmapLoader::CleanUp();
	wrap_destroy_font(font);
	wrap_destroy_display(screen);
	return 0;
}
