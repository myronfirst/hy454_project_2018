// AS, CS454, Project MK (SNES version), code snippets.
// Version 2.
// December 2018, unit tested (partial), unrefactored.
// K:\Docs\Lectures\HY454\Content\Project\Project 2018\MK

#include <assert.h>
#include <stdio.h>
#include <algorithm>
#include <functional>
#include <iostream>
#include <list>
#include <map>
#include <set>
#include <string>
#include "InputController.h"

void printSet(std::set<std::string> set);

int main() {
	printSet(Input::FilterLogical({"block", "block.down", "block.down.highpunch", "highpunch"}));

	std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;

	printSet(Input::FilterLogical({""}));

	std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;

	printSet(Input::FilterLogical({"block.highpunch", "down.highpunch", "highpunch"}));

	std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;

	printSet(Input::FilterLogical({"block", "highpunch"}));
	return 0;
}

void printSet(std::set<std::string> set) {
	for (auto& str : set) {
		std::cout << str << std::endl;
	}
}
