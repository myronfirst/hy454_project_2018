#include "AnimationDataHolder.h"
#include "AnimationFilmHolder.h"
#include "BitmapLoader.h"
#include "CollisionHandlers.h"
#include "EventQueue.h"
#include "Fighter.h"
#include "FrameListAnimator.h"
#include "FrameRangeAnimator.h"
#include "InputController.h"
#include "MovingAnimator.h"
#include "MovingPathAnimator.h"
#include "PauseGame.h"
#include "SoundMixer.h"
#include "Sprite.h"
#include "StateTransitions.h"
#include "TickAnimator.h"
#include "Wrappers.h"

#include <iostream>
#include <sstream>

using namespace std;

ALLEGRO_COLOR COLOR_RED;
ALLEGRO_COLOR COLOR_GREEN;
ALLEGRO_COLOR COLOR_BLACK;
// extern void CheckFighterFacingDirection(Fighter p1, Fighter p2);

InputController::Actions Player1Keybindings = {
	// Common moves
	{{"ALLEGRO_KEY_SPACE"}, "jump"},
	{{"ALLEGRO_KEY_S"}, "duck"},
	{{"ALLEGRO_KEY_D"}, "right"},
	{{"ALLEGRO_KEY_A"}, "left"},
	{{"ALLEGRO_KEY_W"}, "upper"},
	{{"ALLEGRO_KEY_N"}, "block"},
	{{"ALLEGRO_KEY_V"}, "high_kick"},
	{{"ALLEGRO_KEY_B"}, "high_punch"},
	{{"ALLEGRO_KEY_V", "ALLEGRO_KEY_W"}, "upper.high_kick"},
	{{"ALLEGRO_KEY_SPACE", "ALLEGRO_KEY_V", "ALLEGRO_KEY_W"}, "jump.upper.high_kick"},
	{{"ALLEGRO_KEY_B", "ALLEGRO_KEY_S"}, "down.high_punch"},
	{{"ALLEGRO_KEY_D", "ALLEGRO_KEY_S"}, "duck.right"},
	{{"ALLEGRO_KEY_S", "ALLEGRO_KEY_V"}, "duck.high_kick"},
	{{"ALLEGRO_KEY_D", "ALLEGRO_KEY_S", "ALLEGRO_KEY_V", "ALLEGRO_KEY_W"}, "down.high_kick.right.upper"},
	{{"ALLEGRO_KEY_V", "ALLEGRO_KEY_W"}, "upper.high_kick"},
	{{"ALLEGRO_KEY_B", "ALLEGRO_KEY_W"}, "upper.high_punch"},
	{{"ALLEGRO_KEY_N", "ALLEGRO_KEY_S"}, "block.duck"},
	{{"ALLEGRO_KEY_A", "ALLEGRO_KEY_S"}, "duck.left"},
	{{"ALLEGRO_KEY_S", "ALLEGRO_KEY_V"}, "down.high_kick"},
	{{"ALLEGRO_KEY_D", "ALLEGRO_KEY_S", "ALLEGRO_KEY_V"}, "down.high_kick.right"},
	{{"ALLEGRO_KEY_D", "ALLEGRO_KEY_SPACE", "ALLEGRO_KEY_V"}, "jump.forward.high_kick"},

	{{"ALLEGRO_KEY_B", "ALLEGRO_KEY_D", "ALLEGRO_KEY_S"}, "down.forward.high_punch"},	//Special Move Lighting Bolt

};

InputController::Actions Player2Keybindings = {
	{{"ALLEGRO_KEY_RCTRL"}, "jump"},
	{{"ALLEGRO_KEY_DOWN"}, "duck"},
	{{"ALLEGRO_KEY_RIGHT"}, "right"},
	{{"ALLEGRO_KEY_LEFT"}, "left"},
	{{"ALLEGRO_KEY_UP"}, "upper"},
	{{"ALLEGRO_KEY_SLASH"}, "block"},
	{{"ALLEGRO_KEY_COMMA"}, "high_kick"},
	{{"ALLEGRO_KEY_FULLSTOP"}, "high_punch"},

	{{"ALLEGRO_KEY_DOWN", "ALLEGRO_KEY_RIGHT"}, "duck.right"},
	{{"ALLEGRO_KEY_COMMA", "ALLEGRO_KEY_DOWN"}, "duck.high_kick"},
	{{"ALLEGRO_KEY_DOWN", "ALLEGRO_KEY_FULLSTOP"}, "down.high_punch"},
	{{"ALLEGRO_KEY_COMMA", "ALLEGRO_KEY_DOWN", "ALLEGRO_KEY_LEFT"}, "down.high_kick.right"},
	{{"ALLEGRO_KEY_COMMA", "ALLEGRO_KEY_DOWN", "ALLEGRO_KEY_LEFT", "ALLEGRO_KEY_UP"}, "down.high_kick.right.upper"},

	{{"ALLEGRO_KEY_COMMA", "ALLEGRO_KEY_RCTRL", "ALLEGRO_KEY_UP"}, "jump.upper.high_kick"},
	{{"ALLEGRO_KEY_COMMA", "ALLEGRO_KEY_UP"}, "upper.high_kick"},

	//{{"ALLEGRO_KEY_COMMA", "ALLEGRO_KEY_UP"}, "high_kick.upper"},
	{{"ALLEGRO_KEY_FULLSTOP", "ALLEGRO_KEY_UP"}, "upper.high_punch"},
	{{"ALLEGRO_KEY_DOWN", "ALLEGRO_KEY_SLASH"}, "block.duck"},
	{{"ALLEGRO_KEY_DOWN", "ALLEGRO_KEY_RIGHT"}, "duck.left"},
	{{"ALLEGRO_KEY_COMMA", "ALLEGRO_KEY_LEFT", "ALLEGRO_KEY_RCTRL"}, "jump.forward.high_kick"},

	{{"ALLEGRO_KEY_COMMA", "ALLEGRO_KEY_DOWN"}, "down.high_kick"},
	{{"ALLEGRO_KEY_COMMA", "ALLEGRO_KEY_DOWN", "ALLEGRO_KEY_LEFT"}, "down.forward.high_kick"},

	{{"ALLEGRO_KEY_DOWN", "ALLEGRO_KEY_FULLSTOP", "ALLEGRO_KEY_LEFT"}, "down.forward.high_punch"},	//Special Move Freezeball
};

int main() {
	//Allegro lib inits
	wrap_init();
	wrap_init_image_addon();
	wrap_init_font_addon();
	wrap_init_ttf_addon();
	wrap_init_primitives_addon();
	wrap_init_acodec_addon();
	wrap_install_audio();
	wrap_install_keyboard();

	//Allegro flags, display and keyboard inits
	wrap_set_new_display_flags(ALLEGRO_WINDOWED);									  //wrong binary operator??
	wrap_set_new_display_option(ALLEGRO_VSYNC, 1, ALLEGRO_SUGGEST);					  //depends on graphic card settings
	wrap_set_new_display_option(ALLEGRO_AUTO_CONVERT_BITMAPS, 1, ALLEGRO_SUGGEST);	//depends on bitmap flag ALLEGRO_CONVERT_BITMAP which is 1 by default
	wrap_set_new_bitmap_flags(ALLEGRO_CONVERT_BITMAP);								  //auto Converts bitmap to video bitmap when display is created
	ALLEGRO_DISPLAY* screen = wrap_create_display(static_cast<int>(SCREEN_WIDTH), static_cast<int>(SCREEN_HEIGHT));
	::COLOR_BLACK = wrap_map_rgb(0, 0, 0);
	::COLOR_GREEN = wrap_map_rgb(0, 255, 0);
	::COLOR_RED = wrap_map_rgb(225, 0, 0);
	ALLEGRO_FONT* font = wrap_load_ttf_font("./assets/Fonts/mk1.ttf", 28, 0);

	BitmapLoader::Initialize();
	AnimationFilmHolder::Initialize();
	SoundMixer::Initialize();
	AnimationDataHolder::Initialize();
	AnimationFilmHolder::Load("./assets/Raiden/Raiden.ini");
	AnimationFilmHolder::Load("./assets/SubZero/SubZero.ini");	//Bitmap Loading (video bitmaps)
	AnimationDataHolder::Load("./assets/Raiden/Raiden_AnimationData.ini");
	AnimationDataHolder::Load("./assets/SubZero/SubZero_AnimationData.ini");

	// Background assets
	Bitmap Background = BitmapLoader::Load("./assets/Backgrounds/csd600.jpg");
	Point backgroundDisplayLocation(0, 0);
	Rect BackgroundRect(backgroundDisplayLocation, static_cast<unsigned>(SCREEN_WIDTH), static_cast<unsigned>(SCREEN_HEIGHT));

	// Sounds and Sound Effects
	SoundMixer::LoadAudioSamples("./assets/Sounds/Sounds.ini");

	/*****Initialize Player 1 *****/
	Fighter* player1 = new Fighter(new Sprite(100, SCREEN_HEIGHT - 350, AnimationFilmHolder::GetFilm("raiden.idle")), "ready", "raiden", FACE_RIGHT, &Player1Keybindings);
	player1->SetTransitions("ready", {"down.forward.high_punch"}, "raiden.cast.lighting.bolt", LIGHTINGBOLT_ANIMATOR, "cast_lighting_bolt", NORMAL);
	player1->SetTransitions("cast_lighting_bolt", {}, "raiden.rewind.cast.lighting.bolt", FRAME_LIST_ANIMATOR, "ready", NORMAL);
	/***** End of Player 1 *****/

	/*****Initialize Player 2 *****/
	Fighter* player2 = new Fighter(new Sprite(450, SCREEN_HEIGHT - 350, AnimationFilmHolder::GetFilm("subzero.idle")), "ready", "subzero", FACE_LEFT, &Player2Keybindings);
	player2->SetTransitions("ready", {"down.forward.high_punch"}, "subzero.cast.freezeball", FREEZEBALL_ANIMATOR, "cast_freezeball", NORMAL);
	player2->SetTransitions("cast_freezeball", {}, "subzero.rewind.cast.freezeball", FRAME_LIST_ANIMATOR, "ready", NORMAL);

	/***** End of Player 2 *****/

	//Initialize Event Queue
	EventQueue* eventQueue = new EventQueue();
	eventQueue->RegisterSource(wrap_get_keyboard_event_source());

	bool doExit = false;
	//StartScreen
	doExit = StartScreen(eventQueue);

	wrap_clear_to_color(COLOR_BLACK);
	wrap_flip_display();
	SoundMixer::PlayAudioInstance("fight.start");
	wrap_rest(0.5);
	SoundMixer::PlayAudioInstance("theme.song");

	bool reDraw = true;
	while (!doExit) {
		//Input
		player1->GetInputController()->Handle();
		player2->GetInputController()->Handle();
		//Game Exit, Game Pause
		if (!eventQueue->IsEmpty()) {
			ALLEGRO_EVENT event = eventQueue->GetNextEvent();
			if (event.type == ALLEGRO_EVENT_KEY_DOWN) {
				if (event.keyboard.keycode == ALLEGRO_KEY_ESCAPE) {
					doExit = true;
				} else if (event.keyboard.keycode == ALLEGRO_KEY_P) {
					doExit = PauseGame(eventQueue, font, player1, player2);
				}
			}
		}

		//Animations
		CheckFighterFacingDirection(player1, player2);
		unsigned long currTime = GetGameTime();
		player1->ProgressAnimators(currTime);
		player2->ProgressAnimators(currTime);

		//Collision
		if (player1->GetSprite()->CollisionCheck(player2->GetSprite())) {
			FighterToFighterCollisionHandler(player1, player2);
		}

		// Player's 1 Projectile Collision with Player 2
		if (player1->GetProjectile() != NULL && player1->GetProjectile()->GetSprite()->CollisionCheck(player2->GetSprite())) {
			ProjectileToFighterCollisionHandler(player1, player2);
		}

		// Player's 2 Projectile Collision with Player 1
		if (player2->GetProjectile() != NULL && player2->GetProjectile()->GetSprite()->CollisionCheck(player1->GetSprite())) {
			ProjectileToFighterCollisionHandler(player2, player1);
		}

		//Rendering
		if (reDraw) {
			reDraw = true;	//false
			wrap_clear_to_color(COLOR_BLACK);
			BitmapAPI::Blit(Background, BackgroundRect, backgroundDisplayLocation);
			DisplayHealthBars(font, player1, player2);
			player1->GetSprite()->Display(BitmapAPI::GetTargetBitmap(), player1->GetFacingDirection());
			player2->GetSprite()->Display(BitmapAPI::GetTargetBitmap(), player2->GetFacingDirection());
			if (player1->GetProjectile() != NULL) player1->GetProjectile()->GetSprite()->Display(BitmapAPI::GetTargetBitmap(), player1->GetFacingDirection());
			if (player2->GetProjectile() != NULL) player2->GetProjectile()->GetSprite()->Display(BitmapAPI::GetTargetBitmap(), player2->GetFacingDirection());

#ifdef DEBUG
			player1->DisplayHealth(font, Point(30, 550));
			player2->DisplayHealth(font, Point(600, 550));
			player1->DisplayFrameBox();
			player2->DisplayFrameBox();
			if (player2->GetProjectile() != NULL) player2->GetProjectile()->GetSprite()->DisplayFrameBox(wrap_map_rgba(255, 0, 0, 0.7f));
			if (player1->GetProjectile() != NULL) player1->GetProjectile()->GetSprite()->DisplayFrameBox(wrap_map_rgba(255, 0, 0, 0.7f));
#endif
			CheckFatality(player1, player2);
			DisplayCheckVictory(font, player1, player2);
			wrap_flip_display();
		}
	}

	//Free up resources
	delete player1;
	delete player2;
	delete eventQueue;
	AnimationDataHolder::CleanUp();
	SoundMixer::CleanUp();
	AnimationFilmHolder::CleanUp();
	BitmapLoader::CleanUp();
	wrap_destroy_font(font);
	wrap_destroy_display(screen);
	return 0;
}
