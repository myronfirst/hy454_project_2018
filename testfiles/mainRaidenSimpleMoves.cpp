#include "AnimationDataHolder.h"
#include "AnimationFilmHolder.h"
#include "BitmapLoader.h"
#include "Fighter.h"
#include "FrameListAnimator.h"
#include "FrameRangeAnimator.h"
#include "InputController.h"
#include "MovingAnimator.h"
#include "MovingPathAnimator.h"
#include "Sprite.h"
#include "StateTransitions.h"
#include "TickAnimator.h"
#include "Wrappers.h"

#include <iostream>
#include <sstream>

using namespace std;

InputController::Actions RaidenSampleActions = {
	{{"ALLEGRO_KEY_D"}, "right"},
	{{"ALLEGRO_KEY_LCTRL", "ALLEGRO_KEY_W"}, "high_punch.up"}};

InputController::Actions SubZeroSampleActions = {
	{{"ALLEGRO_KEY_RIGHT"}, "right"},
	{{"ALLEGRO_KEY_H", "ALLEGRO_KEY_UP"}, "high_punch.up"}};

int main() {
	//Allegro lib inits
	wrap_init();
	wrap_init_image_addon();
	wrap_install_keyboard();
	wrap_init_font_addon();
	wrap_init_ttf_addon();

	//Allegro flags, display and keyboard inits
	wrap_set_new_display_flags(ALLEGRO_WINDOWED);									  //wrong binary operator??
	wrap_set_new_display_option(ALLEGRO_VSYNC, 1, ALLEGRO_SUGGEST);					  //depends on graphic card settings
	wrap_set_new_display_option(ALLEGRO_AUTO_CONVERT_BITMAPS, 1, ALLEGRO_SUGGEST);	//depends on bitmap flag ALLEGRO_CONVERT_BITMAP which is 1 by default
	wrap_set_new_bitmap_flags(ALLEGRO_CONVERT_BITMAP);								  //auto Converts bitmap to video bitmap when display is created
	ALLEGRO_DISPLAY* screen = wrap_create_display(static_cast<int>(SCREEN_WIDTH), static_cast<int>(SCREEN_HEIGHT));
	ALLEGRO_KEYBOARD_STATE keyboard_state;
	ALLEGRO_COLOR blackColor = wrap_map_rgb(0, 0, 0);
	ALLEGRO_FONT* font = wrap_load_ttf_font("./assets/Fonts/Courier New.ttf", 16, 0);

	BitmapLoader::Initialize();
	AnimationFilmHolder::Initialize();
	AnimationFilmHolder::Load("./assets/Raiden/Raiden.ini");	//Bitmap Loading (video bitmaps)

	// Background assets
	Bitmap Background = BitmapLoader::Load("./assets/Backgrounds/csd.jpg");
	Point backgroundDisplayLocation(0, 0);
	Rect BackgroundRect(backgroundDisplayLocation, static_cast<unsigned>(SCREEN_WIDTH), static_cast<unsigned>(SCREEN_HEIGHT));

	// Set up animation data
	AnimationDataHolder::Load("./assets/Raiden/Raiden_AnimationData.ini");
	//Animators Construction

	/*****Initialize Player 1 *****/
	Fighter Player1 = Fighter(new Sprite(20, SCREEN_HEIGHT, AnimationFilmHolder::GetFilm("raiden.idle")), "ready", "Raiden", FACE_RIGHT, &RaidenSampleActions);

	Player1.GetStateTransitions()->SetState("ready");
	Player1.SetTransitions("ready", {"right"}, "raiden.walking.forward", FRAME_RANGE_ANIMATOR, "ready");

	Player1.SetTransitions("ready", {"high_punch.up"}, "raiden.upper.left.punch", FRAME_LIST_ANIMATOR, "ready");

	// Player1.GetStateTransitions()->SetTransition("ready", {"high_punch.up"}, [&](void) {
	// 	if (!Player1.IsAnimatorRunning() || Player1.GetSprite()->GetFilm() == AnimationFilmHolder::GetFilm("raiden.idle")) { //We need to start a new animation only if there isn't already one running
	// 		Player1.GetFrameRangeAnimator()->Stop();																					//or the idle animation is running
	// 		Player1.GetSprite()->SetFilm(AnimationFilmHolder::GetFilm("raiden.upper.left.punch"));						//change the film
	// 		Player1.GetFrameListAnimator()->Start(Player1.GetSprite(), AnimationDataHolder::GetFrameListAnimation("raiden.upper.left.punch"), GetGameTime());
	// 	} });

	Player1.GetStateTransitions()->SetTransition("ready", {}, [&](void) {	if(!Player1.IsAnimatorRunning()){ // transition for ready.*
			Player1.GetSprite()->SetFilm(AnimationFilmHolder::GetFilm("raiden.idle"));
			Player1.GetFrameRangeAnimator()->Start(Player1.GetSprite(), AnimationDataHolder::GetFrameRangeAnimation("raiden.idle"), GetGameTime());
	} });

	/***** End of Player 1 *****/

	/*****Initialize Player 2 *****/
	Fighter Player2 = Fighter(new Sprite(780, SCREEN_HEIGHT, AnimationFilmHolder::GetFilm("raiden.idle")), "ready", "Raiden", FACE_LEFT, &SubZeroSampleActions);

	Player2.GetStateTransitions()->SetState("ready");
	Player2.SetTransitions("ready", {"right"}, "raiden.walking.forward", FRAME_RANGE_ANIMATOR, "ready");
	// Player2.GetStateTransitions()->SetTransition("ready", {"right"}, [&](void) {
	// 	if (!Player2.IsAnimatorRunning() || Player2.GetSprite()->GetFilm() == AnimationFilmHolder::GetFilm("raiden.idle")) { //We need to start a new animation only if there isn't already one running
	// 		Player2.GetFrameRangeAnimator()->Stop();																		 //or the idle animation is running
	// 		Player2.GetSprite()->SetFilm(AnimationFilmHolder::GetFilm("raiden.walking.forward"));							 //change the film
	// 		Player2.GetFrameRangeAnimator()->Start(Player2.GetSprite(), AnimationDataHolder::GetFrameRangeAnimation("raiden.walking.forward"), GetGameTime());
	// 	} });

	Player2.SetTransitions("ready", {"high_punch.up"}, "raiden.upper.left.punch", FRAME_LIST_ANIMATOR, "ready");
	// Player2.GetStateTransitions()
	// 	->SetTransition("ready", {"high_punch.up"}, [&](void) {
	// 	if (!Player2.IsAnimatorRunning() || Player2.GetSprite()->GetFilm() == AnimationFilmHolder::GetFilm("raiden.idle")) { //We need to start a new animation only if there isn't already one running
	// 		Player2.GetFrameRangeAnimator()->Stop();																		 //or the idle animation is running
	// 		Player2.GetSprite()->SetFilm(AnimationFilmHolder::GetFilm("raiden.upper.left.punch"));							 //change the film
	// 		Player2.GetFrameListAnimator()->Start(Player2.GetSprite(), AnimationDataHolder::GetFrameListAnimation("raiden.upper.left.punch"), GetGameTime());
	// 	} });

	Player2.GetStateTransitions()->SetTransition("ready", {}, [&](void) {	if(!Player2.IsAnimatorRunning()){ // transition for ready.*
			Player2.GetSprite()->SetFilm(AnimationFilmHolder::GetFilm("raiden.idle"));
			Player2.GetFrameRangeAnimator()->Start(Player2.GetSprite(), AnimationDataHolder::GetFrameRangeAnimation("raiden.idle"), GetGameTime());
	} });

	/***** End of Player 2 *****/

	wrap_clear_to_color(blackColor);
	wrap_flip_display();
	bool doExit = false;
	bool reDraw = true;
	while (!doExit) {
		//Input
		wrap_get_keyboard_state(&keyboard_state);
		if (wrap_key_down(&keyboard_state, ALLEGRO_KEY_ESCAPE)) {
			doExit = true;
		}
		Player1.GetInputController()->Handle();
		Player2.GetInputController()->Handle();
		// for (auto it = inputHandler->GetLogical().begin(); it != inputHandler->GetLogical().end(); ++it) {
		// oss << *it << endl;
		// }

		//Animations
		unsigned long currTime = GetGameTime();
		Player1.ProgessAnimators(currTime);
		Player2.ProgessAnimators(currTime);

		//Collision
		//Rendering
		if (reDraw) {
			reDraw = true;	//false
			wrap_clear_to_color(blackColor);
			BitmapAPI::Blit(Background, BackgroundRect, backgroundDisplayLocation);
			Player1.GetSprite()->Display(BitmapAPI::GetTargetBitmap(), FACE_RIGHT);
			Player2.GetSprite()->Display(BitmapAPI::GetTargetBitmap(), FACE_LEFT);
			wrap_flip_display();
		}
	}
	//Free up resources
	AnimationDataHolder::CleanUp();
	AnimationFilmHolder::CleanUp();
	BitmapLoader::CleanUp();
	wrap_destroy_font(font);
	wrap_destroy_display(screen);
	return 0;
}
