#include "AnimationFilmHolder.h"
#include "BitmapLoader.h"
#include "FrameListAnimator.h"
#include "FrameRangeAnimator.h"
#include "InputController.h"
#include "MovingAnimator.h"
#include "MovingPathAnimator.h"
#include "Sprite.h"
#include "StateTransitions.h"
#include "TickAnimator.h"
#include "Wrappers.h"

#include <iostream>
#include <sstream>

InputController::Actions sampleActions = {
	{{"ALLEGRO_KEY_UP"}, "up"},
	{{"ALLEGRO_KEY_DOWN"}, "duck"},
	{{"ALLEGRO_KEY_LEFT"}, "left"},
	{{"ALLEGRO_KEY_RIGHT"}, "right"},
	{{"ALLEGRO_KEY_A"}, "high_punch"},
	{{"ALLEGRO_KEY_S"}, "high_kick"},
	{{"ALLEGRO_KEY_Z"}, "low_punch"},
	{{"ALLEGRO_KEY_X"}, "low_kick"},
	{{"ALLEGRO_KEY_LSHIFT"}, "block"},
	{{"ALLEGRO_KEY_LSHIFT", "ALLEGRO_KEY_DOWN"}, "block.duck"},
	{{"ALLEGRO_KEY_DOWN", "ALLEGRO_KEY_A"}, "duck.high_punch"},
	{{"ALLEGRO_KEY_RIGHT", "ALLEGRO_KEY_DOWN", "ALLEGRO_KEY_Z"}, "right.duck.low_punch"}};

using namespace std;
int main() {
	//Allegro lib inits
	wrap_init();
	wrap_init_image_addon();
	wrap_install_keyboard();
	wrap_init_font_addon();
	wrap_init_ttf_addon();

	//Allegro flags, display and keyboard inits
	wrap_set_new_display_flags(ALLEGRO_WINDOWED);									  //wrong binary operator??
	wrap_set_new_display_option(ALLEGRO_VSYNC, 1, ALLEGRO_SUGGEST);					  //depends on graphic card settings
	wrap_set_new_display_option(ALLEGRO_AUTO_CONVERT_BITMAPS, 1, ALLEGRO_SUGGEST);	//depends on bitmap flag ALLEGRO_CONVERT_BITMAP which is 1 by default
	wrap_set_new_bitmap_flags(ALLEGRO_CONVERT_BITMAP);								  //auto Converts bitmap to video bitmap when display is created
	ALLEGRO_DISPLAY* screen = wrap_create_display(static_cast<int>(SCREEN_WIDTH), static_cast<int>(SCREEN_HEIGHT));
	ALLEGRO_KEYBOARD_STATE keyboard_state;
	Bitmap backbuffer = BitmapAPI::GetScreen();
	ALLEGRO_COLOR blackColor = wrap_map_rgb(0, 0, 0);
	ALLEGRO_COLOR whiteColor = wrap_map_rgb(255, 255, 255);
	ALLEGRO_COLOR yellowColor = wrap_map_rgb(255, 255, 0);
	ALLEGRO_FONT* font = wrap_load_ttf_font("./assets/Fonts/Courier New.ttf", 16, 0);

	BitmapLoader::Initialize();
	AnimationFilmHolder::Initialize();
	AnimationFilmHolder::Load("./assets/SubZero/SubZero.ini");	//Bitmap Loading (video bitmaps)
	const AnimationFilm* film = AnimationFilmHolder::GetFilm("subzero.forward");
	assert(film);
	Sprite* spr = new Sprite(20, 60, film);

	//Animators Construction
	MovingAnimation* moveAnim = new MovingAnimation(0, 100, 100, 1000000u, true);
	MovingAnimator* moveAnimator = new MovingAnimator();
	moveAnimator->SetOnFinish([](Animator* animator) { cout << "moveAnimator onFinish" << endl; });
	FrameRangeAnimation* frameRangeAnim = new FrameRangeAnimation(1, 0, 10, 1400000u, true, 0, 11);
	FrameRangeAnimator* frameRangeAnimator = new FrameRangeAnimator();
	frameRangeAnimator->SetOnFinish([](Animator* animator) { cout << "frameRangeAnimator onFinish" << endl; });
	FrameListAnimation* frameListAnim = new FrameListAnimation(2, 10, 0, 2000000u, true, {2, 4, 6});
	FrameListAnimator* frameListAnimator = new FrameListAnimator();
	frameListAnimator->SetOnFinish([](Animator* animator) { cout << "frameListAnimator onFinish" << endl; });
	MovingPathAnimation* movPathAnim = new MovingPathAnimation(3, true, {PathEntry(300, 0, 8, 1000000u), PathEntry(0, 300, 6, 800000u), PathEntry(-300, 0, 4, 600000), PathEntry(0, -300, 8, 400000)});
	MovingPathAnimator* movPathAnimator = new MovingPathAnimator();
	movPathAnimator->SetOnFinish([](Animator* animator) { cout << "movPathAnimator onFinish" << endl; });

	//Classes for Input
	ostringstream oss;
	string stateDraw;
	InputController* inputHandler = new InputController();
	for (auto it = sampleActions.begin(); it != sampleActions.end(); ++it) {
		inputHandler->AddAction(it->first, it->second);
	}
	StateTransitions* stateTrans = new StateTransitions();
	stateTrans->SetState("ready");
	for (auto it = sampleActions.begin(); it != sampleActions.end(); ++it) {
		string byValue = string(string(it->second));
		stateTrans->SetTransition("ready", {it->second}, [&, byValue]() {
			stateDraw = byValue + " edge";
			stateTrans->SetState("ready");
		});
	}
	stateTrans->SetTransition("ready", {}, [&]() -> void {
		stateDraw = "* edge";
		stateTrans->SetState("ready");
	});

	TickAnimation* tickAnim = new TickAnimation(4, 1000000u, 0, [&]() {
		cout << "TickAnimation Clearing logical, stateDraw, exec PerformTransition" << endl;
		Input::Logical maxLogical = Input::FilterLogical(inputHandler->GetLogical());
		inputHandler->ClearLogical();
		stateDraw.clear();
		stateTrans->PerformTransitions(maxLogical, false);
	});
	TickAnimator* tickAnimator = new TickAnimator();
	tickAnimator->SetOnFinish([](Animator* animator) {
		cout << "tickAnimator onFinish" << endl;
	});

	wrap_clear_to_color(blackColor);
	wrap_flip_display();
	bool doExit = false;
	bool reDraw = true;
	while (!doExit) {
		//Input
		wrap_get_keyboard_state(&keyboard_state);
		if (wrap_key_down(&keyboard_state, ALLEGRO_KEY_ESCAPE)) {
			doExit = true;
		}
		inputHandler->Handle();
		oss.str("");
		oss.clear();
		for (auto it = inputHandler->GetLogical().begin(); it != inputHandler->GetLogical().end(); ++it) {
			oss << *it << endl;
		}

		//Animations
		unsigned long currTime = GetGameTime();
		if (tickAnimator->IsSuspended()) {
			tickAnimator->Start(tickAnim, currTime);
		} else {
			tickAnimator->Progress(currTime);
		}

		//Collision
		//Rendering
		if (reDraw) {
			reDraw = true;	//false
			wrap_clear_to_color(blackColor);
			// spr->Display(backbuffer);
			wrap_draw_multiline_text(font, whiteColor, 100, 400, 200, 20, ALLEGRO_ALIGN_LEFT, oss.str().c_str());
			wrap_draw_multiline_text(font, yellowColor, 500, 400, 200, 20, ALLEGRO_ALIGN_LEFT, stateDraw.c_str());
			wrap_flip_display();
		}
	}
	delete spr;
	delete moveAnim;
	delete moveAnimator;
	delete frameRangeAnim;
	delete frameRangeAnimator;
	delete frameListAnim;
	delete frameListAnimator;
	delete movPathAnim;
	delete movPathAnimator;
	delete tickAnim;
	delete tickAnimator;

	delete inputHandler;
	delete stateTrans;

	AnimationFilmHolder::CleanUp();
	BitmapLoader::CleanUp();
	wrap_destroy_font(font);
	wrap_destroy_display(screen);
	return 0;
}
