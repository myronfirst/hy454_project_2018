#include "AnimationDataHolder.h"
#include <vector>
#include "ConfigParser.h"
#include "Utilities.h"

AnimationDataHolder::MovingMap AnimationDataHolder::movingAnimations;
AnimationDataHolder::MovingPathMap AnimationDataHolder::movingPathAnimations;
AnimationDataHolder::FrameListMap AnimationDataHolder::frameListAnimations;
AnimationDataHolder::FrameRangeMap AnimationDataHolder::frameRangeAnimations;

void AnimationDataHolder::Initialize(void) {}

void AnimationDataHolder::CleanUp(void) {
	//clear all moving animations
	for (MovingMap::iterator i = movingAnimations.begin(); i != movingAnimations.end(); i++) {
		delete (i->second);
	}
	movingAnimations.clear();

	//clear all movingpath animations
	for (MovingPathMap::iterator i = movingPathAnimations.begin(); i != movingPathAnimations.end(); i++) {
		delete (i->second);
	}
	movingPathAnimations.clear();

	//clear all frame list animations
	for (FrameListMap::iterator i = frameListAnimations.begin(); i != frameListAnimations.end(); i++) {
		delete (i->second);
	}
	frameListAnimations.clear();

	//clear all frame range animations
	for (FrameRangeMap::iterator i = frameRangeAnimations.begin(); i != frameRangeAnimations.end(); i++) {
		delete (i->second);
	}
	frameRangeAnimations.clear();
}

void AnimationDataHolder::Load(const std::string& catalogue) {
	//Parse the animation data ini file and load the animations according to the type of the animation
	std::string type("");
	std::string id("");
	std::string continuous("");
	std::string dx("");
	std::string dy("");
	std::string delay("");
	ConfigParser parser(catalogue);

	assert(parser.GetConfigFirstSection().empty());	//must be default section
	for (std::string section = parser.GetConfigNextSection(); !section.empty(); section = parser.GetConfigNextSection()) {
		std::string key = parser.GetConfigFirstKey(section);	//type
		type = parser.GetConfigValue(section, key);

		key = parser.GetConfigNextKey();	//id
		// str << parser.GetConfigValue(section, key);
		// str >> path;
		id = parser.GetConfigValue(section, key);

		if (type == "MovingPathAnimation") {
			std::string frame("");

			key = parser.GetConfigNextKey();	//continuous
			continuous = parser.GetConfigValue(section, key);
			bool continuousFlag = (continuous == "true") ? true : false;

			// PathEntries
			std::vector<PathEntry> path;
			for (key = parser.GetConfigNextKey(); !key.empty(); key = parser.GetConfigNextKey()) {
				dx = parser.GetConfigValue(section, key);

				key = parser.GetConfigNextKey();	//dy
				dy = parser.GetConfigValue(section, key);

				key = parser.GetConfigNextKey();	//frame
				frame = parser.GetConfigValue(section, key);

				key = parser.GetConfigNextKey();	//delay
				delay = parser.GetConfigValue(section, key);

				path.push_back(PathEntry(static_cast<signed short>(std::stoi(dx)),
										 static_cast<signed short>(std::stoi(dy)),
										 static_cast<unsigned long>(std::stoi(frame)),
										 static_cast<unsigned long>(std::stoi(delay))));
			}
			movingPathAnimations.insert(std::pair<std::string, MovingPathAnimation*>(id,
																					 new MovingPathAnimation(id,
																											 continuousFlag,
																											 path)));
			frame.clear();

		} else {	//Moving animation and subtypes

			key = parser.GetConfigNextKey();	//dx
			dx = parser.GetConfigValue(section, key);

			key = parser.GetConfigNextKey();	//dy
			dy = parser.GetConfigValue(section, key);

			key = parser.GetConfigNextKey();	//delay
			delay = parser.GetConfigValue(section, key);

			key = parser.GetConfigNextKey();	//continuous
			continuous = parser.GetConfigValue(section, key);

			bool continuousFlag = (continuous == "true") ? true : false;

			if (type == "MovingAnimation") {
				movingAnimations.insert(std::pair<std::string, MovingAnimation*>(id,
																				 new MovingAnimation(id,
																									 static_cast<signed short>(std::stoi(dx)),
																									 static_cast<signed short>(std::stoi(dy)),
																									 static_cast<unsigned long>(std::stoi(delay)),
																									 continuousFlag)));

			} else if (type == "FrameRangeAnimation") {
				std::string startStr("");
				std::string endStr("");

				key = parser.GetConfigNextKey();	//start
				startStr = parser.GetConfigValue(section, key);

				key = parser.GetConfigNextKey();	//end
				endStr = parser.GetConfigValue(section, key);

				frameRangeAnimations.insert(std::pair<std::string, FrameRangeAnimation*>(id,
																						 new FrameRangeAnimation(id,
																												 static_cast<signed short>(std::stoi(dx)),
																												 static_cast<signed short>(std::stoi(dy)),
																												 static_cast<unsigned long>(std::stoi(delay)),
																												 continuousFlag,
																												 static_cast<unsigned>(std::stoi(startStr)),
																												 static_cast<unsigned>(std::stoi(endStr)))));

				startStr.clear();
				endStr.clear();
			} else if (type == "FrameListAnimation") {
				std::string frameStr("");

				std::vector<frame_t> frames;
				for (key = parser.GetConfigNextKey(); !key.empty(); key = parser.GetConfigNextKey()) {
					frameStr = parser.GetConfigValue(section, key);	//Frame number
					frames.push_back(static_cast<frame_t>(std::stoi(frameStr)));
				}

				frameListAnimations.insert(std::pair<std::string, FrameListAnimation*>(id,
																					   new FrameListAnimation(id,
																											  static_cast<signed short>(std::stoi(dx)),
																											  static_cast<signed short>(std::stoi(dy)),
																											  static_cast<unsigned long>(std::stoi(delay)),
																											  continuousFlag,
																											  frames)));
			} else {	//default case - invalid animation type
				PrintWarning("Animation id: " + id + " has invalid animation type: " + type + ". Discarding it....");
			}
		}

		dx.clear();
		dy.clear();
		delay.clear();
		continuous.clear();
		type.clear();
		key.clear();
		id.clear();
	}
}

MovingAnimation* AnimationDataHolder::GetMovingAnimation(const std::string& id) {
	MovingMap::const_iterator i = movingAnimations.find(id);
	return (i != movingAnimations.end() ? i->second : nullptr);
}
MovingPathAnimation* AnimationDataHolder::GetMovingPathAnimation(const std::string& id) {
	MovingPathMap::const_iterator i = movingPathAnimations.find(id);
	return (i != movingPathAnimations.end() ? i->second : nullptr);
}
FrameRangeAnimation* AnimationDataHolder::GetFrameRangeAnimation(const std::string& id) {
	FrameRangeMap::const_iterator i = frameRangeAnimations.find(id);
	return (i != frameRangeAnimations.end() ? i->second : nullptr);
}
FrameListAnimation* AnimationDataHolder::GetFrameListAnimation(const std::string& id) {
	FrameListMap::const_iterator i = frameListAnimations.find(id);
	return (i != frameListAnimations.end() ? i->second : nullptr);
}
