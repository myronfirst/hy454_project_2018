#include "FrameListAnimator.h"

FrameListAnimator::FrameListAnimator()
	: sprite(nullptr), anim(nullptr), currEl(0) {}
FrameListAnimator::~FrameListAnimator() {}
void FrameListAnimator::Start(Sprite* s, FrameListAnimation* a, timestamp_t t) {
	sprite = s;
	anim = a;
	lastTime = t;
	state = ANIMATOR_RUNNING;
	currEl = 0;
	sprite->SetFrame(anim->GetFrames().at(currEl));
}
void FrameListAnimator::Progress(timestamp_t currTime) {
	while (currTime > lastTime && currTime - lastTime >= anim->GetDelay()) {
		if (currEl == anim->GetFrames().size() - 1)
			currEl = 0;
		else
			++currEl;

		sprite->Move(anim->GetDx(), anim->GetDy());
		sprite->SetFrame(anim->GetFrames().at(currEl));
		lastTime += anim->GetDelay();

		if (currEl == anim->GetFrames().size() - 1 && !anim->GetContinuous()) {
			state = ANIMATOR_FINISHED;
			NotifyStopped();
			return;
		}
	}
}
