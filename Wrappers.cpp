#include "Wrappers.h"
#include "Utilities.h"

#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_primitives.h>
#include <allegro5/allegro_ttf.h>
#include <sstream>

using namespace std;
//Categories from Allegro 5 Reference Manual

//Configuration files
ALLEGRO_CONFIG* wrap_create_config(void) {
	ALLEGRO_CONFIG* ret = al_create_config();
	if (ret == NULL) {
		ostringstream str;
		str << "al_create_config() failed";
		PrintError(str.str());
	}
	return ret;
}
void wrap_destroy_config(ALLEGRO_CONFIG* config) {
	if (config == NULL) {
		ostringstream str;
		str << "wrap_destroy_config(config): config is already NULL";
		PrintWarning(str.str());
	}
	al_destroy_config(config);
}
ALLEGRO_CONFIG* wrap_load_config_file(const char* filename) {
	ALLEGRO_CONFIG* ret = al_load_config_file(filename);
	if (ret == NULL) {
		ostringstream str;
		str << "al_load_config_file(" << filename << ") failed";
		PrintError(str.str());
	}
	return ret;
}
const char* wrap_get_config_value(const ALLEGRO_CONFIG* config, const char* section, const char* key) {
	const char* ret = al_get_config_value(config, section, key);
	if (ret == NULL) {
		ostringstream str;
		str << "al_get_config_value(config, " << section << ", " << key << ") failed";
		PrintError(str.str());
	}
	return ret;
}
char const* wrap_get_first_config_section(ALLEGRO_CONFIG const* config, ALLEGRO_CONFIG_SECTION** iterator) {
	char const* ret = al_get_first_config_section(config, iterator);
	/* if (ret == NULL) {
		ostringstream str;
		str << "al_get_first_config_section(config, iterator): returned NULL:";
		PrintWarning(str.str());
	} */
	return ret;
}
char const* wrap_get_next_config_section(ALLEGRO_CONFIG_SECTION** iterator) {
	char const* ret = al_get_next_config_section(iterator);
	/* if (ret == NULL) {
		ostringstream str;
		str << "al_get_next_config_section(iterator): no more sections";
		PrintWarning(str.str());
	} */
	return ret;
}
char const* wrap_get_first_config_entry(ALLEGRO_CONFIG const* config, char const* section, ALLEGRO_CONFIG_ENTRY** iterator) {
	char const* ret = al_get_first_config_entry(config, section, iterator);
	/* if (ret == NULL) {
		ostringstream str;
		str << "al_get_first_config_entry(config, " << section << ", iterator): no entries in section:" << section;
		PrintWarning(str.str());
	} */
	return ret;
}
char const* wrap_get_next_config_entry(ALLEGRO_CONFIG_ENTRY** iterator) {
	char const* ret = al_get_next_config_entry(iterator);
	/* if (ret == NULL) {
		ostringstream str;
		str << "al_get_next_config_entry(iterator): no more entries";
		PrintWarning(str.str());
	} */
	return ret;
}

//Displays
void wrap_set_new_display_flags(int flags) {
	al_set_new_display_flags(flags);
}
void wrap_set_new_display_option(int option, int value, int importance) {
	al_set_new_display_option(option, value, importance);
}
ALLEGRO_DISPLAY* wrap_create_display(int w, int h) {
	ALLEGRO_DISPLAY* ret = al_create_display(w, h);
	if (ret == NULL) {
		ostringstream str;
		str << "al_create_display(" << w << ", " << h << ") failed";
		PrintError(str.str());
	}
	return ret;
}
void wrap_destroy_display(ALLEGRO_DISPLAY* display) {
	al_destroy_display(display);
}
ALLEGRO_DISPLAY* wrap_get_current_display() {
	return al_get_current_display();
}
void wrap_flip_display(void) {
	al_flip_display();
}

void wrap_set_window_title(ALLEGRO_DISPLAY* display, const char* title) {
	al_set_window_title(display, title);
}

//Events
ALLEGRO_EVENT_QUEUE* wrap_create_event_queue(void) {
	ALLEGRO_EVENT_QUEUE* ret = al_create_event_queue();
	if (ret == NULL) {
		ostringstream str;
		str << "al_create_event_queue() failed";
		PrintError(str.str());
	}
	return ret;
}
void wrap_destroy_event_queue(ALLEGRO_EVENT_QUEUE* queue) {
	al_destroy_event_queue(queue);
}
void wrap_register_event_source(ALLEGRO_EVENT_QUEUE* queue, ALLEGRO_EVENT_SOURCE* source) {
	al_register_event_source(queue, source);
}
void wrap_wait_for_event(ALLEGRO_EVENT_QUEUE* queue, ALLEGRO_EVENT* ret_event) {
	al_wait_for_event(queue, ret_event);
}
bool wrap_is_event_queue_empty(ALLEGRO_EVENT_QUEUE* queue) {
	return al_is_event_queue_empty(queue);
}
bool wrap_get_next_event(ALLEGRO_EVENT_QUEUE* queue, ALLEGRO_EVENT* ret_event) {
	return al_get_next_event(queue, ret_event);
}
//Fullscreen modes

//Graphics routines
void wrap_set_new_bitmap_flags(int flags) {
	al_set_new_bitmap_flags(flags);
}
ALLEGRO_BITMAP* wrap_create_bitmap(int width, int height) {
	ALLEGRO_BITMAP* BitmapPtr;
	BitmapPtr = al_create_bitmap(width, height);
	if (!BitmapPtr) {
		ostringstream str;
		str << "al_create_bitmap(" << width << ", " << height << ") failed";
		PrintError(str.str());
	}
	return BitmapPtr;
}

ALLEGRO_BITMAP* wrap_create_sub_bitmap(ALLEGRO_BITMAP* parent, int x, int y, int w, int h) {
	ALLEGRO_BITMAP* sub_bitmap = al_create_sub_bitmap(parent, x, y, w, h);
	if (!sub_bitmap) {
		ostringstream str;
		str << "al_create_sub_bitmap(" << x << ", " << y << ", " << w << ", " << h << ") failed";
		PrintError(str.str());
	}
	return sub_bitmap;
}

ALLEGRO_BITMAP* wrap_load_bitmap(const char* filename) {
	ALLEGRO_BITMAP* ret = al_load_bitmap(filename);
	if (ret == NULL) {
		ostringstream str;
		str << "al_load_bitmap(" << filename << ") failed";
		PrintError(str.str());
	}
	return ret;
}

void wrap_destroy_bitmap(ALLEGRO_BITMAP* bitmap) {
	al_destroy_bitmap(bitmap);
}

int wrap_get_bitmap_width(ALLEGRO_BITMAP* bitmap) {
	return al_get_bitmap_width(bitmap);
}

int wrap_get_bitmap_height(ALLEGRO_BITMAP* bitmap) {
	return al_get_bitmap_height(bitmap);
}

void wrap_set_target_bitmap(ALLEGRO_BITMAP* bitmap) {
	al_set_target_bitmap(bitmap);
}

ALLEGRO_BITMAP* wrap_get_target_bitmap() {
	return al_get_target_bitmap();
}

void wrap_draw_bitmap(ALLEGRO_BITMAP* bitmap, float dx, float dy, int flags) {
	al_draw_bitmap(bitmap, dx, dy, flags);
}

void wrap_draw_scaled_rotated_bitmap(ALLEGRO_BITMAP* bitmap, float cx, float cy, float dx, float dy, float xscale,
									 float yscale, float angle, int flags) {
	al_draw_scaled_rotated_bitmap(bitmap, cx, cy, dx, dy, xscale, yscale, angle, flags);
}

ALLEGRO_BITMAP* wrap_get_backbuffer(ALLEGRO_DISPLAY* display) {
	return al_get_backbuffer(display);
}
ALLEGRO_COLOR wrap_map_rgb(unsigned char r, unsigned char g, unsigned char b) {
	return al_map_rgb(r, g, b);
}
ALLEGRO_COLOR wrap_map_rgba(unsigned char r, unsigned char g, unsigned char b, unsigned char a) {
	return al_map_rgba(r, g, b, a);
}
void wrap_clear_to_color(ALLEGRO_COLOR color) {
	al_clear_to_color(color);
}

//Keyboard routines
bool wrap_install_keyboard(void) {
	bool ret = al_install_keyboard();
	if (ret == false) {
		ostringstream str;
		str << "al_install_keyboard() failed";
		PrintError(str.str());
	}
	return ret;
}
void wrap_get_keyboard_state(ALLEGRO_KEYBOARD_STATE* ret_state) {
	al_get_keyboard_state(ret_state);
}
bool wrap_key_down(const ALLEGRO_KEYBOARD_STATE* state, int keycode) {
	return al_key_down(state, keycode);
}
ALLEGRO_EVENT_SOURCE* wrap_get_keyboard_event_source(void) {
	ALLEGRO_EVENT_SOURCE* ret = al_get_keyboard_event_source();
	if (ret == NULL) {
		ostringstream str;
		str << "al_get_keyboard_event_source() failed";
		PrintError(str.str());
	}
	return ret;
}
//Monitors

//Mouse routines

//System routines
bool wrap_init(void) {
	bool ret = al_init();
	if (ret == false) PrintError("al_init() failed");
	return ret;
}
//Time routines
double wrap_get_time(void) {
	return al_get_time();
}

void wrap_rest(double seconds) {
	al_rest(seconds);
}
//Timer routines

//Font addons
bool wrap_init_font_addon(void) {
	bool ret = al_init_font_addon();
	if (ret == false) PrintError("al_init_font_addon() failed");
	return ret;
}
bool wrap_init_ttf_addon(void) {
	bool ret = al_init_ttf_addon();
	if (ret == false) PrintError("al_init_ttf_addon() failed");
	return ret;
}
void wrap_destroy_font(ALLEGRO_FONT* f) {
	al_destroy_font(f);
}
ALLEGRO_FONT* wrap_load_ttf_font(char const* filename, int size, int flags) {
	ALLEGRO_FONT* ret = al_load_ttf_font(filename, size, flags);
	if (ret == NULL) {
		ostringstream str;
		str << "al_load_ttf_font(" << filename << "," << size << "," << flags << ") failed";
		PrintError(str.str());
	}
	return ret;
}
void wrap_draw_text(const ALLEGRO_FONT* font, ALLEGRO_COLOR color, float x, float y, int flags, char const* text) {
	al_draw_text(font, color, x, y, flags, text);
}
void wrap_draw_multiline_text(const ALLEGRO_FONT* font, ALLEGRO_COLOR color, float x, float y, float max_width, float line_height, int flags, const char* text) {
	al_draw_multiline_text(font, color, x, y, max_width, line_height, flags, text);
}
//Image IO addon
bool wrap_init_image_addon(void) {
	bool ret = al_init_image_addon();
	if (ret == false) PrintError("al_init_image_addon() failed");
	return ret;
}

//Primitives addon
bool wrap_init_primitives_addon(void) {
	bool ret = al_init_primitives_addon();
	if (ret == false) PrintError("al_init_primitives_addon() failed");
	return ret;
}
void wrap_draw_rectangle(float x1, float y1, float x2, float y2, ALLEGRO_COLOR color, float thickness) {
	al_draw_rectangle(x1, y1, x2, y2, color, thickness);
}
void wrap_draw_filled_rectangle(float x1, float y1, float x2, float y2, ALLEGRO_COLOR color) {
	al_draw_filled_rectangle(x1, y1, x2, y2, color);
}

// Tranformation Routines and Addons
void wrap_use_transform(const ALLEGRO_TRANSFORM* trans) {
	al_use_transform(trans);
}

void wrap_identity_transform(ALLEGRO_TRANSFORM* trans) {
	al_identity_transform(trans);
}

void wrap_build_transform(ALLEGRO_TRANSFORM* trans, float x, float y, float sx, float sy, float theta) {
	al_build_transform(trans, x, y, sx, sy, theta);
}

void wrap_scale_transform(ALLEGRO_TRANSFORM* trans, float sx, float sy) {
	al_scale_transform(trans, sx, sy);
}

//Audio Codecs Addon
bool wrap_init_acodec_addon(void) {
	bool ret = al_init_acodec_addon();
	if (ret == false) PrintError("al_init_acodec_addon() failed");
	return ret;
}

//Audio Addon
bool wrap_install_audio(void) {
	bool ret = al_install_audio();
	if (ret == false) PrintError("al_install_audio() failed");
	return ret;
}
void wrap_uninstall_audio(void) {
	al_uninstall_audio();
}
//Audio Samples
bool wrap_reserve_samples(int reserve_samples) {
	bool ret = al_reserve_samples(reserve_samples);
	if (ret == false) PrintError("al_reserve_samples() failed");
	return ret;
}
ALLEGRO_SAMPLE* wrap_load_sample(const char* filename) {
	ALLEGRO_SAMPLE* ret = al_load_sample(filename);
	if (ret == NULL) {
		ostringstream str;
		str << "al_load_sample(" << filename << ") failed";
		PrintError(str.str());
	}
	return ret;
}
void wrap_destroy_sample(ALLEGRO_SAMPLE* spl) {
	al_destroy_sample(spl);
}
bool wrap_play_sample(ALLEGRO_SAMPLE* spl, float gain, float pan, float speed, ALLEGRO_PLAYMODE loop, ALLEGRO_SAMPLE_ID* ret_id) {
	bool ret = al_play_sample(spl, gain, pan, speed, loop, ret_id);
	if (ret == false) PrintError("al_play_sample(...) failed");
	return ret;
}
//Audio Sample Instances
ALLEGRO_SAMPLE_INSTANCE* wrap_create_sample_instance(ALLEGRO_SAMPLE* sample_data) {
	ALLEGRO_SAMPLE_INSTANCE* ret = al_create_sample_instance(sample_data);
	if (ret == NULL) {
		ostringstream str;
		str << "al_create_sample_instance(...) failed";
		PrintError(str.str());
	}
	return ret;
}
void wrap_destroy_sample_instance(ALLEGRO_SAMPLE_INSTANCE* spl) {
	al_destroy_sample_instance(spl);
}
bool wrap_play_sample_instance(ALLEGRO_SAMPLE_INSTANCE* spl) {
	bool ret = al_play_sample_instance(spl);
	if (ret == false) PrintError("al_play_sample_instance(...) failed");
	return ret;
}

bool wrap_stop_sample_instance(ALLEGRO_SAMPLE_INSTANCE* spl) {
	bool ret = al_stop_sample_instance(spl);
	if (ret == false) PrintError("al_stop_sample_instance(...) failed");
	return ret;
}

bool wrap_set_sample_instance_playmode(ALLEGRO_SAMPLE_INSTANCE* spl, ALLEGRO_PLAYMODE val) {
	bool ret = al_set_sample_instance_playmode(spl, val);
	if (ret == false) PrintError("al_set_sample_instance_playmode(...) failed");
	return ret;
}

bool wrap_attach_sample_instance_to_mixer(ALLEGRO_SAMPLE_INSTANCE* spl, ALLEGRO_MIXER* mixer) {
	bool ret = al_attach_sample_instance_to_mixer(spl, mixer);
	if (ret == false) PrintError("al_attach_sample_instance_to_mixer(...) failed");
	return ret;
}
