#include "InputController.h"
#include <algorithm>
#include "Wrappers.h"

using namespace std;
std::map<std::string, int> Input::keyMap = {
	// Player 1 controls
	{"ALLEGRO_KEY_W", ALLEGRO_KEY_W},
	{"ALLEGRO_KEY_A", ALLEGRO_KEY_A},
	{"ALLEGRO_KEY_S", ALLEGRO_KEY_S},
	{"ALLEGRO_KEY_D", ALLEGRO_KEY_D},
	{"ALLEGRO_KEY_V", ALLEGRO_KEY_V},
	{"ALLEGRO_KEY_B", ALLEGRO_KEY_B},
	{"ALLEGRO_KEY_SPACE", ALLEGRO_KEY_SPACE},
	{"ALLEGRO_KEY_ALT", ALLEGRO_KEY_ALT},
	// ******
	{"ALLEGRO_KEY_C", ALLEGRO_KEY_C},
	{"ALLEGRO_KEY_N", ALLEGRO_KEY_N},

	// Player 2 controls
	{"ALLEGRO_KEY_UP", ALLEGRO_KEY_UP},
	{"ALLEGRO_KEY_LEFT", ALLEGRO_KEY_LEFT},
	{"ALLEGRO_KEY_DOWN", ALLEGRO_KEY_DOWN},
	{"ALLEGRO_KEY_RIGHT", ALLEGRO_KEY_RIGHT},
	{"ALLEGRO_KEY_RCTRL", ALLEGRO_KEY_RCTRL},
	{"ALLEGRO_KEY_COMMA", ALLEGRO_KEY_COMMA},
	{"ALLEGRO_KEY_FULLSTOP", ALLEGRO_KEY_FULLSTOP},
	{"ALLEGRO_KEY_SLASH", ALLEGRO_KEY_SLASH},
	{"ALLEGRO_KEY_BACKSLASH2", ALLEGRO_KEY_BACKSLASH2},
	{"ALLEGRO_KEY_RSHIFT", ALLEGRO_KEY_RSHIFT},
	{"ALLEGRO_KEY_PAD_0", ALLEGRO_KEY_PAD_0},
	{"ALLEGRO_KEY_PAD_1", ALLEGRO_KEY_PAD_1},
	{"ALLEGRO_KEY_PAD_2", ALLEGRO_KEY_PAD_2},
	{"ALLEGRO_KEY_PAD_3", ALLEGRO_KEY_PAD_3}};

extern bool
Input::test_key(const std::string& keyCode) {
	//TODO with allegro Done not tested-myron
	auto key(Input::keyMap.find(keyCode));
	assert(key != Input::keyMap.end());
	ALLEGRO_KEYBOARD_STATE ret_state;
	wrap_get_keyboard_state(&ret_state);
	return wrap_key_down(&ret_state, key->second);
}

inline bool Input::test_keys(const key_combination& keys) {
	for (auto& key : keys)
		if (!Input::test_key(key))
			return false;
	return true;
}

// Get the parsed input filter it and return the largest combo
// in case two or more combos are of the same lenght (measured by ".")
// then it returns the first of them in lexicographic order.
// Note that this function always returns a set with only one string in it
// on none if the input is empty
const Input::Logical Input::FilterLogical(const Logical& input) {
	int count = -1;
	std::string MaximumCombo;
	std::set<std::string> FilteredInput;

	if (!input.empty()) {
		for (auto& str : input) {
			if (std::count(str.begin(), str.end(), '.') > count) {
				count = std::count(str.begin(), str.end(), '.');
				MaximumCombo = str;
			}
		}
	}
	FilteredInput.insert(MaximumCombo);
	return FilteredInput;
}

//InputController
InputController::All InputController::all;
void InputController::SetLogical(const std::string& id) {
	logical.insert(id);
}

//public
InputController::InputController(void) {
	all.push_back(this);
}
InputController::~InputController() {
	all.remove(this);
}
void InputController::AddAction(const key_combination& keys, const std::string& logicalInput) {
	actions.push_back(std::make_pair(keys, logicalInput));
}
const InputController::Logical& InputController::GetLogical(void) const {
	return logical;
}
void InputController::Handle(void) {
	// logical.clear();
	for (auto& i : actions)
		if (Input::test_keys(i.first))
			SetLogical(i.second);
}
void InputController::HandleAll(void) {
	for (auto* handler : all)
		handler->Handle();
}

void InputController::ClearLogical(void) {
	logical.clear();
}
