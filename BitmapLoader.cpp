#include "BitmapLoader.h"
#include "Wrappers.h"

using namespace std;
BitmapLoader::Bitmaps BitmapLoader::bitmaps;
void BitmapLoader::Initialize(void) {}
void BitmapLoader::CleanUp(void) {
	for (Bitmaps::iterator i = bitmaps.begin(); i != bitmaps.end(); ++i) {
		wrap_destroy_bitmap(i->second);
	}
	bitmaps.clear();
}
Bitmap BitmapLoader::GetBitmap(const std::string path) {
	Bitmaps::const_iterator i = bitmaps.find(path);
	return i != bitmaps.end() ? i->second : nullptr;
}
Bitmap BitmapLoader::Load(const std::string& path) {
	Bitmap b = GetBitmap(path);
	if (!b) {
		bitmaps[path] = (b = wrap_load_bitmap(path.c_str()));
		assert(b);	//path should contain a valid picture
	}
	return b;
}
