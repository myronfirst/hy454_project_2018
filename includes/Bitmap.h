#ifndef _BITMAP_H
#define _BITMAP_H

#include <allegro5/allegro.h>
#include "Utilities.h"

using Bitmap = ALLEGRO_BITMAP*;

namespace BitmapAPI {
	Bitmap Create(unsigned width, unsigned height);
	bool Destroy(Bitmap bmp);
	Bitmap GetScreen();	//Get the backbuffer  of the active display
	unsigned GetWidth(Bitmap bmp);
	unsigned GetHeight(Bitmap bmp);
	//This function blits the src Sub Bitmap to the  Backbuffer of the current display
	void Blit(Bitmap src, const Rect& from, Point paintCoords);
	void Blit(Bitmap src, const Rect& from, Point paintCoords, int flags);
	void Blit(Bitmap src, const Rect& from, Bitmap dest, Point paintCoords);
	void Blit(Bitmap src, const Rect& from, Bitmap dest, Point paintCoords, int flags);
	void SetTargetBitmap(Bitmap bmp);
	Bitmap GetTargetBitmap();
}	// namespace BitmapAPI

#endif
