#ifndef _FRAME_RANGE_ANIMATION_H_
#define _FRAME_RANGE_ANIMATION_H_

#include "MovingAnimation.h"

using frame_t = unsigned long;

class FrameRangeAnimation : public MovingAnimation {
	frame_t start, end;

  public:
	FrameRangeAnimation(animd_t _id, offset_t _dx, offset_t _dy, delay_t _delay, bool _continuous, frame_t _start, frame_t _end);
	virtual ~FrameRangeAnimation();
	Animation* Clone(animd_t newId) const override final;
	frame_t GetStart(void) const;
	void SetStart(frame_t _start);
	frame_t GetEnd(void) const;
	void SetEnd(frame_t _end);
};

#endif
