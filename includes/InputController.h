#ifndef _INPUT_CONTROLLER_H_
#define _INPUT_CONTROLLER_H_

#include <list>
#include <map>
#include <set>
#include <string>

using key_combination = std::list<std::string>;

namespace Input {
	using Logical = std::set<std::string>;
	extern std::map<std::string, int> keyMap;
	extern bool test_key(const std::string& keyCode);
	inline bool test_keys(const key_combination& keys);
	const Logical FilterLogical(const Logical& input);
}	//nameSpace Input

class InputController final {
  public:
	using Logical = std::set<std::string>;

  public:	//private
	using Actions = std::list<std::pair<key_combination, std::string>>;
	using All = std::list<InputController*>;
	Actions actions;
	Logical logical;
	static All all;	// TODO: define it in a cpp file! Done-myron
	void SetLogical(const std::string& id);

  public:
	~InputController();
	InputController();
	void AddAction(const key_combination& keys, const std::string& logicalInput);
	const Logical& GetLogical(void) const;
	void Handle(void);
	static void HandleAll(void);
	void ClearLogical(void);
};

#endif
