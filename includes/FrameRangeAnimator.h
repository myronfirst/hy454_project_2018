#ifndef _FRAME_RANGE_ANIMATOR_H
#define _FRAME_RANGE_ANIMATOR_H

#include "Animator.h"
#include "FrameRangeAnimation.h"
#include "Sprite.h"

class FrameRangeAnimator : public Animator {
  private:
	Sprite* sprite;
	FrameRangeAnimation* anim;
	frame_t currFrame;

  public:
	FrameRangeAnimator();
	virtual ~FrameRangeAnimator();
	void Start(Sprite* s, FrameRangeAnimation* a, timestamp_t t);
	void Progress(timestamp_t currTime) override final;
};

#endif
