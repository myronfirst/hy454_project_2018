#ifndef _PROJECTILE_H_
#define _PROJECTILE_H_

#include "FrameListAnimator.h"
#include "Sprite.h"
#include "list"
class Projectile {
  private:
	Sprite* sprite;
	FrameListAnimator* animator;
	std::string name;

  public:
	~Projectile();
	Projectile(Sprite* _sprite, std::string _name, std::string animationFilmID);
	bool toBeDeleted;
	Sprite* GetSprite();
	FrameListAnimator* GetAnimator();
	std::string GetName();
	bool IsOutOfBounds();
};

class ProjectileHolder {
  private:
	static std::list<Projectile*> projectilesList;

  public:
	static void AddNewProjectile(Projectile* p);
};

#endif
