#ifndef _ANIMATION_H_
#define _ANIMATION_H_

#include <string>

using animd_t = std::string;

class Animation {
  private:
	animd_t id;

  public:
	Animation(animd_t _id);
	virtual ~Animation();
	virtual Animation* Clone(animd_t newId) const = 0;
	animd_t GetId(void) const;
};
#endif
