#ifndef _COLLISION_HANDLERS_H
#define _COLLISION_HANDLERS_H

#include "Fighter.h"
#include "Projectile.h"

class CollisionData {
  private:
	std::string filmID;
	std::string newForcedState;
	int damage;

  public:
	CollisionData(std::string film_name, std::string newState, int dmg);
	~CollisionData();

	void SetFilmName(std::string name);
	std::string GetFilmName();
	void SetNewForcedState(std::string state);
	std::string GetNewForcedState();
	void SetDamage(int dmg);
	int GetDamage();
};

class ProjectileCollisionData {
  private:
	std::string filmID;
	std::string newForcedState;
	int damage;

  public:
	ProjectileCollisionData(/* args */);
	~ProjectileCollisionData();
};

void SeparateSprites(Sprite* player, Sprite* enemy);

void FighterToFighterCollisionHandler(Fighter* player, Fighter* enemy);

void ProjectileToFighterCollisionHandler(Fighter* player, Fighter* enemy);
/* struct CollisionHandler {
	CollisionHandler();
	virtual ~CollisionHandler();
	virtual CollisionHandler* Clone(void) const = 0;
	virtual void operator()(Sprite* caller, Sprite* arg) const = 0;
};

struct FightersCollisionHandler : public CollisionHandler {
	FightersCollisionHandler();
	virtual ~FightersCollisionHandler() override;
	virtual CollisionHandler* Clone(void) const override final;
	virtual void operator()(Sprite* player, Sprite* enemy) const override final;
}; */

void CheckFatality(Fighter* player, Fighter* enemy);
#endif
