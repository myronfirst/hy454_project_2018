#ifndef _SPRITE_H
#define _SPRITE_H

#include "AnimationFilm.h"
#include "Utilities.h"

#include <algorithm>
#include <list>

using byte = std::uint8_t;
// using Dim = signed short;
using Dim = unsigned int;

class Sprite {
  private:
	//TileLayer* myLayer;
	byte frameNo;
	Rect frameBox;
	Dim vFrameAllign, hFrameAllign;
	Dim x, y;	// Display x, y position
	bool isVisible;
	const AnimationFilm* currFilm;
	void MovePriv(int dx, int dy);

  public:
	Sprite(Dim _x, Dim _y, const AnimationFilm* Film);
	~Sprite();
	void SetFrame(byte i);
	byte GetFrameNo(void) const;
	const Rect& GetFrameBox(void) const;
	Dim GetX(void) const;
	Dim GetY(void) const;
	Dim GethFrameAllign(void) const;
	Dim GetvFrameAllign(void) const;
	void SetVisibility(bool v);
	bool IsVisible(void) const;
	const AnimationFilm* GetFilm(void) const;
	void SetFilm(const AnimationFilm* newFilm);

	void Move(int dx, int dy);

	// void AddCollisionHandler(const CollisionHandler& h);
	// void ClearHandlers(void);
	bool CollisionCheck(Sprite* enemy);
	// void Display(Bitmap dest, const Rect& da);
	// void Display(Bitmap dest, const Point& at);
	void Display(Bitmap dest, int direction);
	void DisplayFrameBox(ALLEGRO_COLOR color);
};

#endif
