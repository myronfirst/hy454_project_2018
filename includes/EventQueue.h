#ifndef _EVENT_QUEUE_H_
#define _EVENT_QUEUE_H_

#include "Wrappers.h"

class EventQueue {
  private:
	ALLEGRO_EVENT_QUEUE* queue;

  public:
	EventQueue();
	~EventQueue();
	void RegisterSource(ALLEGRO_EVENT_SOURCE* source);
	bool IsEmpty() const;
	ALLEGRO_EVENT_QUEUE* GetQueue() const;
	ALLEGRO_EVENT GetNextEvent() const;
	ALLEGRO_EVENT WaitNextEvent() const;
};

#endif
