#ifndef _STATE_TRANSITIONS_H_
#define _STATE_TRANSITIONS_H_

#include <functional>
#include <list>
#include <map>
#include <set>
#include <string>

class StateTransitions final {
  public:
	using Input = std::set<std::string>;

  private:
	using Inputs = std::map<std::string, std::list<Input>>;
	using Table = std::map<std::string, std::function<void(void)>>;
	Table table;
	Inputs inputs;
	std::string state;

	const std::string MakeKey(const Input& input) const;
	void PerformDefaultTransition(void);
	void FireTransitions(const std::set<std::string>& keys);
	void InsertByRetainingMaximalEdges(std::set<std::string>& keys, const std::string& fired);

  public:
	StateTransitions(StateTransitions&&) = delete;
	StateTransitions(const StateTransitions&) = delete;
	StateTransitions() = default;

	// template<typename Tfunc>
	StateTransitions& SetTransition(
		const std::string& from,
		const Input& input,
		const std::function<void(void)>& f);
	StateTransitions& SetState(const std::string& newState);
	// all plausible transitions are fired
	StateTransitions& PerformTransitions(const Input& input, bool useMaximalEdges);
	const std::string& GetState(void) const;
};

#endif
