#ifndef _MOVING_PATH_ANIMATION_H
#define _MOVING_PATH_ANIMATION_H

#include "Animation.h"

#include <vector>

using offset_t = signed short;
using frame_t = unsigned long;
using delay_t = unsigned long;

struct PathEntry {
	offset_t dx, dy;
	frame_t frame;
	delay_t delay;
	PathEntry();
	PathEntry(offset_t _dx, offset_t _dy, frame_t _frame, delay_t _delay);
	PathEntry(const PathEntry& p);
	~PathEntry();
};

class MovingPathAnimation : public Animation {
  private:
	bool continuous;
	std::vector<PathEntry> path;

  public:
	MovingPathAnimation(animd_t _id, bool _continuous, const std::vector<PathEntry>& _path);
	~MovingPathAnimation();
	Animation* Clone(animd_t newId) const;
	bool GetContinuous(void) const;
	void SetContinuous(bool _continuous);
	const std::vector<PathEntry>& GetPath(void) const;
	void SetPath(const std::vector<PathEntry>& _path);
};

#endif
