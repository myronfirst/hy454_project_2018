#ifndef _FIGHTER_H_
#define _FIGHTER_H_

#include <set>
#include <string>
#include "FrameListAnimator.h"
#include "FrameRangeAnimator.h"
#include "InputController.h"
#include "MovingAnimator.h"
#include "MovingPathAnimator.h"
#include "Projectile.h"
#include "Sprite.h"
#include "StateTransitions.h"
#include "TickAnimation.h"
#include "TickAnimator.h"
#include "Wrappers.h"

#define FACE_RIGHT 0
#define FACE_LEFT ALLEGRO_FLIP_HORIZONTAL
extern const int FIGHTER_ACTION_DELAY_MSECS;

enum AnimatorType { MOVING_ANIMATOR,
					MOVING_PATH_ANIMATOR,
					FRAME_LIST_ANIMATOR,
					FRAME_RANGE_ANIMATOR,
					FREEZEBALL_ANIMATOR,
					LIGHTINGBOLT_ANIMATOR
};

enum FighterState {
	NORMAL = 0,
	ATTACK = 1,
	ATTACKED = 2,
	BLOCK = 3,
};

class Fighter final {
  private:
	Sprite* sprite;
	// this is needed to store the correct  Action input for each player.
	// In the future if we want to load the the keybindings from a config file
	// then this member will be a string that contains the path of the correct
	// config file.
	InputController::Actions* keybindings;
	//Collision
	FighterState fighterState;
	int health;

	std::string name;
	std::string nextAction;
	int faceDir;

	bool FinishHim;

	InputController inputController;
	StateTransitions* stateTransitions;

	MovingAnimator* movingAnimator;
	MovingPathAnimator* movingPathAnimator;
	FrameListAnimator* frameListAnimator;
	FrameRangeAnimator* frameRangeAnimator;
	TickAnimator* tickAnimator;	// deferred firing actions; always dynamic //unused (?)
	TickAnimation* tickAnim;

	Projectile* projectile;

	void
	LoadKeybindings(InputController::Actions* actions);
	void InitializeStates();
	void CleanUp();

  public:
	Fighter(Sprite* _sprite, std::string _nextAction, std::string _name, int facing_direction, InputController::Actions* keybinds);
	~Fighter();

	bool IsAnimatorRunning(void);
	void SetTransitions(std::string state, const StateTransitions::Input& input, std::string film, AnimatorType type, std::string newState, FighterState _fighterState);
	void ProgressAnimators(timestamp_t time);
	timestamp_t StopAnimators(void);
	void TimeShiftAnimators(timestamp_t offset);
	// TODO: CleanUP
	InputController* GetInputController();
	Sprite* GetSprite();
	StateTransitions* GetStateTransitions();
	FrameRangeAnimator* GetFrameRangeAnimator();
	FrameListAnimator* GetFrameListAnimator();
	MovingPathAnimator* GetMovingPathAnimator();
	MovingAnimator* GetMovingAnimator();

	const std::string& GetName(void) const;
	FighterState GetState(void) const;
	void SetState(FighterState _fighterState);
	int GetHealth(void) const;
	void SetHealth(int _health);
	void ReduceHealth(int damage);
	void ForceFrameRangeAnimation(animd_t id);
	void ForceFrameListAnimation(animd_t id);
	void DisplayFrameBox(void);
	void DisplayHealth(ALLEGRO_FONT* font, const Point& pos);
	void SetFacingDirection(int _faceDir);
	int GetFacingDirection();
	Projectile* GetProjectile();
	void SetFinisher(bool b);
	bool GetFinisher();
};

void CheckFighterFacingDirection(Fighter* p1, Fighter* p2);
#endif
