#ifndef _TICK_ANIMATION_H
#define _TICK_ANIMATION_H

#include "Animation.h"

#include <functional>

using delay_t = unsigned long;

class TickAnimation : public Animation {
  public:
	using TickFunc = std::function<void(void)>;

  private:
	delay_t delay;
	unsigned repetitions;
	TickFunc action;

  public:
	TickAnimation(animd_t _id, delay_t _delay, unsigned _repetitions, TickFunc _action);
	virtual ~TickAnimation();
	Animation* Clone(animd_t newId) const;
	delay_t GetDelay(void) const;
	void SetDelay(delay_t _delay);
	unsigned GetRepetitions(void) const;
	void SetRepetitions(unsigned _repetitions);
	TickFunc GetAction(void) const;
	void SetAction(TickFunc _action);
};

#endif
