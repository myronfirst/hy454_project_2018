#ifndef _PAUSE_GAME_H_
#define _PAUSE_GAME_H_

#include "EventQueue.h"
#include "Fighter.h"

bool PauseGame(EventQueue* queue, ALLEGRO_FONT* font, Fighter* player, Fighter* enemy);
bool StartScreen(EventQueue* queue);
void DisplayCheckVictory(ALLEGRO_FONT* font, Fighter* player, Fighter* enemy);
void DisplayHealthBars(ALLEGRO_FONT* font, Fighter* player1, Fighter* player2);

#endif
