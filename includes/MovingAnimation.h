#ifndef _MOVING_ANIMATION_H_
#define _MOVING_ANIMATION_H_

#include "Animation.h"

using offset_t = signed short;
using delay_t = unsigned long;

class MovingAnimation : public Animation {
  private:
	offset_t dx, dy;
	delay_t delay;
	bool continuous;

  public:
	MovingAnimation(animd_t _id, offset_t _dx, offset_t _dy, delay_t _delay, bool _continuous);
	virtual ~MovingAnimation();
	virtual Animation* Clone(animd_t newId) const override;
	offset_t GetDx(void) const;
	void SetDx(offset_t _dx);
	offset_t GetDy(void) const;
	void SetDy(offset_t _dy);
	delay_t GetDelay(void) const;
	void SetDelay(delay_t _delay);
	bool GetContinuous(void) const;
	void SetContinuous(bool _continuous);
};

#endif
