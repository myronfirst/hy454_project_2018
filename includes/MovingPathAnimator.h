#ifndef _MOVING_PATH_ANIMATOR_H
#define _MOVING_PATH_ANIMATOR_H

#include "Animator.h"
#include "MovingPathAnimation.h"
#include "Sprite.h"

class MovingPathAnimator : public Animator {
  private:
	Sprite* sprite;
	MovingPathAnimation* anim;
	unsigned currEl;

  public:
	MovingPathAnimator();
	virtual ~MovingPathAnimator();
	void Start(Sprite* s, MovingPathAnimation* a, timestamp_t t);
	void Progress(timestamp_t currTime) override final;
};

#endif
