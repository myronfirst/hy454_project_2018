#ifndef _ANIMATION_DATA_HOLDER_H
#define _ANIMATION_DATA_HOLDER_H

#include <map>
#include <string>
#include "FrameListAnimation.h"
#include "FrameRangeAnimation.h"
#include "MovingAnimation.h"
#include "MovingPathAnimation.h"

class AnimationDataHolder {
  private:
	using MovingMap = std::map<std::string, MovingAnimation*>;
	using MovingPathMap = std::map<std::string, MovingPathAnimation*>;
	using FrameListMap = std::map<std::string, FrameListAnimation*>;
	using FrameRangeMap = std::map<std::string, FrameRangeAnimation*>;

	static MovingMap movingAnimations;
	static MovingPathMap movingPathAnimations;
	static FrameListMap frameListAnimations;
	static FrameRangeMap frameRangeAnimations;

  public:
	static void Initialize(void);
	static void CleanUp(void);
	static void Load(const std::string& catalogue);
	static MovingAnimation* GetMovingAnimation(const std::string& id);
	static MovingPathAnimation* GetMovingPathAnimation(const std::string& id);
	static FrameRangeAnimation* GetFrameRangeAnimation(const std::string& id);
	static FrameListAnimation* GetFrameListAnimation(const std::string& id);
};

#endif
