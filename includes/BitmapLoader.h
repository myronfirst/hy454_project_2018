#ifndef _BITMAP_LOADER_H
#define _BITMAP_LOADER_H

#include <map>
#include <string>
#include "Bitmap.h"

class BitmapLoader {
  private:
	using Bitmaps = std::map<std::string, Bitmap>;
	static Bitmaps bitmaps;

  public:
	// BitmapLoader();
	// ~BitmapLoader();
	static void Initialize(void);
	static void CleanUp(void);
	static Bitmap GetBitmap(const std::string path);
	static Bitmap Load(const std::string& path);
};

#endif
