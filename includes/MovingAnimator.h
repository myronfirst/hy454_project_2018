#ifndef _MOVING_ANIMATOR_H
#define _MOVING_ANIMATOR_H

#include "Animator.h"
#include "MovingAnimation.h"
#include "Sprite.h"

class MovingAnimator : public Animator {
  private:
	Sprite* sprite;
	MovingAnimation* anim;

  public:
	MovingAnimator();
	virtual ~MovingAnimator();
	void Start(Sprite* s, MovingAnimation* a, timestamp_t t);
	void Progress(timestamp_t currTime) override final;
};

#endif
