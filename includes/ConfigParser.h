#ifndef _CONFIG_PARSER_H
#define _CONFIG_PARSER_H

#include <allegro5/allegro.h>
#include <string>

class ConfigParser {
  private:
	ALLEGRO_CONFIG* config;
	ALLEGRO_CONFIG_SECTION* sectionIterator;
	ALLEGRO_CONFIG_ENTRY* entryIterator;

  public:
	ConfigParser(const std::string& path);
	~ConfigParser();
	void LoadConfigFile(const std::string& path);
	ALLEGRO_CONFIG* GetConfigFile(void);
	//Methods return empty string on default section or nullptr
	const std::string GetConfigValue(const std::string& section, const std::string& key);
	const std::string GetConfigFirstSection();
	const std::string GetConfigNextSection();
	const std::string GetConfigFirstKey(const std::string& section);
	const std::string GetConfigNextKey();
};

#endif
