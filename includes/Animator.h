#ifndef _ANIMATOR_H
#define _ANIMATOR_H
#include <functional>

using timestamp_t = unsigned long;

enum animatorstate_t {
	ANIMATOR_FINISHED = 0,
	ANIMATOR_RUNNING = 1,
	ANIMATOR_STOPPED = 2
};

class Animator {
  public:
	// using FinishCallBack = std::function<void(Animator*, void*)>;
	using FinishCallBack = std::function<void(Animator*)>;

  protected:
	timestamp_t lastTime;
	animatorstate_t state;
	FinishCallBack onFinish;
	// void* finishClosure;
	void NotifyStopped(void);

  public:
	Animator();
	virtual ~Animator();
	void Stop(void);
	bool IsSuspended(void) const;
	virtual void TimeShift(timestamp_t offset);
	virtual void Progress(timestamp_t currTime) = 0;
	// void SetOnFinish(FinishCallBack f, void* c);
	void SetOnFinish(FinishCallBack f);
};

#endif
