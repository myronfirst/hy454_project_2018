#ifndef _FRAME_LIST_ANIMATION_H_
#define _FRAME_LIST_ANIMATION_H_

#include "MovingAnimation.h"

#include <vector>

using frame_t = unsigned long;

class FrameListAnimation : public MovingAnimation {
	std::vector<frame_t> frames;

  public:
	FrameListAnimation(animd_t _id, offset_t _dx, offset_t _dy, delay_t _delay, bool _continuous, const std::vector<frame_t>& _frames);
	virtual ~FrameListAnimation();
	Animation* Clone(animd_t newId) const override final;
	const std::vector<frame_t>& GetFrames(void) const;
	void SetFrames(const std::vector<frame_t>& _frames);
};
#endif
