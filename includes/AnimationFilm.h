#ifndef _ANIMATION_FILM_H
#define _ANIMATION_FILM_H

#include "Bitmap.h"
#include "Utilities.h"

#include <string>
#include <vector>

class AnimationFilm {
  private:
	std::string id;
	Bitmap bitmap;
	std::vector<Rect> rects;

  public:
	AnimationFilm(const std::string& id, Bitmap bitmap, const std::vector<Rect> rects);
	~AnimationFilm();
	unsigned GetTotalFrames(void) const;
	Bitmap GetBitmap(void) const;
	const std::string GetId(void) const;
	const Rect GetFrameBox(unsigned frameNo) const;
	void DisplayFrame(Bitmap dest, const Point& at, unsigned frameNo) const;	// add const later
};

#endif
