#ifndef _WRAPPERS_H_
#define _WRAPPERS_H_

#include <allegro5/allegro.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_font.h>

//Categories from Allegro 5 Reference Manual

//Configuration files
ALLEGRO_CONFIG* wrap_create_config(void);
void wrap_destroy_config(ALLEGRO_CONFIG* config);
ALLEGRO_CONFIG* wrap_load_config_file(const char* filename);
const char* wrap_get_config_value(const ALLEGRO_CONFIG* config, const char* section, const char* key);
char const* wrap_get_first_config_section(ALLEGRO_CONFIG const* config, ALLEGRO_CONFIG_SECTION** iterator);
char const* wrap_get_next_config_section(ALLEGRO_CONFIG_SECTION** iterator);
char const* wrap_get_first_config_entry(ALLEGRO_CONFIG const* config, char const* section, ALLEGRO_CONFIG_ENTRY** iterator);
char const* wrap_get_next_config_entry(ALLEGRO_CONFIG_ENTRY** iterator);
//Displays
void wrap_set_new_display_flags(int flags);
void wrap_set_new_display_option(int option, int value, int importance);
ALLEGRO_DISPLAY* wrap_create_display(int w, int h);
void wrap_destroy_display(ALLEGRO_DISPLAY* display);
ALLEGRO_DISPLAY* wrap_get_current_display();
void wrap_flip_display(void);
void wrap_set_window_title(ALLEGRO_DISPLAY* display, const char* title);
//Events
ALLEGRO_EVENT_QUEUE* wrap_create_event_queue(void);
void wrap_destroy_event_queue(ALLEGRO_EVENT_QUEUE* queue);
void wrap_register_event_source(ALLEGRO_EVENT_QUEUE* queue, ALLEGRO_EVENT_SOURCE* source);
void wrap_wait_for_event(ALLEGRO_EVENT_QUEUE* queue, ALLEGRO_EVENT* ret_event);
bool wrap_is_event_queue_empty(ALLEGRO_EVENT_QUEUE* queue);
bool wrap_get_next_event(ALLEGRO_EVENT_QUEUE* queue, ALLEGRO_EVENT* ret_event);
//Fullscreen modes
//Graphics routines
void wrap_set_new_bitmap_flags(int flags);
ALLEGRO_BITMAP* wrap_create_bitmap(int width, int height);
ALLEGRO_BITMAP* wrap_create_sub_bitmap(ALLEGRO_BITMAP* parent, int x, int y, int w, int h);
ALLEGRO_BITMAP* wrap_load_bitmap(const char* filename);
void wrap_destroy_bitmap(ALLEGRO_BITMAP* bitmap);
int wrap_get_bitmap_width(ALLEGRO_BITMAP* bitmap);
int wrap_get_bitmap_height(ALLEGRO_BITMAP* bitmap);
void wrap_set_target_bitmap(ALLEGRO_BITMAP* bitmap);
ALLEGRO_BITMAP* wrap_get_target_bitmap();
void wrap_draw_bitmap(ALLEGRO_BITMAP* bitmap, float dx, float dy, int flags);
ALLEGRO_BITMAP* wrap_get_backbuffer(ALLEGRO_DISPLAY* display);
ALLEGRO_COLOR wrap_map_rgb(unsigned char r, unsigned char g, unsigned char b);
ALLEGRO_COLOR wrap_map_rgba(unsigned char r, unsigned char g, unsigned char b, unsigned char a);
void wrap_clear_to_color(ALLEGRO_COLOR color);
void wrap_draw_scaled_rotated_bitmap(ALLEGRO_BITMAP* bitmap, float cx, float cy, float dx, float dy, float xscale, float yscale, float angle, int flags);
//Keyboard routines
bool wrap_install_keyboard(void);
void wrap_get_keyboard_state(ALLEGRO_KEYBOARD_STATE* ret_state);
bool wrap_key_down(const ALLEGRO_KEYBOARD_STATE* state, int keycode);
ALLEGRO_EVENT_SOURCE* wrap_get_keyboard_event_source(void);
//Monitors
//Mouse routines
//System routines
bool wrap_init(void);
//Time routines
double wrap_get_time(void);
void wrap_rest(double seconds);
//Timer routines
ALLEGRO_TIMER* wrap_create_timer(double speed_secs);
void wrap_start_timer(ALLEGRO_TIMER* timer);
void wrap_resume_timer(ALLEGRO_TIMER* timer);
void wrap_stop_timer(ALLEGRO_TIMER* timer);
void wrap_destroy_timer(ALLEGRO_TIMER* timer);
int64_t wrap_get_timer_count(const ALLEGRO_TIMER* timer);

//Font addons
bool wrap_init_font_addon(void);
bool wrap_init_ttf_addon(void);
void wrap_destroy_font(ALLEGRO_FONT* f);
ALLEGRO_FONT* wrap_load_ttf_font(char const* filename, int size, int flags);
void wrap_draw_text(const ALLEGRO_FONT* font, ALLEGRO_COLOR color, float x, float y, int flags, char const* text);
void wrap_draw_multiline_text(const ALLEGRO_FONT* font, ALLEGRO_COLOR color, float x, float y, float max_width, float line_height, int flags, const char* text);
//Color addons
//Image IO addon
bool wrap_init_image_addon(void);
//Primitives addon
bool wrap_init_primitives_addon(void);
void wrap_draw_rectangle(float x1, float y1, float x2, float y2, ALLEGRO_COLOR color, float thickness);
void wrap_draw_filled_rectangle(float x1, float y1, float x2, float y2, ALLEGRO_COLOR color);
// Transformation Addons
void wrap_use_transform(const ALLEGRO_TRANSFORM* trans);
void wrap_identity_transform(ALLEGRO_TRANSFORM* trans);
void wrap_build_transform(ALLEGRO_TRANSFORM* trans, float x, float y, float sx, float sy, float theta);
void wrap_scale_transform(ALLEGRO_TRANSFORM* trans, float sx, float sy);

//Audio Codecs Addon
bool wrap_init_acodec_addon(void);
//Audio Addon
bool wrap_install_audio(void);
void wrap_uninstall_audio(void);
//Audio Samples
bool wrap_reserve_samples(int reserve_samples);
ALLEGRO_SAMPLE* wrap_load_sample(const char* filename);
void wrap_destroy_sample(ALLEGRO_SAMPLE* spl);
bool wrap_play_sample(ALLEGRO_SAMPLE* spl, float gain, float pan, float speed,
					  ALLEGRO_PLAYMODE loop, ALLEGRO_SAMPLE_ID* ret_id);
//Audio Sample Instances
ALLEGRO_SAMPLE_INSTANCE* wrap_create_sample_instance(ALLEGRO_SAMPLE* sample_data);
void wrap_destroy_sample_instance(ALLEGRO_SAMPLE_INSTANCE* spl);
bool wrap_play_sample_instance(ALLEGRO_SAMPLE_INSTANCE* spl);
bool wrap_stop_sample_instance(ALLEGRO_SAMPLE_INSTANCE* spl);
bool wrap_set_sample_instance_playmode(ALLEGRO_SAMPLE_INSTANCE* spl, ALLEGRO_PLAYMODE val);
bool wrap_attach_sample_instance_to_mixer(ALLEGRO_SAMPLE_INSTANCE* spl, ALLEGRO_MIXER* mixer);

#endif
