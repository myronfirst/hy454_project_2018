#ifndef _UTILITIES_H_
#define _UTILITIES_H_

#include "Wrappers.h"

#include <string>

extern ALLEGRO_COLOR COLOR_RED;
extern ALLEGRO_COLOR COLOR_GREEN;
extern ALLEGRO_COLOR COLOR_BLACK;

extern const unsigned SCREEN_WIDTH;
extern const unsigned SCREEN_HEIGHT;
extern const int GRID_SIZE;

unsigned long GetGameTime();

void PrintWarning(std::string msg);
void PrintError(std::string msg);

class Point {
  private:
	unsigned x, y;

  public:
	Point();
	Point(unsigned _x, unsigned _y);
	Point(const Point& p);
	~Point();
	void SetX(unsigned _x);
	unsigned GetX() const;
	void SetY(unsigned _y);
	unsigned GetY() const;
	std::string PointStringify(void) const;
};

class Rect {
  private:
	Point start;
	unsigned width, height;

  public:
	Rect();
	Rect(Point x_y, unsigned _w, unsigned _h);
	~Rect();
	void PrintRect() const;	//Prints the values of the Rect
	std::string RectStringify() const;
	void SetStart(Point p);
	Point GetStart(void) const;
	void SetW(unsigned w);
	unsigned GetW(void) const;
	void SetH(unsigned h);
	unsigned GetH(void) const;
};

#endif
