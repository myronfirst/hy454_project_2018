#ifndef _ANIMATION_FILM_HOLDER_H
#define _ANIMATION_FILM_HOLDER_H

#include "AnimationFilm.h"
#include "BitmapLoader.h"

#include <map>
#include <string>

class AnimationFilmHolder {
  private:
	using Films = std::map<std::string, AnimationFilm*>;
	static Films films;
	static BitmapLoader bitmaps;

  public:
	static void Initialize(void);
	static void CleanUp(void);
	static void Load(const std::string& catalogue);
	static const AnimationFilm* GetFilm(const std::string& id);
};

#endif
