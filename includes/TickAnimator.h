#ifndef _TICK_ANIMATOR_H
#define _TICK_ANIMATOR_H

#include "Animator.h"
#include "TickAnimation.h"

class TickAnimator : public Animator {
  private:
	unsigned completedReps;
	TickAnimation* anim;

  public:
	TickAnimator();
	virtual ~TickAnimator();
	void Start(TickAnimation* a, timestamp_t t);
	void Progress(timestamp_t currTime) override final;
};

#endif
