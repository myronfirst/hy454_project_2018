#ifndef _SOUND_MIXER_H_
#define _SOUND_MIXER_H_

#include <map>
#include "Wrappers.h"

using AudioSample = ALLEGRO_SAMPLE*;
using AudioInstance = ALLEGRO_SAMPLE_INSTANCE*;

class SoundMixer {
  private:
	using AudioSampleHolder = std::map<std::string, AudioSample>;
	using AudioInstanceHolder = std::map<std::string, AudioInstance>;

	static AudioSampleHolder audioSamples;
	static AudioInstanceHolder audioInstances;

  public:
	static void Initialize(void);
	static void CleanUp(void);
	static void LoadAudioSamples(const std::string& path);
	static void CreateAudioSampleInstance(const std::string id);
	static void AttachAudioInstanceToMixer(const std::string id);
	static void PlayAudioSample(const std::string id);
	static void PlayAudioInstance(const std::string id);
	static AudioSample GetAudioSample(const std::string id);
	static AudioInstance GetAudioInstance(const std::string id);
};

#endif
