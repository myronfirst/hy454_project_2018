#ifndef _FRAME_LIST_ANIMATOR_H
#define _FRAME_LIST_ANIMATOR_H

#include "Animator.h"
#include "FrameListAnimation.h"
#include "Sprite.h"

class FrameListAnimator : public Animator {
  private:
	Sprite* sprite;
	FrameListAnimation* anim;
	unsigned currEl;

  public:
	FrameListAnimator();
	virtual ~FrameListAnimator();
	void Start(Sprite* s, FrameListAnimation* a, timestamp_t t);
	void Progress(timestamp_t currTime) override final;
};

#endif
