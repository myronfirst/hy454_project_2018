#include "Animation.h"

using namespace std;
Animation::Animation(animd_t _id)
	: id(_id) {}
Animation::~Animation() {}
// Animation* Animation::Clone(animd_t newId) const //Pure Virtual
animd_t Animation::GetId(void) const {
	return id;
}
