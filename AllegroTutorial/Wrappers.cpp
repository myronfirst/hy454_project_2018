#include "Wrappers.h"

#include "Utilities.h"

#include <sstream>

#include <allegro5/allegro_image.h>

using namespace std;
//Categories from Allegro 5 Reference Manual

//Configuration files
ALLEGRO_CONFIG *wrap_load_config_file(const char *filename) {
	ALLEGRO_CONFIG *ret = al_load_config_file(filename);
	if (ret == NULL) {
		ostringstream str;
		str << "al_load_config_file(" << filename << ") failed";
		PrintError(str.str());
	}
	return ret;
}

//Displays
ALLEGRO_DISPLAY *wrap_create_display(int w, int h) {
	ALLEGRO_DISPLAY *ret = al_create_display(w, h);
	if (ret == NULL) {
		ostringstream str;
		str << "al_create_display(" << w << ", " << h << ") failed";
		PrintError(str.str());
	}
	return ret;
}

//Events
ALLEGRO_EVENT_QUEUE *wrap_create_event_queue(void) {
	ALLEGRO_EVENT_QUEUE *ret = al_create_event_queue();
	if (ret == NULL) {
		ostringstream str;
		str << "al_create_event_queue() failed";
		PrintError(str.str());
	}
	return ret;
}
//Fullscreen modes

//Graphics routines
ALLEGRO_BITMAP *wrap_load_bitmap(const char *filename) {
	ALLEGRO_BITMAP *ret = al_load_bitmap(filename);
	if (ret == NULL) {
		ostringstream str;
		str << "al_load_bitmap(" << filename << ") failed";
		PrintError(str.str());
	}
	return ret;
}

//Keyboard routines

//Monitors

//Mouse routines

//System routines
bool wrap_init(void) {
	bool ret = al_init();
	if (ret == false) PrintError("al_init() failed");
	return ret;
}
//Timer

//Font addons

//Image IO addon

//Primitives addon
