#include "LateDestruction.h"

#include <algorithm>
#include <cassert>

using namespace std;

std::list<LatelyDestroyable*> DestructionManager::dead;
void DestructionManager::Initalise(void) {
	assert(dead.size() == 0);
}
void DestructionManager::CleanUp(void) {
	assert(dead.size() == 0);
}
void DestructionManager::Register(LatelyDestroyable* x) {
	assert(!x->IsAlive());
	dead.push_back(x);
}
void DestructionManager::Commit(void) {
	for_each(dead.begin(), dead.end(), LatelyDestroyable::Delete);
	dead.clear();
}

/* std::function<void(LatelyDestroyable* x)> LatelyDestroyable::Delete = [](LatelyDestroyable* x) {
	x->inDestruction = true;
	delete x;
	x = nullptr;
} */
void LatelyDestroyable::Delete(LatelyDestroyable* x) {
	x->inDestruction = true;
	delete x;
	x = nullptr;
}
LatelyDestroyable::LatelyDestroyable(void)
	: alive(true), inDestruction(false) {
}
LatelyDestroyable::~LatelyDestroyable() {
	assert(inDestruction);
}
bool LatelyDestroyable::IsAlive(void) const {
	return alive;
}
void LatelyDestroyable::Destroy(void) {
	if (alive) {
		alive = false;
		DestructionManager::Register(this);
	}
}
