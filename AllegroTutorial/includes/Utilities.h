#ifndef _UTILITIES_H_
#define _UTILITIES_H_

#include <string>

void PrintWarning(std::string msg);
void PrintError(std::string msg);

#endif
