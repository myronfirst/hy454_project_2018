#ifndef _WRAPPERS_H_
#define _WRAPPERS_H_

#include <allegro5/allegro.h>

//Categories from Allegro 5 Reference Manual

//Configuration files
ALLEGRO_CONFIG* wrap_load_config_file(const char* filename);
//Displays
ALLEGRO_DISPLAY* wrap_create_display(int w, int h);
//Events
ALLEGRO_EVENT_QUEUE* wrap_create_event_queue(void);
//Fullscreen modes
//Graphics routines
ALLEGRO_BITMAP* wrap_load_bitmap(const char* filename);
//Keyboard routines
//Monitors
//Mouse routines
//System routines
bool wrap_init(void);
//Timer
//Font addons
//Image IO addon
//Primitives addon

#endif
