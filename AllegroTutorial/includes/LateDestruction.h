#ifndef _LATE_DESTRUCTION_H
#define _LATE_DESTRUCTION_H

#include <functional>
#include <list>

class LatelyDestroyable;
class DestructionManager {
  private:
	static std::list<LatelyDestroyable*> dead;

  public:
	static void Initalise(void);
	static void CleanUp(void);
	static void Register(LatelyDestroyable* x);
	static void Commit(void);
};
class LatelyDestroyable {
  private:
	friend class DestructionManager;	//DestructionManager can use Delete()
	bool alive;
	bool inDestruction;
	// static std::function<void(LatelyDestroyable* x)> Delete;
	static void Delete(LatelyDestroyable* x);

  public:
	LatelyDestroyable(void);		 //virtual?
	virtual ~LatelyDestroyable();	//why public?
	bool IsAlive(void) const;
	void Destroy(void);
};
#endif
