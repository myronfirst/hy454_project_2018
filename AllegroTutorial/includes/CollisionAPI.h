#ifndef _COLLISION_API_H
#define _COLLISION_API_H

#include <list>

struct Point {
  public:
	unsigned x, y;
	Point(void);
	Point(unsigned _x, unsigned _y);
	Point(const Point& p);
	~Point();
};
typedef std::list<Point> Polygon;

class BoundingBox;
// class BoundingCircle;
// class BoundingPolygon;

class BoundingArea {
  public:
	virtual bool Intersects(const BoundingBox& box) const = 0;
	// virtual bool intersects(const BoundingCircle& circle) const = 0;
	// virtual bool intersects(const BoundingPolygon& poly) const = 0;

  public:
	BoundingArea();
	virtual ~BoundingArea();
	virtual bool In(unsigned x, unsigned y) const = 0;
	virtual bool Intersects(const BoundingArea& area) const = 0;
	virtual BoundingArea* Clone(void) const = 0;
};

class BoundingBox : public BoundingArea {
	//   protected:
  public:
	Point p1, p2;

  public:
	BoundingBox();
	BoundingBox(unsigned x1, unsigned y1, unsigned x2, unsigned y2);
	BoundingBox(Point _p1, Point _p2);
	~BoundingBox();
	virtual bool Intersects(const BoundingBox& box) const override;
	// virtual bool intersects(const BoundingCircle& circle) const;
	// virtual bool intersects(const BoundingPolygon& poly) const;
	virtual bool In(unsigned x, unsigned y) const override;
	virtual bool Intersects(const BoundingArea& area) const override;
	virtual BoundingBox* Clone(void) const override;
};

/* class BoundingCircle : public BoundingArea {
  protected:
	unsigned x, y, r;

  public:
	BoundingCircle();
	BoundingCircle(unsigned _x1, unsigned _y1, unsigned _r);
	~BoundingCircle();
	virtual bool intersects(const BoundingBox& box) const;
	virtual bool intersects(const BoundingCircle& circle) const;
	virtual bool intersects(const BoundingPolygon& poly) const;
	virtual bool in(unsigned _x, unsigned _y) const;
	virtual bool intersects(const BoundingArea& area) const;
	virtual BoundingCircle* clone(void) const;
}; */

/* class BoundingPolygon : public BoundingArea {
  protected:
	Polygon points;

  public:
	BoundingPolygon();
	~BoundingPolygon();
	virtual bool intersects(const BoundingBox& box) const;
	virtual bool intersects(const BoundingCircle& circle) const;
	virtual bool intersects(const BoundingPolygon& poly) const;
	virtual bool in(unsigned x, unsigned y) const;
	virtual bool intersects(const BoundingArea& area) const;
	virtual BoundingPolygon* clone(void) const;
}; */

#endif
