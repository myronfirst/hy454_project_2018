#include "CollisionAPI.h"

Point::Point(void)
	: x(0), y(0) {}
Point::Point(unsigned _x, unsigned _y)
	: x(_x), y(_y) {}
Point::Point(const Point& p)
	: x(p.x), y(p.y) {}
Point::~Point() {}

BoundingArea::BoundingArea() {}
BoundingArea::~BoundingArea() {}

BoundingBox::BoundingBox() {}
BoundingBox::BoundingBox(unsigned x1, unsigned y1, unsigned x2, unsigned y2)
	: p1(x1, y1),
	  p2(x2, y2) {}
BoundingBox::BoundingBox(Point _p1, Point _p2)
	: p1(_p1), p2(_p2) {}
BoundingBox::~BoundingBox() {}
bool BoundingBox::Intersects(const BoundingBox& box) const {
	return !(box.p2.x < p1.x || p2.x < box.p1.x || box.p2.y < p1.y || p2.y < box.p1.y);
}

bool BoundingBox::In(unsigned x, unsigned y) const {
	return p1.x <= x && x <= p2.x && p1.y <= y && y <= p2.y;
}
bool BoundingBox::Intersects(const BoundingArea& area) const {
	return area.Intersects(*this);
}
BoundingBox* BoundingBox::Clone(void) const {
	return new BoundingBox(p1, p2);
}
