#include "Utilities.h"

#include <iostream>

using namespace std;

void PrintWarning(std::string msg) {
	cout << msg << endl;
}
void PrintError(std::string msg) {
	PrintWarning(msg);
	exit(-1);
}
