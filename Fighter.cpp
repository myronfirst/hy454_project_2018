#include "Fighter.h"
#include <iostream>
#include <map>
#include <sstream>
#include "AnimationDataHolder.h"
#include "AnimationFilmHolder.h"
#include "SoundMixer.h"

const int FIGHTER_ACTION_DELAY_MSECS = 150;

using namespace std;

// Sound Effects
std::map<std::string, std::string> SoundEffectsMap = {
	{"right_high_punch", "right.punch"},
	{"left_high_punch", "left.punch"},
	{"high_kick", "kick"},
	{"uppercut", "upper.kick"},
	{"upper_right_high_punch", "right.punch"},
	{"upper_left_high_punch", "left.punch"},
	{"upper_high_kick", "upper.kick"},
	{"jump_upper_high_kick", "upper.kick"},
	{"jump_forward_high_kick", "kick"},
	{"flip_low_kick", "kick"},
	{"flip_upper_kick", "upper.kick"},
	{"down_high_kick", "kick"},
	{"down_forward_high_kick", "kick"},
};

void CheckFighterFacingDirection(Fighter* p1, Fighter* p2) {
	if (p1->GetSprite()->GetX() < p2->GetSprite()->GetX()) {
		p1->SetFacingDirection(FACE_RIGHT);
		p2->SetFacingDirection(FACE_LEFT);
	} else {
		p1->SetFacingDirection(FACE_LEFT);
		p2->SetFacingDirection(FACE_RIGHT);
	}
}

Fighter::Fighter(Sprite* _sprite, std::string _nextAction, std::string _name, int facing_direction, InputController::Actions* keybinds)
	: sprite(_sprite), keybindings(keybinds), fighterState(NORMAL), health(100), name(_name), nextAction(_nextAction), faceDir(facing_direction) {
	// Initialize Animators
	movingAnimator = new MovingAnimator();
	movingAnimator->SetOnFinish([&](Animator* animator) {
		std::cout << "Moving Animator onFinish" << std::endl;
	});

	movingPathAnimator = new MovingPathAnimator();
	movingPathAnimator->SetOnFinish([&](Animator* animator) {
		std::cout << "Moving Path Animator onFinish" << std::endl;
	});

	frameListAnimator = new FrameListAnimator();
	frameListAnimator->SetOnFinish([&](Animator* animator) {
		std::cout << "Frame List Animator onFinish" << std::endl;
	});

	frameRangeAnimator = new FrameRangeAnimator();
	frameRangeAnimator->SetOnFinish([&](Animator* animator) {
		std::cout << "Frame Range Animator onFinish" << std::endl;
	});

	// Get Keybindings
	LoadKeybindings(keybindings);

	// Initialize State Transitions
	stateTransitions = new StateTransitions();
	stateTransitions->SetState("ready");
	InitializeStates();

	// Set up TickAnimation
	tickAnim = new TickAnimation(name + "InputController", 150000u, 0, [&]() {
		// cout << "TickAnimation Clearing logical, exec PerformTransition" << endl;
		Input::Logical maxLogical = Input::FilterLogical(inputController.GetLogical());
		inputController.ClearLogical();

		stateTransitions->PerformTransitions(maxLogical, false);
	});

	tickAnimator = new TickAnimator();
	tickAnimator->SetOnFinish([](Animator* animator) {
		cout << "tickAnimator onFinish" << endl;
	});

	projectile = NULL;
	FinishHim = false;
}

bool Fighter::IsAnimatorRunning(void) {
	return ((!movingAnimator->IsSuspended()) || (!movingPathAnimator->IsSuspended()) || (!frameListAnimator->IsSuspended()) || (!frameRangeAnimator->IsSuspended()));
}

void Fighter::SetTransitions(std::string state, const StateTransitions::Input& input, std::string film, AnimatorType type, std::string newState, FighterState _fighterState) {
	std::function<void(void)> dispatchedAction;
	switch (type) {
		case MOVING_ANIMATOR:
			dispatchedAction = [&, film, newState, _fighterState](void) {
				if (!IsAnimatorRunning() || stateTransitions->GetState() == "ready") {			//We need to start a new animation only if there isn't already one running
					if (stateTransitions->GetState() == "ready") frameRangeAnimator->Stop();	//stop Idle animation

					stateTransitions->SetState(newState);					//Change to new state
					sprite->SetFilm(AnimationFilmHolder::GetFilm(film));	//change the film
					movingAnimator->Start(sprite, AnimationDataHolder::GetMovingAnimation(film), GetGameTime());
					fighterState = _fighterState;
				}
			};
			break;
		case MOVING_PATH_ANIMATOR:
			dispatchedAction = [&, film, newState, _fighterState](void) {
				if (!IsAnimatorRunning() || sprite->GetFilm()->GetId() == name + ".idle") {		//We need to start a new animation only if there isn't already one running
					if (stateTransitions->GetState() == "ready") frameRangeAnimator->Stop();	//stop Idle animation

					stateTransitions->SetState(newState);					//Change to new state
					sprite->SetFilm(AnimationFilmHolder::GetFilm(film));	//change the film
					movingPathAnimator->Start(sprite, AnimationDataHolder::GetMovingPathAnimation(film), GetGameTime());
					fighterState = _fighterState;
				}
			};
			break;
		case FRAME_LIST_ANIMATOR:
			dispatchedAction = [&, film, newState, _fighterState](void) {
				if (!IsAnimatorRunning() || stateTransitions->GetState() == "ready") {			//We need to start a new animation only if there isn't already one running
					if (stateTransitions->GetState() == "ready") frameRangeAnimator->Stop();	//stop Idle animation
					stateTransitions->SetState(newState);										//Change to new state
					sprite->SetFilm(AnimationFilmHolder::GetFilm(film));						//change the film
					frameListAnimator->Start(sprite, AnimationDataHolder::GetFrameListAnimation(film), GetGameTime());
					fighterState = _fighterState;
					if (fighterState == ATTACK) {
						auto sound = SoundEffectsMap.find(stateTransitions->GetState());
						assert(sound != SoundEffectsMap.end());
						SoundMixer::PlayAudioInstance(sound->second);
					}
				}
			};
			break;
		case FRAME_RANGE_ANIMATOR:
			dispatchedAction = [&, film, newState, _fighterState](void) {
				if (!IsAnimatorRunning() || stateTransitions->GetState() == "ready") {			//We need to start a new animation only if there isn't already one running
					if (stateTransitions->GetState() == "ready") frameRangeAnimator->Stop();	//stop Idle animation

					stateTransitions->SetState(newState);					//Change to new state
					sprite->SetFilm(AnimationFilmHolder::GetFilm(film));	//change the film
					frameRangeAnimator->Start(sprite, AnimationDataHolder::GetFrameRangeAnimation(film), GetGameTime());
					fighterState = _fighterState;
					if (fighterState == ATTACK) {
						auto sound = SoundEffectsMap.find(stateTransitions->GetState());
						assert(sound != SoundEffectsMap.end());
						SoundMixer::PlayAudioInstance(sound->second);
					}
				}
			};
			break;
		case FREEZEBALL_ANIMATOR:
			dispatchedAction = [&, film, newState, _fighterState](void) {
				if (!IsAnimatorRunning() || stateTransitions->GetState() == "ready") {			//We need to start a new animation only if there isn't already one running
					if (stateTransitions->GetState() == "ready") frameRangeAnimator->Stop();	//stop Idle animation

					stateTransitions->SetState(newState);					//Change to new state
					sprite->SetFilm(AnimationFilmHolder::GetFilm(film));	//change the film
					frameListAnimator->Start(sprite, AnimationDataHolder::GetFrameListAnimation(film), GetGameTime());
					fighterState = _fighterState;

					// Freezeball Projectile
					projectile = new Projectile(new Sprite(sprite->GetX(), sprite->GetY() - 100, AnimationFilmHolder::GetFilm("subzero.freeze.ball")), "FREEZEBALL", "subzero.freeze.ball");
				}
			};
			break;
		case LIGHTINGBOLT_ANIMATOR:
			dispatchedAction = [&, film, newState, _fighterState](void) {
				if (!IsAnimatorRunning() || stateTransitions->GetState() == "ready") {			//We need to start a new animation only if there isn't already one running
					if (stateTransitions->GetState() == "ready") frameRangeAnimator->Stop();	//stop Idle animation

					stateTransitions->SetState(newState);					//Change to new state
					sprite->SetFilm(AnimationFilmHolder::GetFilm(film));	//change the film
					frameListAnimator->Start(sprite, AnimationDataHolder::GetFrameListAnimation(film), GetGameTime());
					fighterState = _fighterState;

					// Freezeball Projectile
					projectile = new Projectile(new Sprite(sprite->GetX(), sprite->GetY() - 100, AnimationFilmHolder::GetFilm("raiden.lighting.bolt")), "LIGHTINGBOLT", "raiden.lighting.bolt");
				}
			};
			break;
		default:	//invalid type
			ostringstream str;
			str << "In fighter class: " << name << " invalid type " << static_cast<int>(type) << std::endl;
			PrintWarning(str.str());
			assert(false);
			break;
	}

	stateTransitions->SetTransition(state, input, dispatchedAction);
}

void Fighter::ProgressAnimators(timestamp_t time) {
	if (tickAnimator->IsSuspended()) {
		tickAnimator->Start(tickAnim, time);
	} else {
		tickAnimator->Progress(time);
	}

	if (!movingAnimator->IsSuspended()) movingAnimator->Progress(time);
	if (!movingPathAnimator->IsSuspended()) movingPathAnimator->Progress(time);
	if (!frameListAnimator->IsSuspended()) frameListAnimator->Progress(time);
	if (!frameRangeAnimator->IsSuspended()) frameRangeAnimator->Progress(time);
	if (projectile != NULL) {
		if (projectile->IsOutOfBounds()) {
			projectile->toBeDeleted = true;
		}

		if (projectile->toBeDeleted) {
			// projectile->~Projectile();
			delete (projectile);
			projectile = NULL;
		} else {
			projectile->GetAnimator()->Progress(time);
		}
	}
}
timestamp_t Fighter::StopAnimators(void) {
	timestamp_t stopTime = GetGameTime();
	if (!movingAnimator->IsSuspended()) movingAnimator->Stop();
	if (!movingPathAnimator->IsSuspended()) movingPathAnimator->Stop();
	if (!frameListAnimator->IsSuspended()) frameListAnimator->Stop();
	if (!frameRangeAnimator->IsSuspended()) frameRangeAnimator->Stop();
	return stopTime;
}
void Fighter::TimeShiftAnimators(timestamp_t offset) {
	movingAnimator->TimeShift(offset);
	movingPathAnimator->TimeShift(offset);
	frameListAnimator->TimeShift(offset);
	frameRangeAnimator->TimeShift(offset);
}
InputController* Fighter::GetInputController() {
	return &inputController;
}
Sprite* Fighter::GetSprite() {
	return sprite;
}
StateTransitions* Fighter::GetStateTransitions() {
	return stateTransitions;
}
FrameRangeAnimator* Fighter::GetFrameRangeAnimator() {
	return frameRangeAnimator;
}
FrameListAnimator* Fighter::GetFrameListAnimator() {
	return frameListAnimator;
}
MovingPathAnimator* Fighter::GetMovingPathAnimator() {
	return movingPathAnimator;
}
MovingAnimator* Fighter::GetMovingAnimator() {
	return movingAnimator;
}

const std::string& Fighter::GetName(void) const {
	return name;
}
FighterState Fighter::GetState(void) const {
	return fighterState;
}
void Fighter::SetState(FighterState _fighterState) {
	if (fighterState != _fighterState)
		fighterState = _fighterState;
}
int Fighter::GetHealth(void) const {
	return health;
}
void Fighter::SetHealth(int _health) {
	health = _health;
}
void Fighter::ReduceHealth(int damage) {
	health -= damage;
}
void Fighter::DisplayHealth(ALLEGRO_FONT* font, const Point& pos) {
	ALLEGRO_COLOR color = wrap_map_rgb(0, 0, 0);
	ostringstream oss;
	oss << name << " health: " << health;
	const string healthText = oss.str();

	wrap_draw_text(font, color, pos.GetX(), pos.GetY(), ALLEGRO_ALIGN_LEFT, healthText.c_str());
}

void Fighter::DisplayFrameBox(void) {
	ALLEGRO_COLOR color;
	float alpha = 0.7f;
	switch (fighterState) {
		case NORMAL:
			color = wrap_map_rgba(0, 255, 0, alpha);
			break;
		case ATTACK:
			color = wrap_map_rgba(255, 0, 0, alpha);
			break;
		case ATTACKED:
			color = wrap_map_rgba(255, 255, 0, alpha);
			break;
		case BLOCK:
			color = wrap_map_rgba(0, 0, 255, alpha);
			break;

		default:
			assert(0);
			break;
	}
	sprite->DisplayFrameBox(color);
}
void Fighter::ForceFrameRangeAnimation(animd_t id) {
	StopAnimators();
	sprite->SetFilm(AnimationFilmHolder::GetFilm(id));	//change the film
	frameRangeAnimator->Start(sprite, AnimationDataHolder::GetFrameRangeAnimation(id), GetGameTime());
}

void Fighter::ForceFrameListAnimation(animd_t id) {
	StopAnimators();
	sprite->SetFilm(AnimationFilmHolder::GetFilm(id));	//change the film
	frameListAnimator->Start(sprite, AnimationDataHolder::GetFrameListAnimation(id), GetGameTime());
}

void Fighter::LoadKeybindings(InputController::Actions* actions) {
	for (auto it = actions->begin(); it != actions->end(); ++it) {
		inputController.AddAction(it->first, it->second);
	}
}

void Fighter::InitializeStates() {
	stateTransitions->SetState("ready");

	// Ready State
	stateTransitions->SetTransition("ready", {}, [&](void) {	if(!this->IsAnimatorRunning()){ // transition for ready.*
			this->sprite->SetFilm(AnimationFilmHolder::GetFilm(this->name+".idle"));
			frameRangeAnimator->Start(this->sprite, AnimationDataHolder::GetFrameRangeAnimation(this->name + ".idle"), GetGameTime());
	} });
	// Common moves

	// Movement
	this->SetTransitions("ready", {"right"}, name + ".walking.forward", FRAME_RANGE_ANIMATOR, "forward", NORMAL);
	this->SetTransitions("ready", {"left"}, name + ".walking.backwards", FRAME_RANGE_ANIMATOR, "backward", NORMAL);
	this->SetTransitions("ready", {"jump"}, name + ".jump", MOVING_PATH_ANIMATOR, "jump", NORMAL);
	this->SetTransitions("ready", {"duck"}, name + ".prune", FRAME_RANGE_ANIMATOR, "duck", NORMAL);

	// Simple Attacks
	this->SetTransitions("ready", {"high_kick"}, name + ".high.kick", FRAME_LIST_ANIMATOR, "high_kick", ATTACK);
	this->SetTransitions("ready", {"high_punch"}, name + ".high.right.punch", FRAME_LIST_ANIMATOR, "right_high_punch", ATTACK);
	this->SetTransitions("ready", {"block"}, name + ".upper.block", FRAME_RANGE_ANIMATOR, "block", BLOCK);

	// Common Combos
	this->SetTransitions("ready", {"jump.upper.high_kick"}, name + ".jump.upper.kick", MOVING_PATH_ANIMATOR, "jump_upper_high_kick", ATTACK);
	this->SetTransitions("ready", {"jump.forward.high_kick"}, name + ".jump.forward.kick", MOVING_PATH_ANIMATOR, "jump_forward_high_kick", ATTACK);
	this->SetTransitions("ready", {"down.high_punch"}, name + ".upper.cut", FRAME_LIST_ANIMATOR, "uppercut", ATTACK);
	this->SetTransitions("ready", {"down.right"}, name + ".roll.bottom.forward", FRAME_LIST_ANIMATOR, "roll_forward", NORMAL);
	this->SetTransitions("ready", {"upper.high_kick"}, name + ".upper.kick", FRAME_LIST_ANIMATOR, "upper_high_kick", ATTACK);
	this->SetTransitions("ready", {"upper.high_punch"}, name + ".upper.right.punch", FRAME_LIST_ANIMATOR, "upper_right_high_punch", ATTACK);
	this->SetTransitions("ready", {"down.high_kick.right"}, name + ".flip.low.kick", FRAME_RANGE_ANIMATOR, "flip_low_kick", ATTACK);
	this->SetTransitions("ready", {"down.high_kick.right.upper"}, name + ".flip.upper.kick", FRAME_RANGE_ANIMATOR, "flip_upper_kick", ATTACK);

	// Forward State
	this->SetTransitions("forward", {}, name + ".idle", FRAME_RANGE_ANIMATOR, "ready", NORMAL);
	this->SetTransitions("forward", {"right"}, name + ".walking.forward", FRAME_RANGE_ANIMATOR, "forward", NORMAL);

	// Backward State
	this->SetTransitions("backward", {}, name + ".idle", FRAME_RANGE_ANIMATOR, "ready", NORMAL);
	this->SetTransitions("backward", {"left"}, name + ".walking.backwards", FRAME_RANGE_ANIMATOR, "backward", NORMAL);

	// Jump State
	this->SetTransitions("jump", {}, name + ".idle", FRAME_RANGE_ANIMATOR, "ready", NORMAL);
	this->SetTransitions("jump", {"upper_high_kick.jump"}, name + ".jump.upper.kick", MOVING_PATH_ANIMATOR, "jump_upper_high_kick", ATTACK);
	this->SetTransitions("jump", {"forward_high_kick.jump"}, name + ".jump.forward.kick", MOVING_PATH_ANIMATOR, "jump_forward_high_kick", ATTACK);

	// Jump Upper High Kick
	this->SetTransitions("jump_upper_high_kick", {}, name + ".idle", FRAME_RANGE_ANIMATOR, "ready", NORMAL);
	this->SetTransitions("jump_forward_high_kick", {}, name + ".idle", FRAME_RANGE_ANIMATOR, "ready", NORMAL);

	// Duck state
	this->SetTransitions("duck", {}, name + ".rewind.prune", FRAME_LIST_ANIMATOR, "ready", NORMAL);
	this->stateTransitions->SetTransition("duck", {"duck"}, [&](void) {});
	this->SetTransitions("duck", {"block.duck"}, name + ".down.block", FRAME_RANGE_ANIMATOR, "block_duck", BLOCK);
	this->SetTransitions("duck", {"duck.left"}, name + ".down.back", FRAME_RANGE_ANIMATOR, "down_back", NORMAL);
	this->SetTransitions("duck", {"down.high_kick"}, name + ".low.high.kick", FRAME_LIST_ANIMATOR, "down_high_kick", ATTACK);
	this->SetTransitions("duck", {"down.forward.high_kick"}, name + ".low.forward.kick", FRAME_LIST_ANIMATOR, "down_forward_high_kick", ATTACK);
	this->SetTransitions("duck", {"high_punch"}, name + ".upper.cut", FRAME_LIST_ANIMATOR, "uppercut", ATTACK);
	this->SetTransitions("duck", {"down.high_kick.right"}, name + ".flip.low.kick", FRAME_RANGE_ANIMATOR, "flip_low_kick", ATTACK);
	this->SetTransitions("duck", {"down.high_kick.right.upper"}, name + ".flip.upper.kick", FRAME_RANGE_ANIMATOR, "flip_upper_kick", ATTACK);

	// Block Duck State
	this->SetTransitions("block_duck", {}, name + ".rewind.prune", FRAME_LIST_ANIMATOR, "ready", NORMAL);
	this->stateTransitions->SetTransition("block_duck", {"block.duck"}, [&](void) {});

	// Down Back State
	this->SetTransitions("down_back", {}, name + ".rewind.prune", FRAME_LIST_ANIMATOR, "ready", NORMAL);
	this->stateTransitions->SetTransition("down_back", {"duck.left"}, [&](void) {});

	// Down High Kick State
	this->SetTransitions("down_high_kick", {}, name + ".rewind.prune", FRAME_LIST_ANIMATOR, "ready", NORMAL);

	// Down Forward High Kick State
	this->SetTransitions("down_forward_high_kick", {}, name + ".rewind.prune", FRAME_LIST_ANIMATOR, "ready", NORMAL);

	// High Kick State
	this->SetTransitions("high_kick", {}, name + ".start.position", MOVING_ANIMATOR, "ready", NORMAL);

	// Block State
	this->SetTransitions("block", {}, name + ".rewind.upper.block", FRAME_LIST_ANIMATOR, "ready", NORMAL);	//BLOCK for collisions
	this->stateTransitions->SetTransition("block", {"block"}, [&](void) {});

	// Right Hight Punch State
	this->SetTransitions("right_high_punch", {}, name + ".start.position", MOVING_ANIMATOR, "ready", NORMAL);
	this->SetTransitions("right_high_punch", {"high_punch"}, name + ".high.left.punch", FRAME_LIST_ANIMATOR, "left_high_punch", ATTACK);

	// Left Hight Punch State
	this->SetTransitions("left_high_punch", {}, name + ".start.position", MOVING_ANIMATOR, "ready", NORMAL);
	this->SetTransitions("left_high_punch", {"high_punch"}, name + ".high.right.punch", FRAME_LIST_ANIMATOR, "right_high_punch", ATTACK);

	// Uppercut State
	this->SetTransitions("uppercut", {""}, name + ".idle", FRAME_RANGE_ANIMATOR, "ready", NORMAL);

	// Roll Forward State
	this->SetTransitions("roll_forward", {}, name + ".start.position", MOVING_ANIMATOR, "ready", NORMAL);

	// Upper High Kick State
	this->SetTransitions("upper_high_kick", {}, name + ".idle", FRAME_RANGE_ANIMATOR, "ready", NORMAL);

	// Jump Upper Kick State
	this->SetTransitions("jump_high_kick", {}, name + ".idle", FRAME_RANGE_ANIMATOR, "ready", NORMAL);

	// Upper Right High Punch State
	this->SetTransitions("upper_right_high_punch", {}, name + ".start.position", MOVING_ANIMATOR, "ready", NORMAL);
	this->SetTransitions("upper_right_high_punch", {"upper.high_punch"}, name + ".upper.left.punch", FRAME_LIST_ANIMATOR, "upper_left_high_punch", ATTACK);

	// Upper Left High Punch State
	this->SetTransitions("upper_left_high_punch", {}, name + ".start.position", MOVING_ANIMATOR, "ready", NORMAL);
	this->SetTransitions("upper_left_high_punch", {"upper.high_punch"}, name + ".upper.right.punch", FRAME_LIST_ANIMATOR, "upper_right_high_punch", ATTACK);

	// Flip Low Kick State
	this->SetTransitions("flip_low_kick", {}, name + ".idle", FRAME_RANGE_ANIMATOR, "ready", NORMAL);

	// Flip Upper Kick State
	this->SetTransitions("flip_upper_kick", {}, name + ".start.position", MOVING_ANIMATOR, "ready", NORMAL);

	// Jump Forward Kick State
	this->SetTransitions("jump_forward_kick", {}, name + ".idle", FRAME_RANGE_ANIMATOR, "ready", NORMAL);

	// Hit By kick State
	this->SetTransitions("hit_by_high_kick", {}, name + ".rewind.successful.kick.by.enemy", FRAME_LIST_ANIMATOR, "ready", NORMAL);

	// Hit By Punch State
	this->SetTransitions("hit_by_high_punch", {}, name + ".rewind.enemy.hit.left.punch", FRAME_LIST_ANIMATOR, "ready", NORMAL);

	// Hit By Upper Punch State
	this->SetTransitions("hit_by_upper_punch", {}, name + ".rewind.enemy.hit.upper.punch", FRAME_LIST_ANIMATOR, "ready", NORMAL);

	// Frozen State
	this->SetTransitions("frozen", {}, name + ".freeze.form", FRAME_LIST_ANIMATOR, "ready", NORMAL);

	//Fatality Start
	this->SetTransitions("fatality_start", {}, name + ".fatality.start", FRAME_RANGE_ANIMATOR, "fatality_start", NORMAL);

	// After Fatality
	this->SetTransitions("Fatality", {}, name + ".winning.pose", FRAME_RANGE_ANIMATOR, "winning_pose", NORMAL);

	// After being Killed by Fatality (do nothing)
	// stateTransitions->SetTransition("Being_Executed", {}, [&](void) {
	// 	// this->StopAnimators();
	// });
}

void Fighter::CleanUp() {
	delete (sprite);
	delete (movingAnimator);
	delete (movingPathAnimator);
	delete (frameListAnimator);
	delete (frameRangeAnimator);
	delete (tickAnimator);
	delete (tickAnim);
	delete (stateTransitions);
	delete (projectile);
}

Fighter::~Fighter() {
	CleanUp();
}

void Fighter::SetFacingDirection(int _faceDir) {
	faceDir = _faceDir;
}
int Fighter::GetFacingDirection() {
	return faceDir;
}

Projectile* Fighter::GetProjectile() {
	return projectile;
}
void Fighter::SetFinisher(bool b) {
	FinishHim = b;
}
bool Fighter::GetFinisher() {
	return FinishHim;
}
