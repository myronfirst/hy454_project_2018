#include "PauseGame.h"

#include <string>
#include "BitmapLoader.h"
#include "SoundMixer.h"
#include "Utilities.h"
#include "Wrappers.h"

using namespace std;
bool PauseGame(EventQueue* queue, ALLEGRO_FONT* font, Fighter* player, Fighter* enemy) {
	unsigned long pauseTime = 0;
	unsigned long resumeTime = 0;
	pauseTime = GetGameTime();
	ALLEGRO_COLOR trans = wrap_map_rgba(0, 0, 0, 100);
	ALLEGRO_COLOR color = wrap_map_rgb(0, 0, 0);
	Point textPos(360, 300);
	wrap_draw_filled_rectangle(0, 0, static_cast<int>(SCREEN_WIDTH), static_cast<int>(SCREEN_HEIGHT), trans);
	wrap_draw_text(font, color, textPos.GetX(), textPos.GetY(), ALLEGRO_ALIGN_LEFT, "PAUSED");
	wrap_flip_display();

	bool doResume = false;
	bool doExit = false;
	while (!doResume) {
		ALLEGRO_EVENT event = queue->WaitNextEvent();
		if (event.type == ALLEGRO_EVENT_KEY_DOWN) {
			if (event.keyboard.keycode == ALLEGRO_KEY_ESCAPE) {
				doExit = true;
				doResume = true;
			} else if (event.keyboard.keycode == ALLEGRO_KEY_P) {
				doResume = true;
			}
		}
	}
	resumeTime = GetGameTime();
	assert(resumeTime > pauseTime);
	player->TimeShiftAnimators(resumeTime - pauseTime);
	enemy->TimeShiftAnimators(resumeTime - pauseTime);
	return doExit;
}

bool StartScreen(EventQueue* queue) {
	ALLEGRO_FONT* startFont = wrap_load_ttf_font("./assets/Fonts/Helvetica.ttf", 18, 0);
	Bitmap startImage = BitmapLoader::Load("./assets/Backgrounds/logo454.jpg");
	Point startImageLocation(0, 0);
	Rect startImageRect(startImageLocation, static_cast<unsigned>(SCREEN_WIDTH), static_cast<unsigned>(SCREEN_HEIGHT));

	ALLEGRO_COLOR color = wrap_map_rgb(0, 0, 0);
	Point namesPos(500, 40);
	Point uniPos(20, 500);
	string names(
		"DIMITRIS KONSTANTINOU\n\n"
		"DIONYSIS-ARISTOTELIS\nKALOCHRISTIANAKIS\n\n"
		"MYRON TSATSARAKIS\n\n");
	string uni(
		"University of Crete\n"
		"Department of Computer Science\n"
		"CS-454. Development of Intelligent Interfaces and Games\n"
		"Term Project, Fall Semester 2018\n");

	wrap_clear_to_color(wrap_map_rgb(0, 0, 0));
	BitmapAPI::Blit(startImage, startImageRect, startImageLocation);
	wrap_draw_multiline_text(startFont, color, namesPos.GetX(), namesPos.GetY(), 500, 20, ALLEGRO_ALIGN_LEFT, names.c_str());
	wrap_draw_multiline_text(startFont, color, uniPos.GetX(), uniPos.GetY(), 500, 20, ALLEGRO_ALIGN_LEFT, uni.c_str());
	wrap_flip_display();

	bool doResume = false;
	bool doExit = false;
	while (!doResume) {
		ALLEGRO_EVENT event = queue->WaitNextEvent();
		if (event.type == ALLEGRO_EVENT_KEY_DOWN) {
			if (event.keyboard.keycode == ALLEGRO_KEY_ESCAPE) {
				doExit = true;
				doResume = true;
			} else {
				doResume = true;
			}
		}
	}
	return doExit;
}

void DisplayCheckVictory(ALLEGRO_FONT* font, Fighter* player, Fighter* enemy) {
	ALLEGRO_COLOR color = wrap_map_rgb(0, 0, 0);
	Point pos(350, 50);
	string victoryText("");
	string soundVictoryText("");
	if (enemy->GetStateTransitions()->GetState() == "winning_pose") {
		victoryText = enemy->GetName() + " WINS!";
		//soundVictoryText = enemy->GetName() + ".wins";

		SoundMixer::PlayAudioInstance(enemy->GetName() + ".wins");

	} else if (player->GetStateTransitions()->GetState() == "winning_pose") {
		victoryText = player->GetName() + " WINS!";
		SoundMixer::PlayAudioInstance(player->GetName() + ".wins");

		//soundVictoryText = player->GetName() + ".wins";
	}
	wrap_draw_text(font, color, pos.GetX(), pos.GetY(), ALLEGRO_ALIGN_LEFT, victoryText.c_str());
}

void DisplayHealthBars(ALLEGRO_FONT* font, Fighter* player1, Fighter* player2) {
	// Health Bar 1
	float healthBar1Width = 250 * (player1->GetHealth() / 100.0f);
	if (healthBar1Width < 0) healthBar1Width = 0;
	wrap_draw_filled_rectangle(50.0f, 50.0f, 300.0f, 80.0f, COLOR_RED);
	wrap_draw_filled_rectangle(50.0f, 50.0f, 50.0f + healthBar1Width, 80.0f, COLOR_GREEN);
	wrap_draw_rectangle(50.0f, 50.0f, 300.0f, 80.0f, COLOR_BLACK, 3.5f);
	wrap_draw_text(font, ::COLOR_BLACK, 55.0f, 45.0f, ALLEGRO_ALIGN_LEFT, player1->GetName().c_str());

	// Health Bar 2
	float healthBar2Width = 250 * (player2->GetHealth() / 100.0f);
	if (healthBar2Width < 0) healthBar2Width = 0;
	wrap_draw_filled_rectangle(500.0f, 50.0f, 750.0f, 80.0f, COLOR_RED);
	wrap_draw_filled_rectangle(750 - healthBar2Width, 50.0f, 750.0f, 80.0f, COLOR_GREEN);
	wrap_draw_rectangle(500.0f, 50.0f, 750.0f, 80.0f, COLOR_BLACK, 3.5f);
	wrap_draw_text(font, ::COLOR_BLACK, 745.0f, 45.0f, ALLEGRO_ALIGN_RIGHT, player2->GetName().c_str());
}
