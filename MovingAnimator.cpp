#include "MovingAnimator.h"

using namespace std;

MovingAnimator::MovingAnimator()
	: sprite(nullptr), anim(nullptr) {}
MovingAnimator::~MovingAnimator() {}
void MovingAnimator::Start(Sprite* s, MovingAnimation* a, timestamp_t t) {
	sprite = s;
	anim = a;
	lastTime = t;
	state = ANIMATOR_RUNNING;
}
void MovingAnimator::Progress(timestamp_t currTime) {
	while (currTime > lastTime && currTime - lastTime >= anim->GetDelay()) {
		sprite->Move(anim->GetDx(), anim->GetDy());
		lastTime += anim->GetDelay();
		if (!anim->GetContinuous()) {
			state = ANIMATOR_FINISHED;
			NotifyStopped();
			return;
		}
	}
}
