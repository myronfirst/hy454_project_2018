#include "Sprite.h"

#include "Wrappers.h"

#include <assert.h>
#include <math.h>
#include <iostream>

using namespace std;

//Spite private
void Sprite::MovePriv(int dx, int dy) {
	long _x = static_cast<long>(this->x) + dx;
	long _y = static_cast<long>(this->y) + dy;
	if (_x < 0) _x = 0;
	if (_x + frameBox.GetW() > SCREEN_WIDTH) _x = SCREEN_WIDTH - frameBox.GetW();
	//where is height origin point?
	if (_y < 0) _y = 0;
	if (_y + frameBox.GetH() > SCREEN_HEIGHT) _y = SCREEN_HEIGHT - frameBox.GetH();

	this->x = _x;
	this->y = _y;
}

// void Sprite::NotifyCollision(Sprite* arg) {
// 	for (auto it = handlers.begin(); it != handlers.end(); ++it)
// 		(**it)(this, arg);
// }

//Sprite public
Sprite::Sprite(Dim _x, Dim _y, const AnimationFilm* film)
	: x(_x), y(_y), isVisible(true), currFilm(film) {
	//vFrameAllign, hFrameAllign Initialization??
	frameNo = currFilm->GetTotalFrames();
	SetFrame(0);
}

Sprite::~Sprite() {
	// ClearHandlers();
}
void Sprite::SetFrame(byte i) {
	if (i != frameNo) {
		assert(i < currFilm->GetTotalFrames());
		vFrameAllign = 120 - currFilm->GetFrameBox(i).GetH();
		hFrameAllign = 120 - currFilm->GetFrameBox(i).GetW();
		//hFrameAllign = hFrameAllign / 2;
		hFrameAllign -= 10;
		vFrameAllign *= 2.5;
		//vFrameAllign = 120 - vFrameAllign;
		//hFrameAllign = 120 - hFrameAllign;
		frameBox = currFilm->GetFrameBox(frameNo = i);
	}
}

byte Sprite::GetFrameNo(void) const {
	return frameNo;
}

const Rect& Sprite::GetFrameBox(void) const {
	return frameBox;
}

Dim Sprite::GetX(void) const {
	return x;
}
Dim Sprite::GetY(void) const {
	return y;
}

Dim Sprite::GethFrameAllign(void) const {
	return hFrameAllign;
}

Dim Sprite::GetvFrameAllign(void) const {
	return vFrameAllign;
}

void Sprite::SetVisibility(bool v) {
	isVisible = v;
}

bool Sprite::IsVisible(void) const {
	return isVisible;
}

const AnimationFilm* Sprite::GetFilm(void) const {
	return currFilm;
}

void Sprite::SetFilm(const AnimationFilm* newFilm) {
	currFilm = newFilm;
}

/* void Sprite::Move(int dx, int dy) {
	do {
		if (dx <= 0 - static_cast<int>(Sprite::frameBox.GetW())) {	// Need to add check for dx move passed Screen Width
			dx = 0;
		}
	} while (dx || dy);
} */
void Sprite::Move(int dx, int dy) {
	do {
		auto dxFinal(
			dx >= 0 ? min(GRID_SIZE, dx) : max(-GRID_SIZE, dx));
		auto dyFinal(
			dy >= 0 ? min(GRID_SIZE, dy) : max(-GRID_SIZE, dy));

		MovePriv(dxFinal, dyFinal);

		dx -= dxFinal;
		dy -= dyFinal;
	} while (dx || dy);
}
// void Sprite::Display(Bitmap dest, const Rect& da) {
// 	Rect visibleArea;
// 	Rect spriteArea(x, y, frameBox.GetW(), frameBox.GetH());
// 	if (!ClippedEntirely(spriteArea, myLayer->GetPixelViewWindow(), &visibleArea)) {
// 		Rect clippedFrame(
// 			frameBox.GetX() + (visibleArea.GetX() - x),
// 			frameBox.GetY() + (visibleArea.GetY() - y),
// 			visibleArea.GetW(),
// 			visibleArea.GetY());

// 		Point at(
// 			da.GetX() + (visibleArea.GetX() + myLayer->GetPixelViewWinX()),
// 			da.GetY() + (visibleArea.GetY() + myLayer->GetPixelViewWinY())

// 		);

// 		MaskedBlit(
// 			currFilm->GetBitmap(),
// 			clippedFrame,
// 			dest,
// 			at);
// 	}
// }

/* void Sprite::AddCollisionHandler(const CollisionHandler& h) {
	handlers.push_back(h.Clone());
}
void Sprite::ClearHandlers(void) {
	for (auto it = handlers.begin(); it != handlers.end(); ++it)
		delete *it;
	handlers.clear();
}
 */
bool Sprite::CollisionCheck(Sprite* enemy) {
	// Current (this) sprite bounding box bounds
	unsigned thisX = x;
	unsigned thisW = frameBox.GetW() + hFrameAllign;
	unsigned thisXnW = thisX + thisW;

	unsigned thisY = y;
	unsigned thisH = frameBox.GetH() + vFrameAllign;
	unsigned thisYnH = thisY + thisH;

	// Enemy Bounding Box Bounds
	unsigned enemyX = enemy->GetX();
	unsigned enemyW = enemy->frameBox.GetW() + hFrameAllign;
	unsigned enemyXnW = enemyX + enemyW;

	unsigned enemyY = enemy->GetY();
	unsigned enemyH = enemy->frameBox.GetH() + vFrameAllign;
	unsigned enemyYnH = enemyY + enemyH;

	// std::cout << "--------Player1--------:" << std::endl;
	// std::cout << "X1: " << thisX << "\tX2: " << thisXnW << std::endl;
	// std::cout << "Y1: " << thisY << "\tY2: " << thisYnH << std::endl;

	// std::cout << "--------Player2--------:" << std::endl;
	// std::cout << "X1: " << enemyX << "\tX2: " << enemyXnW << std::endl;
	// std::cout << "Y1: " << enemyY << "\tY2: " << enemyYnH << std::endl;

	// wrap_draw_filled_rectangle(thisX, thisY, thisXnW, thisYnH, wrap_map_rgba(0, 255, 0, 0.87f));
	// wrap_draw_filled_rectangle(enemyX, enemyY, enemyXnW, enemyYnH, wrap_map_rgba(0, 255, 0, 0.87f));

	// return !(enemyX > thisXnW || thisX > enemyXnW || enemyY > thisYnH || thisY > enemyYnH);
	return (!(enemyX > thisXnW || thisX > enemyXnW || enemyY > thisYnH || thisY > enemyYnH));
}

//Not thought out well enough
void Sprite::Display(Bitmap dest, int direction) {
	BitmapAPI::Blit(currFilm->GetBitmap(), frameBox, dest, Point(x + hFrameAllign, y + vFrameAllign), direction);
}

void Sprite::DisplayFrameBox(ALLEGRO_COLOR color) {
	wrap_draw_filled_rectangle(x + hFrameAllign, y + vFrameAllign, x + frameBox.GetW() * 2.5 + hFrameAllign, y + frameBox.GetH() * 2.5 + vFrameAllign, color);
}
