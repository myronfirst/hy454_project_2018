#include "MovingPathAnimation.h"

#include <cassert>

using namespace std;
PathEntry::PathEntry()
	: dx(0), dy(0), frame(0), delay(0) {}
PathEntry::PathEntry(offset_t _dx, offset_t _dy, frame_t _frame, delay_t _delay)
	: dx(_dx), dy(_dy), frame(_frame), delay(_delay) {}
PathEntry::PathEntry(const PathEntry& p)
	: dx(p.dx), dy(p.dy), frame(p.frame), delay(p.delay) {}
PathEntry::~PathEntry() {}

//MovingPathAnimation
MovingPathAnimation::MovingPathAnimation(animd_t _id, bool _continuous, const std::vector<PathEntry>& _path)
	: Animation(_id), continuous(_continuous), path(_path) {
	assert(!path.empty());
}
MovingPathAnimation::~MovingPathAnimation() {}
Animation* MovingPathAnimation::Clone(animd_t newId) const {
	return new MovingPathAnimation(newId, continuous, path);
}
bool MovingPathAnimation::GetContinuous(void) const {
	return continuous;
}
void MovingPathAnimation::SetContinuous(bool _continuous) {
	continuous = _continuous;
}
const std::vector<PathEntry>& MovingPathAnimation::GetPath(void) const {
	return path;
}
void MovingPathAnimation::SetPath(const std::vector<PathEntry>& _path) {
	path.clear();
	path = _path;
	assert(!path.empty());
}
