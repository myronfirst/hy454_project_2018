#include "TickAnimation.h"

#include <cassert>

TickAnimation::TickAnimation(animd_t _id, delay_t _delay, unsigned _repetitions, TickFunc _action)
	: Animation(_id), delay(_delay), repetitions(_repetitions), action(_action) {}
TickAnimation::~TickAnimation() {}
Animation* TickAnimation::Clone(animd_t newId) const {
	return (new TickAnimation(newId, delay, repetitions, action));
}
delay_t TickAnimation::GetDelay(void) const {
	return delay;
}
void TickAnimation::SetDelay(delay_t _delay) {
	delay = _delay;
}
unsigned TickAnimation::GetRepetitions(void) const {
	return repetitions;
}
void TickAnimation::SetRepetitions(unsigned _repetitions) {
	repetitions = _repetitions;
}
TickAnimation::TickFunc TickAnimation::GetAction(void) const {
	return action;
}
void TickAnimation::SetAction(TickAnimation::TickFunc _action) {
	action = _action;
}
