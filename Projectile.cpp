#include "Projectile.h"
#include <iostream>
#include "AnimationDataHolder.h"

Projectile::Projectile(Sprite* _sprite, std::string _name, std::string animationFilmID)
	: sprite(_sprite), name(_name) {
	animator = new FrameListAnimator();
	toBeDeleted = false;
	animator->SetOnFinish([&](Animator* animator) {
		std::cout << "Projectile Animator onFinish" << std::endl;
		toBeDeleted = true;
	});

	animator->Start(sprite, AnimationDataHolder::GetFrameListAnimation(animationFilmID), GetGameTime());
}

Projectile::~Projectile() {
	delete (sprite);
	delete (animator);
}

Sprite* Projectile::GetSprite() {
	return sprite;
}
FrameListAnimator* Projectile::GetAnimator() {
	return animator;
}
std::string Projectile::GetName() {
	return name;
}
bool Projectile::IsOutOfBounds() {
	unsigned x = sprite->GetX();
	if (x > SCREEN_WIDTH || x <= 0) return true;
	return false;
}
