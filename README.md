Για την ονοματολογία χρησιμοποιούμε τα στυλ:

Defines: #Define CAMELCASE
Global vars: CamelCase
Local vars and class attributes: camelCase
Global functions: CamelCase()
Class member functions: CamelCase()
Files: CamelCase, except main.cpp

Τις συναρτήσεις της Allegro τις χρησιμοποιούμε ως έχει, μέχρι να τις κάνουμε wrap σε μία Global ή class function ανάλογα με σχεδιαστικές αποφάσεις.

Αρχικές αποφάσεις:
Τις Allegro συναρτήσεις που επιστεφουν τιμή λάθος τις κάνουμε wrap σε δικές μας σε ξεχωριστό αρχείο wrappers.cpp/.h

Οι παραπάνω αποφάσεις μπορεί να αλλάξουν με την πάροδο του χρόνου αν δούμε ότι δε μας βολεύουν.

#Tasks to do:
-Implement Audio controller
-Add game pause
-Seperate Background members and methods in a different class
-Implement Projectile Manager
-Add more moves/states
(feel free to add more tasks or choose one of the above to do)

#About "libpng warning: iCCP: known incorrect sRGB profile"
We ignore it as it is explained in the following link:  https://stackoverflow.com/questions/22745076/libpng-warning-iccp-known-incorrect-srgb-profile
#Known fix: Load png image in Gimp, export as png

#When loading bitmaps keep in mind that:
As the reference manual says: this is an abstract type representing a bitmap (2D image). After creating our display we load our image using the al_load_bitmap() function.

If you load an image before the creation of a display you would be loading it as a Memory Bitmap which is extremely slow, for that reason remember always to load bitmaps after the creation of a display. Future versions of Allegro 5 (Allegro 5.2), will have the possibility of changing from memory to video bitmaps any time in the code, but currently the only way to get a Video Bitmap is loading the image after the creation of a display.

reference link: https://wiki.allegro.cc/index.php?title=Basic_tutorial_on_loading_and_showing_images


#About subBitmap deallocation:
SubBitmaps share the same memory with the parent Bitmap therefore we don't allocate
new memory when we create a new sub_bit_map

#Constructors Destructors
Στην πάνω πάνω στα declarations/definitions (χωρίς void στην παραμετρο)
πχ
class Animation {
	private:
	...

	public:
		Animation();
		~Animation();
}

#Functions with no parameters
Declare as:
eg. int function(void)
(use void keyword)


#About Sprite constructor
AnimationFilm* currFilm can be changed to const AnimationFilm* currFilm because Sprite doesn't change
the properties of AnimationFilm


#About animation data
The AnimationDataHolder class simply reads the .ini files and loads the data into the corresponding animation class. If you want to change some values from the config file you must know what you are doing because there is no error checking. You can find a template animation data config at ./assets/Raiden/Template_Raiden_AnimationData.ini


#About loading lamdas in SetTranstion
I wrote a method in Fighter that tries to handle all setTranstions depending on input, state and type of animator but it doesn't work. So, I load the Transition table in main, we need to find a more easy way to set our Transitions in the future.


#Character Movelist: https://gamefaqs.gamespot.com/snes/588499-mortal-kombat/faqs/60043


#Keybindings Setup and Configuration
Currently the keybindings are hardwired inside the program (see main2Player.cpp), there will be some changes in the Fighter class (see Fighter.h for more details).
TIP: If you add more Keys in the two instances of Action make sure that they are already defined in the keyMap of the InputController file (or define them yourself).

#Collision Detection Notes
Όσο τέσταρα το Collision Handling, έπαιρνα μηνύματα στην console κάθε φορα που μετακινούσα ένα χαρακτήρα του τύπου:
Warning: RectX: 417 Y: 2 W: 45 H: 103exceeds bitmap grid size
Δεν ξέρω τι είναι. Δεν πιστευω πως έχει να κάνει με το handling καθώς λάμβανα αυτά τα μηνύματα πριν αρχίσω να γραφω κώδικα.

Επιπλέον, χρησιμοποιόντας το κανονικό μέγεθος των sprites παρατήρησα πως τα  BoundingBoxes είναι misaligned. Υποπτεύομαι πως φταίει το Frame Change Alignment Policy αλλάζοντας μόνο το sprite Display χωρίς να πειράζει τις τοποθεσείες των FrameBoxes.
Χρησιμοποιόντας scaled Sprites, το πρόβλημα φαίνεται πιο άσχημο.

Σχετικά με το Handling.
Αρχικά μετακινούνται τα sprites των χαρακτήρων ώστε να μην κάνουν overlap. (προτεραιότητα έχει ο 2ος παίχτης, φτιάξτε το αν σκεφτείτε κάποιον τρόπο).
Στη συνέχεια, ανάλογα με τα states των 2 παιχτών (NORMAL, ATTACK, BLOCK), λαμβάνεται η απόφαση για το ποιος θα φάει damage + θα παίξει το stun animation.
Για τους χαρακτήρες που είναι BLOCK, αυτοί δεν λαμβάνουν damage και δεν παίζουν κάποιο animation.
Το ATTACKED state, χρησιμοποιείται προκειμένου να αποφευχθεί το πολλαπλό damage received στις περιπτώσεις που υπάρχει πολλαπλό collision detection ανα animation.

To stun animation είναι κάποιου είδους uppercut. Αλλάξτε το σε κάτι πιο "πρέπον".
