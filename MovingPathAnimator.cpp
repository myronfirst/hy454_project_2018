#include "MovingPathAnimator.h"

MovingPathAnimator::MovingPathAnimator()
	: sprite(nullptr), anim(nullptr), currEl(0) {}
MovingPathAnimator::~MovingPathAnimator() {}
void MovingPathAnimator::Start(Sprite* s, MovingPathAnimation* a, timestamp_t t) {
	sprite = s;
	anim = a;
	lastTime = t;
	state = ANIMATOR_RUNNING;
	currEl = 0;
	sprite->Move(anim->GetPath().at(currEl).dx, anim->GetPath().at(currEl).dy);
	sprite->SetFrame(anim->GetPath().at(currEl).frame);
}
void MovingPathAnimator::Progress(timestamp_t currTime) {
	while (currTime > lastTime && currTime - lastTime >= anim->GetPath().at(currEl).delay) {
		lastTime += anim->GetPath().at(currEl).delay;
		if (currEl == anim->GetPath().size() - 1) {
			currEl = 0;
		} else {
			++currEl;
		}

		sprite->Move(anim->GetPath().at(currEl).dx, anim->GetPath().at(currEl).dy);
		sprite->SetFrame(anim->GetPath().at(currEl).frame);

		if (currEl == anim->GetPath().size() - 1 && !anim->GetContinuous()) {
			state = ANIMATOR_FINISHED;
			NotifyStopped();
			return;
		}
	}
}
