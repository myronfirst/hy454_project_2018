#include "MovingAnimation.h"

using namespace std;
MovingAnimation::MovingAnimation(animd_t _id, offset_t _dx, offset_t _dy, delay_t _delay, bool _continuous)
	: Animation(_id), dx(_dx), dy(_dy), delay(_delay), continuous(_continuous) {}
MovingAnimation::~MovingAnimation() {}
Animation* MovingAnimation::Clone(animd_t newId) const {
	return (new MovingAnimation(newId, dx, dy, delay, continuous));
}
offset_t MovingAnimation::GetDx(void) const {
	return dx;
}
void MovingAnimation::SetDx(offset_t _dx) {
	dx = _dx;
}
offset_t MovingAnimation::GetDy(void) const {
	return dy;
}
void MovingAnimation::SetDy(offset_t _dy) {
	dy = _dy;
}
delay_t MovingAnimation::GetDelay(void) const {
	return delay;
}
void MovingAnimation::SetDelay(delay_t _delay) {
	delay = _delay;
}
bool MovingAnimation::GetContinuous(void) const {
	return continuous;
}
void MovingAnimation::SetContinuous(bool _continuous) {
	continuous = _continuous;
}
