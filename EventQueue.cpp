#include "EventQueue.h"

#include <cassert>

EventQueue::EventQueue()
	: queue(nullptr) {
	queue = wrap_create_event_queue();
}
EventQueue::~EventQueue() {
	wrap_destroy_event_queue(queue);
	queue = nullptr;
}
void EventQueue::RegisterSource(ALLEGRO_EVENT_SOURCE* source) {
	wrap_register_event_source(queue, source);
}
bool EventQueue::IsEmpty() const {
	return wrap_is_event_queue_empty(queue);
}
ALLEGRO_EVENT_QUEUE* EventQueue::GetQueue() const {
	assert(queue);
	return queue;
}
ALLEGRO_EVENT EventQueue::GetNextEvent() const {
	ALLEGRO_EVENT event;
	assert(wrap_get_next_event(queue, &event));
	return event;
}
ALLEGRO_EVENT EventQueue::WaitNextEvent() const {
	ALLEGRO_EVENT event;
	wrap_wait_for_event(queue, &event);
	return event;
}
